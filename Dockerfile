FROM maven:3.8-amazoncorretto-19 AS build
COPY src src
COPY pom.xml pom.xml
COPY .mvn/jvm.config .mvn/jvm.config
RUN mvn package -T 1.0C


FROM openjdk:19-jdk-alpine
COPY --from=build target/uebungstool-1.2.jar /usr/app/uebungstool.jar
EXPOSE 8080 8443

ENTRYPOINT ["java","-jar","/usr/app/uebungstool.jar"]
