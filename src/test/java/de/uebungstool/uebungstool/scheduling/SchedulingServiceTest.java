package de.uebungstool.uebungstool.scheduling;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

import de.uebungstool.uebungstool.scheduling.logic.SchedulingService;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramCell;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SchedulingServiceTest {

  private SchedulingDiagram diagram;
  SchedulingService schedulerService;

  /**
   * Initialises the Diagram Object with a valid FCFS Strategy configuration This is the same
   * standard configuration which is used in the Website.
   */
  @BeforeEach
  public void initDiagram() {
    diagram = new SchedulingDiagram();
    diagram.setStrategy(STRATEGIES_FCFS);
    diagram.setPreemptive(false);
    diagram.setSchedulerTime(true);
    diagram.setDispatcherTime(false);
    diagram.setCoreNumber(1);
    diagram.setInitialScheduler(true);

    // Priority Bonus
    // diagram.setDynamicPriorities(true);
    diagram.setPrioChangeComputing(2.0);
    diagram.setPrioChangeWaiting(-1.0);
    diagram.setLowestPriorityFirst(false);

    // RR Bonus
    diagram.setTimeSliceLength(5);
    diagram.setUserSchedulerTime(false);

    schedulerService = new SchedulingService();
  }

  /** addes basic UserlevelThreads to the UserlevelThread array of the diagram object */
  private void initDefaultThreads() {
    SchedulingUserLevelThread[] userLevelThreads = new SchedulingUserLevelThread[2];
    userLevelThreads[0] = new SchedulingUserLevelThread();
    userLevelThreads[0].setComputingTime(5);
    userLevelThreads[0].setStartTime(1);
    userLevelThreads[0].setCells(newDiagramCells(20));
    userLevelThreads[0].setDeadline(-1);
    userLevelThreads[0].setId(0);
    userLevelThreads[0].setIoAftern(-1);
    userLevelThreads[0].setIoLength(-1);
    userLevelThreads[1] = configureScheduler();

    diagram.userLevelThreads = (userLevelThreads);
  }

  private SchedulingDiagramCell[] newDiagramCells(int i) {
    SchedulingDiagramCell[] retVal = new SchedulingDiagramCell[i];
    for (int u = 0; u < i; u++) {
      retVal[u] = new SchedulingDiagramCell();
      retVal[u].setStatus("");
    }
    return retVal;
  }

  private SchedulingUserLevelThread configureScheduler() {
    SchedulingUserLevelThread retValue = new SchedulingUserLevelThread();
    retValue.setComputingTime(-1);
    retValue.setStartTime(-1);
    retValue.setCells(newDiagramCells(20));
    retValue.setDeadline(-1);
    retValue.setId(-1);
    retValue.setIoAftern(-1);
    retValue.setIoLength(-1);
    retValue.setPriority(-1);
    return retValue;
  }

  /** Tests whether the preemptive attribute was changed for specific Strategies */
  @Test
  void checkIfDiagramIsValid_unvalid_NullThreads() {
    diagram.userLevelThreads = (null);
    assertFalse(schedulerService.evaluate(diagram));
  }

  @Test
  void checkIfDiagramIsValid_unvalid_NoThreads() {
    diagram.userLevelThreads = (new SchedulingUserLevelThread[0]);
    assertFalse(schedulerService.evaluate(diagram));
  }

  @Test
  void checkIfDiagramIsValid_unvalid_OneThreadNull() {
    diagram.userLevelThreads = (new SchedulingUserLevelThread[1]);
    assertFalse(schedulerService.evaluate(diagram));
  }

  @Test
  void checkIfDiagramIsValid_unvalid_ThreeThreadsNull() {
    diagram.userLevelThreads = (new SchedulingUserLevelThread[3]);
    assertFalse(schedulerService.evaluate(diagram));
  }

  @Test
  void checkIfDiagramIsValid_unvalid_TwoSchedulers() {
    initDefaultThreads();
    diagram.userLevelThreads[0] = diagram.userLevelThreads[1];
    assertFalse(schedulerService.evaluate(diagram));
  }

  @Test
  void checkIfDiagramIsValid_unvalid_NoScheduler() {
    initDefaultThreads();
    diagram.userLevelThreads[1] = diagram.userLevelThreads[0];
    assertFalse(schedulerService.evaluate(diagram));
  }

  /** Tests whether the preemptive attribute was changed for specific Strategies */
  @Test
  void checkIfDiagramIsValid_unvalid_noScheduler() {
    initDefaultThreads();
    assert (schedulerService.evaluate(diagram));
    assert (!diagram.isPreemptive());
  }

  /** Tests whether the preemptive attribute was changed for specific Strategies */
  @Test
  void checkIfDiagramIsValid_valid_FCFSPreemptiveChanged() {
    initDefaultThreads();
    diagram.setPreemptive(true);
    assert (schedulerService.evaluate(diagram));
    assertFalse(diagram.isPreemptive());
  }

  /** Tests whether the preemptive attribute was changed for specific Strategies */
  @Test
  void checkIfDiagramIsValid_valid_FCFSChanged() {
    initDefaultThreads();
    diagram.setPreemptive(true);
    assert (schedulerService.evaluate(diagram));
    assertFalse(diagram.isPreemptive());
  }

  /** Tests whether the preemptive attribute was changed for specific Strategies */
  @Test
  void checkIfDiagramIsValid_valid_SRTNChanged() {
    initDefaultThreads();
    diagram.setStrategy(STRATEGIES_SRTN);
    diagram.setPreemptive(false);
    assert (schedulerService.evaluate(diagram));
    assert (diagram.isPreemptive());
  }

  @Test
  void checkIfDiagramIsValid_valid_PreemptiveChanged() {
    assert (checkAttributeChanged(STRATEGIES_FCFS, true, false));
    assert (checkAttributeChanged(STRATEGIES_SJF, true, false));
    assert (checkAttributeChanged(STRATEGIES_SRTN, false, true));
    // initDefaultKernellevelThreads();
    // assert(checkAttributeChanged(STRATEGIES_RR,true,false));

    assert (checkAttributeChanged(STRATEGIES_PRIORITY, true, true));
    assert (checkAttributeChanged(STRATEGIES_PRIORITY, false, false));
    assert (checkAttributeChanged(STRATEGIES_EDF, true, true));
    assert (checkAttributeChanged(STRATEGIES_EDF, false, false));
  }

  private boolean checkAttributeChanged(
      int strategy, boolean preemptive, boolean preemptiveExpected) {
    initDefaultThreads();
    diagram.setStrategy(strategy);
    diagram.setPreemptive(preemptive);
    if (schedulerService.evaluate(diagram)) return preemptiveExpected == diagram.isPreemptive();
    else return false;
  }
}
