package de.uebungstool.uebungstool.paging.data;

import org.junit.jupiter.api.Test;

class ConfigTest {
  Config c1 = new Config("test", new PagingDiagram());

  @Test
  void getName() {
    assert c1.getName().equals("test");
  }

  @Test
  void getDiagram() {
    assert c1.getDiagram() != null;
  }
}
