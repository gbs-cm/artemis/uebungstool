package de.uebungstool.uebungstool.paging.data;

import org.junit.jupiter.api.Test;

class TableCellTest {
  TableCell t1 = new TableCell(-1, -1, 0, 0); // Empty TableCell
  TableCell t2 = new TableCell(1, -1, 0, 0); // Page filled, Flag empty
  TableCell t3 = new TableCell(-1, 10, 0, 0); // Page empty, Flag filled
  TableCell t4 = new TableCell(1, 10, 0, 0); // Page filled, Flag filled
  TableCell t5 = new TableCell(-2, -1, 0, 0); // Page lower than -1
  TableCell t6 = new TableCell(-2, -1, 1, 0); // Other index
  TableCell t7 = new TableCell(); // Empty TableCell
  TableCell t8 = new TableCell(3, -1, 0, 0); // Higher Page

  @Test
  void testCompare() {
    assert (0 == t1.compare(t1));
    assert (0 != t1.compare(t2));
    assert (0 != t1.compare(t3));
    assert (0 != t1.compare(t4));
    assert (0 == t1.compare(t5));
    assert (0 == t2.compare(t2));
    assert (0 != t2.compare(t3));
    assert (0 != t2.compare(t4));
    assert (0 != t2.compare(t5));
    assert (0 == t3.compare(t3));
    assert (0 != t3.compare(t4));
    assert (0 != t3.compare(t5));
    assert (0 == t4.compare(t4));
    assert (0 != t4.compare(t5));
    assert (0 == t5.compare(t5));
    assert (-3 == t1.compare(t6));
    assert (TableCell.DIFFERENT == t2.compare(t8));
  }

  @Test
  void differsInPoint() {
    TableCell result = t1.differsInPoint(t1);
    assert (result.getPage() < 0 && result.getFlag() < 0);
    result = t1.differsInPoint(t2);
    assert (result.getPage() > 0 && result.getFlag() < 0);
    result = t1.differsInPoint(t3);
    assert (result.getPage() < 0 && result.getFlag() > 0);
    result = t1.differsInPoint(t4);
    assert (result.getPage() > 0 && result.getFlag() > 0);
  }

  @Test
  void getPage() {
    assert t1.getPage() == -1;
    assert t2.getPage() == 1;
    assert t7.getPage() == -1;
  }

  @Test
  void getFlag() {
    assert t1.getFlag() == -1;
    assert t3.getFlag() == 10;
    assert t7.getFlag() == -1;
  }
}
