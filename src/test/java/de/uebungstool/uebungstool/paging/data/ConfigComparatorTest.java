package de.uebungstool.uebungstool.paging.data;

import org.junit.jupiter.api.Test;

class ConfigComparatorTest {
  Config c1 = new Config("test", null);
  Config c2 = new Config("test2", null);
  Config c3 = new Config("test", new PagingDiagram());
  ConfigComparator cc = new ConfigComparator();

  @Test
  void compare() {
    assert cc.compare(c1, c2) != 0;
    assert cc.compare(c1, c3) == 0;
    assert cc.compare(c2, c2) == 0;
  }
}
