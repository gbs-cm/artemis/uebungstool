package de.uebungstool.uebungstool.paging.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

class PagingDiagramTest {
  ObjectMapper objectMapper = new ObjectMapper();
  PagingDiagram d1;
  PagingDiagram d2;
  int[] callList = {1, 3, 5, 4, 2, 4, 3, 2, 1, 6, 5, 3};

  public PagingDiagramTest() {
    try {
      d1 =
          objectMapper.readValue(
              "{\"diagram\":null,\"callList\":[1,3,5,4,2,4,3,2,1,6,5,3],\"pageFaults\":null,\"strategy\":\"fifo\",\"frameCount\":4}",
              PagingDiagram.class);
      d2 =
          objectMapper.readValue(
              "{\"diagram\":null,\"callList\":[],\"pageFaults\":null,\"strategy\":\"fifo\",\"frameCount\":9999}",
              PagingDiagram.class);
    } catch (JsonProcessingException e) {
      d1 = null;
      d2 = null;
    }
  }

  @Test
  void selfCheck() {
    try {
      d1.selfCheck();
      d2.selfCheck();
    } catch (LimitException e) {
      assert e.getMessage().compareTo("Too few distinct pages!") == 0;
    }
  }

  @Test
  void getCallList() {
    assert d1.getCallList().size() == callList.length;
  }

  @Test
  void getRWList() {
    assert d1.getRWList() == null;
  }

  @Test
  void getStrategy() {
    assert d1.getStrategy().compareTo("fifo") == 0;
  }

  @Test
  void getFrameCount() {
    assert d1.getFrameCount() == 4;
  }

  @Test
  void getTable() {
    assert d1.getTable() == null;
  }

  @Test
  void getPageFaults() {
    assert d1.getPageFaults() == null;
  }

  @Test
  void getWriteBacks() {
    assert d1.getWriteBacks() == null;
  }
}
