package de.uebungstool.uebungstool.paging;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

class PagingControllerTest {
  PagingController pagingController = new PagingController();

  @Test
  void checkDiagram() {
    String request =
        "{\"diagram\":[[{\"page\":1,\"flag\":-1,\"i\":0,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":0,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":0,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":0,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":1,\"j\":0},{\"page\":3,\"flag\":-1,\"i\":1,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":1,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":1,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":2,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":2,\"j\":1},{\"page\":5,\"flag\":-1,\"i\":2,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":2,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":3,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":3,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":3,\"j\":2},{\"page\":4,\"flag\":-1,\"i\":3,\"j\":3}],[{\"page\":2,\"flag\":-1,\"i\":4,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":4,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":4,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":4,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":5,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":5,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":5,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":5,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":6,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":6,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":6,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":6,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":7,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":7,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":7,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":7,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":8,\"j\":0},{\"page\":1,\"flag\":-1,\"i\":8,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":8,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":8,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":9,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":9,\"j\":1},{\"page\":6,\"flag\":-1,\"i\":9,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":9,\"j\":3}],[{\"page\":-1,\"flag\":-1,\"i\":10,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":10,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":10,\"j\":2},{\"page\":5,\"flag\":-1,\"i\":10,\"j\":3}],[{\"page\":3,\"flag\":-1,\"i\":11,\"j\":0},{\"page\":-1,\"flag\":-1,\"i\":11,\"j\":1},{\"page\":-1,\"flag\":-1,\"i\":11,\"j\":2},{\"page\":-1,\"flag\":-1,\"i\":11,\"j\":3}]],\"callList\":[1,3,5,4,2,4,3,2,1,6,5,3],\"rwList\":[false,true,false,false,true,true,true,false,false,false,false,true],\"pageFaults\":[0,1,2,3,4,8,9,10,11],\"writeBacks\":[],\"strategy\":\"fifo\",\"frameCount\":4}";
    String s = pagingController.checkDiagram(request).getBody();
    assert s != null && s.length() < 11;
  }

  @Test
  void getSampleConfig() {
    assert pagingController.getSampleConfig().getStatusCodeValue() == HttpStatus.OK.value();
  }
}
