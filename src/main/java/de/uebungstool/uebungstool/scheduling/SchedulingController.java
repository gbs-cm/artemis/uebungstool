package de.uebungstool.uebungstool.scheduling;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uebungstool.uebungstool.scheduling.logic.SchedulingService;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "scheduling")
public class SchedulingController {

  private final SchedulingService schedulerService;
  private static final Logger log = Logger.getLogger(SchedulingController.class.getName());

  @Autowired
  @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "Not accessed by untrusted Code.")
  public SchedulingController(SchedulingService schedulerService) {
    log.info("Server started");
    this.schedulerService = schedulerService;
  }

  @PostMapping()
  public ResponseEntity<String> checkDiagram(@RequestBody String request) {

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      SchedulingDiagram diagram = objectMapper.readValue(request, SchedulingDiagram.class);
      if (schedulerService.evaluate(diagram)) {
        log.info(diagram.generateLogString());
        return new ResponseEntity<>(objectMapper.writeValueAsString(diagram), HttpStatus.CREATED);
      } else {
        log.info("Error with the diagram");
        return new ResponseEntity<>(
            objectMapper.writeValueAsString(diagram), HttpStatus.INTERNAL_SERVER_ERROR);
      }

    } catch (JsonProcessingException e) {
      log.info("Error while converting request from Json");
      log.info(e.getMessage());
      e.printStackTrace();
      return new ResponseEntity<>(
          "JSON String was not in the correct format", HttpStatus.BAD_REQUEST);
    }
  }
}
