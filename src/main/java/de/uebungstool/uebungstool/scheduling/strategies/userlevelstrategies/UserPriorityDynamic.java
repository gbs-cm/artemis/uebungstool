package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class UserPriorityDynamic extends UserStrategy {

  private static final Logger log = Logger.getLogger(UserPriorityDynamic.class.getName());
  private final boolean isPreemptive;

  public UserPriorityDynamic(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    super(diagram, context);
    log.finest("Strategy: User -  PriorityDynamic");
    if (context.isLowestPriorityFirstKernel()) {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getPriority);
    } else {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getInvertedPriority);
    }
    this.isPreemptive = context.isUserSchedulingPreemptive();
    prioChangeWaiting = context.getPrioChangeWaitingKernel();
    prioChangeComputing = context.getPrioChangeComputingKernel();
  }

  @Override
  public int evaluate(int timeslot, int lastComputedCellIndex, String suffix) {
    initUserSlot(timeslot, lastComputedCellIndex, suffix);
    if (isPreemptive) {
      evaluatePreemptiveStrategy();
    } else {
      evaluateNonPreemptiveStrategy();
    }
    return Math.min(
        this.lastComputedCellIndex, initialLastComputedCellIndex + diagram.getTimeSliceLength());
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: User -  PriorityDynamic - This function should not be called!!!");
  }

  /**
   * evaluates a non preemptive Scheduling strategy and fills in the correction, color and
   * annotation attribute of the Diagram object.
   */
  @Override
  protected void evaluateNonPreemptiveStrategy() {
    while (arrivalOrder.size() + waitingThreads.size() > 0
        && lastComputedCellIndex - initialLastComputedCellIndex < diagram.getTimeSliceLength()) {
      if (actThread == null
          || actThread.getComputingTime() <= 0
          || actThread.getDeadline() <= lastComputedCellIndex
          || actThread.getStartTime() > lastComputedCellIndex) {
        if (!prepareNextThreadContainer()) {
          return;
        }
        alterPriorities();
        sortWaitingThreads();
        assignActThread();
      }
      if (fillUntilComputing()) {
        alterPrioritiesThread(actThread);

        if (!fillInNonPreemptiveThread()) {
          return;
        }
      }
      if (actThread.getComputingTime() < 1
          || (actThread.getDeadline() <= lastComputedCellIndex && actThread.getDeadline() > -1)) {

        arrivalOrder.remove(actThread);
        waitingThreads.remove(actThread);

        if (actThread.getDeadline() < lastComputedCellIndex && actThread.getDeadline() > -1) {
          actThread = null;
        }
      }
    }
  }

  /**
   * All priorities of waiting processes get decreased until the position of the last computed Cell
   * is reached
   */
  private void alterPriorities() {
    waitingThreads.forEach(this::alterPrioritiesThread);
  }

  /**
   * alters the priorities of the given thread until the lastcomputed index.
   *
   * @param w thread to compute priorities.
   */
  private void alterPrioritiesThread(SchedulingUserLevelThread w) {
    for (int i = w.getStartTime(); i < lastComputedCellIndex; i++) {
      if (w.getCells()[i].getCorrectionIndex() == null
          && (w.getDeadline() > i || w.getDeadline() == -1)) {
        w.addToPriority(prioChangeWaiting);
        w.getCells()[i].setCorrectionIndex(w.getPriority());
      }
    }
  }

  /**
   * Fills in all the computing cells for this specific thread. Different strategies for different
   * strategies are possible. Depending on the number of cores, this information is also added to
   * the solution.
   *
   * @param fillUntil number of cells that should be filled in
   */
  @Override
  protected void fillCellsComputing(int fillUntil) {
    for (int i = 0; i < fillUntil; i++) {
      actThread.getCells()[i + lastComputedCellIndex].setCorrected(
          SchedulingDiagramConstants.STATUS_COMPUTING, suffix);
      actThread.addToPriority(prioChangeComputing);
      actThread.getCells()[i + lastComputedCellIndex].setCorrectionIndex(actThread.getPriority());
    }
  }

  //      === Preemptive Strategy ===

  @Override
  protected void evaluatePreemptiveStrategy() {
    boolean firstIteration = true;
    while (arrivalOrder.size() + waitingThreads.size() > 0
        && lastComputedCellIndex - initialLastComputedCellIndex < diagram.getTimeSliceLength()) {
      if (prepareNextThreadContainer()) {
        if (firstIteration) {
          addPrioritiesRemainingCells();
          firstIteration = false;
        }
        sortWaitingThreads();
        computePreemptiveThread();
      } else {
        break;
      }
      recomputePriorities();
      lastComputedCellIndex++;
    }
  }

  /**
   * Adds the Priority annotation for every cell at this timeslot which is not computed at this time
   * index.
   */
  private void addPrioritiesRemainingCells() {
    lastComputedCellIndex = lastComputedCellIndex - 2;
    if (lastComputedCellIndex >= 0) {
      checkPrioritiesAtIndex(lastComputedCellIndex);
    }
    lastComputedCellIndex++;
    if (lastComputedCellIndex >= 0) {
      checkPrioritiesAtIndex(lastComputedCellIndex);
    }

    lastComputedCellIndex++;
  }

  private void checkPrioritiesAtIndex(int index) {
    waitingThreads.stream()
        .filter(
            u ->
                u.getCells()[index].getCorrectionIndex() == null
                    && (u.getDeadline() >= index || u.getDeadline() == -1)
                    && u.getStartTime() <= index)
        .forEach(
            u -> {
              u.addToPriority(prioChangeWaiting);
              u.getCells()[index].setCorrectionIndex(u.getPriority());
            });
  }

  private void recomputePriorities() {
    waitingThreads.forEach(
        w -> {
          if (w.getCells()[lastComputedCellIndex].getCorrectionIndex() == null
              && (w.getDeadline() >= lastComputedCellIndex || w.getDeadline() == -1)) {
            w.addToPriority(prioChangeWaiting);
            w.getCells()[lastComputedCellIndex].setCorrectionIndex(w.getPriority());
          }
        });
  }

  /**
   * Wrapper fot the filluntil methode to use the ame method for both strategies which are computed
   * with different indexes
   */
  @Override
  protected void fillUntilComputingPreemptive() {
    lastComputedCellIndex++;
    fillUntilComputing();
    lastComputedCellIndex--;
  }
}
