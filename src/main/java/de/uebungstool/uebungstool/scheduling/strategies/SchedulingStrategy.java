package de.uebungstool.uebungstool.scheduling.strategies;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.*;

import de.uebungstool.uebungstool.scheduling.processor.Core;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.*;

public abstract class SchedulingStrategy {

  protected SchedulingDiagram diagram;
  protected int lastComputedCellIndex;
  protected List<SchedulingUserLevelThread> arrivalOrder;
  protected ArrayList<SchedulingUserLevelThread> waitingThreads;

  protected SchedulingUserLevelThread actThread = null;
  protected Comparator<SchedulingUserLevelThread> secondaryIDSort =
      Comparator.comparingInt(SchedulingUserLevelThread::getId);
  protected Comparator<SchedulingUserLevelThread> sortStrategy;

  protected List<Core> waitingCores;
  protected List<Core> computingCores;
  protected Comparator<Core> waitingCoresComparator;
  protected Core actCore;

  // Attributes which might be different between the kernel and the user strategy
  protected boolean isSchedulerTime;
  protected double prioChangeComputing;
  protected double prioChangeWaiting;
  protected boolean newThreadsLastRound = false;

  protected SchedulingStrategy() {}

  protected SchedulingStrategy(SchedulingDiagram diagram) {
    this.diagram = diagram;
    lastComputedCellIndex = 0;
    isSchedulerTime = diagram.isSchedulerTime();
    this.waitingThreads = new ArrayList<>();
    arrivalOrder = diagram.accessAllUserLevelThreadsWithoutScheduler();
    arrivalOrder.sort(Comparator.comparingInt(SchedulingUserLevelThread::getStartTime));
    lastComputedCellIndex =
        arrivalOrder.get(0).getStartTime(); // fewer iterations in assign act thread necessary
    initCores();
  }

  /** Initializes the two arraylists for the Computing Core. */
  protected void initCores() {
    waitingCoresComparator = Comparator.comparingInt(Core::getCoreID);

    waitingCores = new ArrayList<>();
    computingCores = new ArrayList<>();
    for (int i = 0; i < diagram.getCoreNumber(); i++) {
      waitingCores.add(new Core(i));
    }
  }

  public abstract void evaluate();

  /**
   * evaluates a non preemptive Scheduling strategy and fills in the correction, color and
   * annotation attribute of the Diagram object.
   */
  protected void evaluateNonPreemptiveStrategy() {
    while (arrivalOrder.size() + waitingThreads.size() > 0) {
      prepareNextThreadNon();
      sortWaitingThreads();
      if (!waitingCores.isEmpty()) {
        waitingCores.sort(waitingCoresComparator);
      }
      int lastComputedCellIndexBackup = lastComputedCellIndex;

      if (assignActThread()) {
        fillUntilComputing();
        computeNonPreemptiveThread(actThread.getComputingTime());
        lastComputedCellIndex = lastComputedCellIndexBackup;
      }
    }
  }

  /**
   * sorts the waiting threads by their priority. threas with the same sorting value must be sorted
   * by their ids. The waiting threads must not be sorted without sorting the ids.
   */
  protected void sortWaitingThreads() {
    waitingThreads.sort(secondaryIDSort);
    waitingThreads.sort(sortStrategy);
  }

  /**
   * Adds the KernellevelScheduler and dispatcher time to the diagram. Decides whether they are
   * needed with the configuration and also respects deadlines and Threads in the same Userspace.
   *
   * @return true if the next thread was actually dispatched.
   */
  protected boolean addSchedulerDispatcher() {
    addScheduling();
    if (diagram.isDispatcherTime()) {
      return addDispatching();
    } else {
      return true;
    }
  }

  /** adds a time unit for the scheduler if necessary in the configuration. */
  private void addScheduling() {
    if (isSchedulerTime) {
      if (actThread == null) {
        diagram.useSystemAtTimeslot(lastComputedCellIndex);
        lastComputedCellIndex++;
      } else {
        if (!(actThread.getDeadline() > -1 && actThread.getDeadline() <= lastComputedCellIndex)) {
          diagram.useSystemAtTimeslot(lastComputedCellIndex);
          lastComputedCellIndex++;
        }
      }
    }
  }

  /**
   * adds one timeslot of dispatching time if necessary for the thread change
   *
   * @return true if the dispatcher was used as anticipated.
   */
  private boolean addDispatching() {
    if (actThread == null
        || actCore.getLastParentID() != actThread.getParentID()
            && actThread.getParentID() >= 0
            && (actThread.getDeadline() == -1 || lastComputedCellIndex < actThread.getDeadline())) {
      if (diagram.isPreemptive()) {
        updateWaitingUserThreadsArrivalOrder();
        sortWaitingThreads();
        if (actThread.equals(waitingThreads.get(0))) {
          diagram.useSystemAtTimeslot(lastComputedCellIndex);
          lastComputedCellIndex++;
          return true;
        } else {
          return addSchedulerDispatcher();
        }
      } else {
        diagram.useSystemAtTimeslot(lastComputedCellIndex);
        lastComputedCellIndex++;
        return true;
      }
    } else {
      return actCore.getLastParentID() == actThread.getParentID();
    }
  }

  /**
   * Goes through the diagram until at least on process is ready to be computed and can be added to
   * the list of waiting threads. Increments the last computed index, until at least one thread is
   * ready to be computed and at least one core is ready to compute said thread.
   */
  protected void prepareNextThreadNon() {
    updateWaitingUserThreadsArrivalOrder();
    updateWaitingUserThreadsComputingCores();
    while (waitingThreads.isEmpty() || waitingCores.isEmpty()) {
      lastComputedCellIndex++;
      updateWaitingUserThreadsArrivalOrder();
      updateWaitingUserThreadsComputingCores();
    }
    actCore = waitingCores.get(0);
  }

  /**
   * Goes through the diagram until at least on process is ready to be computed and can be added to
   * the list of waiting threads.
   */
  protected boolean prepareNextThreadPreemptive() {
    updateWaitingUserThreadsArrivalOrder();

    removeThreadsWithExpiredDeadline();
    while (waitingThreads.isEmpty()
        || waitingCores.isEmpty()
            && lastComputedCellIndex < diagram.userLevelThreads[0].getCells().length) {
      lastComputedCellIndex++;
      updateWaitingUserThreadsArrivalOrder();
      removeThreadsWithExpiredDeadline();
      if (waitingThreads.isEmpty()) {
        return false;
      }
    }
    if (diagram.getStrategy() == STRATEGIES_RR && !diagram.isOwnUserLevelScheduler()) {
      newThreadsLastRound = false;
    } else {
      newThreadsLastRound =
          waitingThreads.stream().anyMatch(u -> u.getStartTime() == lastComputedCellIndex);
    }

    return true;
  }

  /** removes all threads with an expired deadline and computes its missing cells. */
  private void removeThreadsWithExpiredDeadline() {
    waitingThreads.stream()
        .filter(t -> t.getDeadline() > -1 && t.getDeadline() <= lastComputedCellIndex)
        .forEach(
            t -> {
              actThread = t;
              fillUntilComputingPreemptive();
            });
    waitingThreads.removeIf(t -> t.getDeadline() > -1 && t.getDeadline() <= lastComputedCellIndex);
  }

  /**
   * Assigns the first thread in the waiting threads List as the next thread to be computed Will add
   * the scheduler and dispatcher if necessary.
   */
  protected boolean assignActThread() {
    if (actThread != null) {

      SchedulingUserLevelThread actThreadBackup = actThread;
      int parentIDBackup = actCore.getLastParentID();
      actCore.setLastParentID(actThread.getParentID());
      actThread = waitingThreads.get(0);
      removeThread();
      if (!addSchedulerDispatcher()) {
        actCore.setLastThreadID(parentIDBackup);
        fillUntilComputing();
        actThread = actThreadBackup;
        return false;
      }
    } else {
      if (diagram.isInitialScheduler()) {
        addSchedulerDispatcher();
      }
      actThread = waitingThreads.get(0);
      removeThread();
    }
    return true;
  }

  /**
   * Removes a (finished) thread from the list of waiting threads. If the strategy is RR, the thread
   * will only be removed, if it has actually finished computing.
   */
  private void removeThread() {
    if (diagram.getStrategy() == STRATEGIES_RR) {
      if (diagram.getTimeSliceLength() >= actThread.getComputingTime()) {
        waitingThreads.remove(actThread);
        resetTimeSliceLength();
      }
    } else {
      waitingThreads.remove(actThread);
    }
  }

  /**
   * Extracts all threads from the arrival order, which are ready to be computed at the latest
   * timeslot. These Threads will then be added to the list of computing threads.
   */
  protected void updateWaitingUserThreadsArrivalOrder() {
    int counter = 0;
    int prevWaitingSize = waitingThreads.size();
    SchedulingUserLevelThread prevThread;

    if (waitingThreads.isEmpty()) {
      prevThread = null;
    } else {
      prevThread = waitingThreads.get(0);
    }

    while (counter < arrivalOrder.size()) {
      if (arrivalOrder.get(counter).getStartTime() <= lastComputedCellIndex) {
        if (diagram.getStrategy() == STRATEGIES_RR && !diagram.isOwnUserLevelScheduler()) {
          int index = 0;
          if (!waitingThreads.isEmpty()) {
            if (diagram.isSortByPid()) {
              for (int i = 0; i < waitingThreads.size(); i++) {
                if (waitingThreads.get((i) % waitingThreads.size()).getId()
                        < arrivalOrder.get(counter).getId()
                    && arrivalOrder.get(counter).getId()
                        < waitingThreads.get((i + 1) % waitingThreads.size()).getId()) {
                  index = i + 1;
                }
              }
            } else {
              for (int i = 0; i < waitingThreads.size(); i++) {
                if (waitingThreads.get((i) % waitingThreads.size()).getStartTime()
                        < arrivalOrder.get(counter).getStartTime()
                    && arrivalOrder.get(counter).getStartTime()
                        < waitingThreads.get((i + 1) % waitingThreads.size()).getStartTime()) {
                  index = i + 1;
                }
              }
            }
            if (index == 0) {
              index = waitingThreads.size();
            }
          }
          waitingThreads.add(index, arrivalOrder.get(counter));
        } else {
          waitingThreads.add(arrivalOrder.get(counter));
        }
        arrivalOrder.remove(counter);
      } else if (arrivalOrder.get(counter).getStartTime() > lastComputedCellIndex) {
        break;
      } else {
        counter++;
      }
    }

    // Move potential thread from last round all the way to the end (otherwise just computed thread
    // will be before the newly added threads)
    if (actCore != null
        && actCore.getLastThreadID() == -1
        && prevThread != null
        && prevWaitingSize < waitingThreads.size()
        && actCore.maxRemainingTimeSlice == diagram.getTimeSliceLength()
        && diagram.getStrategy() == STRATEGIES_RR
        && !diagram.isOwnUserLevelScheduler()
        && prevThread.equals(waitingThreads.get(0))) {
      waitingThreads.remove(prevThread);
      waitingThreads.add(prevThread);
    }
  }

  /**
   * Extracts all cores from the computing cores, which are ready to compute at the latest timeslot.
   * These cores will then be added to the list of waiting cores.
   */
  protected void updateWaitingUserThreadsComputingCores() {
    int counter = 0;
    while (counter < computingCores.size()) {
      if (computingCores.get(counter).getComputingUntil() <= lastComputedCellIndex) {
        waitingCores.add(computingCores.get(counter));
        computingCores.remove(counter);

        if (computingCores.isEmpty()) {
          break;
        }
      } else {
        counter++;
      }
    }
  }

  /**
   * Sets the status "Waiting" for the act thread in every cell from the starttime until the last
   * computed index.
   *
   * @return false if a deadline interrupted the filling process. true if it was successful.
   */
  protected boolean fillUntilComputing() {
    if (actThread.getStartTime() > lastComputedCellIndex) {
      lastComputedCellIndex = actThread.getStartTime();
    } else {
      for (int i = actThread.getStartTime(); i < lastComputedCellIndex; i++) {
        if (actThread.getDeadline() <= i && actThread.getDeadline() > -1) {
          return false;
        }
        if (actThread.getCells()[i].getCorrected() == null) {
          actThread.getCells()[i].setCorrected(SchedulingDiagramConstants.STATUS_WAITING);
        }
      }
    }
    return true;
  }

  /**
   * Fills in the computing time for the act thread and sets the used core to the status computing
   * until the thread has finished computing.
   *
   * @param fillUntil amount of time units for how long the thread will compute.
   */
  protected void computeNonPreemptiveThread(int fillUntil) {
    computeNonPreemptiveThreadBody(fillUntil);

    computingCores.add(actCore);
    actCore.setLastParentID(actThread.getParentID());
    actCore.setComputingUntil(lastComputedCellIndex);
    waitingCores.remove(actCore);
  }

  /**
   * Computes one thread on one core with a non-preemptive strategy. Fills in the cell and manages
   * IO interruptions.
   *
   * @param fillUntil number of cells, which will be computed
   */
  protected void computeNonPreemptiveThreadBody(int fillUntil) {
    boolean iohappening = false;
    if (actThread.getIoAftern() > -1 && actThread.getIoAftern() < fillUntil) {
      fillUntil = actThread.getIoAftern();
      iohappening = true;
    }
    if (actThread.getDeadline() > -1
        && actThread.getDeadline() < fillUntil + lastComputedCellIndex) {
      fillUntil = actThread.getDeadline() - lastComputedCellIndex;
    }
    fillCellsComputing(fillUntil);
    lastComputedCellIndex = fillUntil + lastComputedCellIndex;
    if (iohappening) {
      actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength());
      actThread.setComputingTime(actThread.getComputingTime() - actThread.getIoAftern());
      actThread.setIoAftern(-1);
      addToArrivalOrder();
    }
  }

  /**
   * Fills in all the computing cells for this specific thread. Different strategies for different
   * strategies are possible. Depending on the number of cores, this information is also added to
   * the solution.
   *
   * @param fillUntil number of cells that should be filled in
   */
  protected void fillCellsComputing(int fillUntil) {
    if (diagram.getCoreNumber() > 1) {
      for (int i = 0; i < fillUntil; i++) {
        actThread.getCells()[i + lastComputedCellIndex].setCorrected(
            SchedulingDiagramConstants.STATUS_COMPUTING, actCore.getCoreID());
      }
    } else {
      for (int i = 0; i < fillUntil; i++) {
        actThread.getCells()[i + lastComputedCellIndex].setCorrected(
            SchedulingDiagramConstants.STATUS_COMPUTING);
      }
    }
  }

  /**
   * Includes the new thread to the arrival value and sorts the list afterward. Values must not be
   * added to the arrival order without this function. Otherwise, functions to select the next
   * thread will terminate too early.
   */
  protected void addToArrivalOrder() {
    arrivalOrder.add(actThread);
    arrivalOrder.sort(secondaryIDSort);
    arrivalOrder.sort(Comparator.comparingInt(SchedulingUserLevelThread::getStartTime));
  }

  //                        === Preemptive Strategy ===

  /**
   * Evaluates the diagram with one of the standard preemptive strategies. Simply computes all
   * threads in rounds. Each round computes one row in the diagram.
   */
  protected void evaluatePreemptiveStrategy() {
    computingCores.addAll(
        waitingCores); // Only used as a Backup mechanism in the preemptive methods

    while (arrivalOrder.size() + waitingThreads.size() > 0) {
      evaluatePreemptiveStrategyBody();
      newThreadsLastRound = false;
      lastComputedCellIndex++;
    }
  }

  /**
   * Prepares next threads and cores for the next computing round. Once they are prepared, the row
   * will be computed in additional methods.
   */
  protected void evaluatePreemptiveStrategyBody() {
    if (prepareNextThreadPreemptive()) {
      sortWaitingThreads();
      if (!waitingCores.isEmpty()) {
        waitingCores.sort(waitingCoresComparator);
      }

      if (waitingCores.size() == 1) {
        actCore = waitingCores.get(0);
        actThread = waitingThreads.get(0);
        computePreemptiveThread(actCore, false);
      } else {
        evaluatePreemptiveStrategyTimeSlot();

        waitingCores.clear();
        waitingCores.addAll(computingCores);
      }
    }
  }

  /** Evaluates one timeslot for multiple cores and threads */
  protected void evaluatePreemptiveStrategyTimeSlot() {
    List<SchedulingUserLevelThread>
        actComputingThreads; // All threads which will be computed in the next iteration

    if (waitingCores.size() < waitingThreads.size()) {
      actComputingThreads = new ArrayList<>(waitingThreads.subList(0, waitingCores.size()));
    } else {
      actComputingThreads = new ArrayList<>(waitingThreads);
    }
    actComputingThreads.forEach(SchedulingUserLevelThread::unmark);

    for (SchedulingUserLevelThread u : actComputingThreads) {
      Optional<Core> computingCore =
          waitingCores.stream().filter(c -> c.getLastThreadID() == u.getId()).findFirst();

      if (computingCore.isPresent()) {
        actThread = u;
        actThread.markForRound();
        computePreemptiveThread(computingCore.get(), true);
        waitingCores.remove(computingCore.get());
      }
    }

    actComputingThreads.removeIf(SchedulingUserLevelThread::getMark);

    for (int i = 0; i < Math.min(waitingCores.size(), actComputingThreads.size()); i++) {
      actThread = actComputingThreads.get(i);
      computePreemptiveThread(waitingCores.get(i), true);
    }
  }

  /**
   * Commputes the global actthread on the given core for this round. Three different scenarios are
   * possible: 1. the thread is already in the context of the core -> the thread can be computed 2.
   * The Thread must be scheduled 3. The thread was scheduled and now needs to be dispatched.
   *
   * @param actCore Core on which the thread will be computed
   * @param multiCore false if the system has only one core.
   */
  protected void computePreemptiveThread(Core actCore, boolean multiCore) {

    if (actCore.isFirstRound()) { // Add a scheduler to the first round if necessary
      if (!diagram.isInitialScheduler()) {
        actCore.setLastThreadID(actThread.getId());
        actCore.setLastParentID(actThread.getParentID());
        newThreadsLastRound = false;
        resetTimeSliceLength();
      }
      actCore.deactivateFirstRound();
    }

    if (actCore.getLastThreadID() == actThread.getId()) { // next cell will be computed
      if ((newThreadsLastRound) && isSchedulerTime) {
        diagram.useSystemAtTimeslot(lastComputedCellIndex); // additional scheduler time for bugfix
        if (multiCore) {
          actThread.getCells()[lastComputedCellIndex].setCorrected(
              STATUS_WAITING, actCore.getCoreID());
        } else {
          actThread.getCells()[lastComputedCellIndex].setCorrected(
              SchedulingDiagramConstants.STATUS_WAITING);
        }
      } else {
        preemptiveComputing(multiCore);
      }
    } else if (actCore.nextDispatcherNeeded() == lastComputedCellIndex
        && actCore.getNextDispatchedThreadid() == actThread.getId()) { // Use Dispatcher
      if (newThreadsLastRound && isSchedulerTime) {
        diagram.useSystemAtTimeslot(lastComputedCellIndex); // additional scheduler time for bugfix
        actCore.setNextDispatcherNeeded(lastComputedCellIndex + 1);
        if (multiCore) {
          actThread.getCells()[lastComputedCellIndex].setCorrected(
              STATUS_WAITING, actCore.getCoreID());
        } else {
          actThread.getCells()[lastComputedCellIndex].setCorrected(
              SchedulingDiagramConstants.STATUS_WAITING);
        }
      } else {
        preemptiveDispatching();
      }
    } else { // Use Scheduler
      preemptiveScheduling(multiCore);
    }
  }

  /**
   * sets the next cell as computing and manages the possibility that the thread has finished
   * computing or is interrupted for IO
   *
   * @param multiCore whether there is only one core ore more.
   */
  private void preemptiveComputing(boolean multiCore) {
    if (multiCore) {
      actThread.getCells()[lastComputedCellIndex].setCorrected(
          SchedulingDiagramConstants.STATUS_COMPUTING, actCore.getCoreID());
    } else {
      actThread.getCells()[lastComputedCellIndex].setCorrected(
          SchedulingDiagramConstants.STATUS_COMPUTING);
    }
    if (actThread.decrementComputingTime()) {
      waitingThreads.remove(actThread);
      resetTimeSliceLength();
    } else {
      actThread.decrementIOAfter();
      actCore.decrementMaxRemainingTimeSlice();

      if (actThread.getIoAftern() == 0) {
        actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength() + 1);
        waitingThreads.remove(actThread);
        resetTimeSliceLength();
        addToArrivalOrder();
        actThread.decrementIOAfter();
      } else if (actCore.maxRemainingTimeSlice == 0) {
        waitingThreads.add(waitingThreads.get(0));
        waitingThreads.remove(0);
        resetTimeSliceLength();
        actCore.setLastThreadID(-1); // scheduler must be active for one round
      }
    }
    if (diagram.getStrategy() == STRATEGIES_PRIORITY_DYNAMIC) {
      actThread.addToPriority(prioChangeComputing);
      actThread.getCells()[lastComputedCellIndex].setCorrectionIndex(actThread.getPriority());
    }
  }

  private void resetTimeSliceLength() {
    if (diagram.getStrategy() == STRATEGIES_RR && !diagram.isOwnUserLevelScheduler()) {
      actCore.maxRemainingTimeSlice = diagram.getTimeSliceLength();
    }
  }

  /** Dispatches the next thread for the actCore. */
  private void preemptiveDispatching() {
    if (actCore.getLastParentID() != actThread.getParentID()) {
      diagram.useSystemAtTimeslot(lastComputedCellIndex);
    }
    actCore.setLastThreadID(actThread.getId());
    actCore.setLastParentID(actThread.getParentID());
    fillUntilComputingPreemptive();
  }

  /**
   * Schedules the nex thread on the actcore. Prepares the dispatching for the next timeslot if
   * needed
   *
   * @param multiCore whether there is only one core ore more.
   */
  private void preemptiveScheduling(boolean multiCore) {
    if (diagram.isDispatcherTime() && actThread.getParentID() != actCore.getLastParentID()) {
      if (diagram.isSchedulerTime()) {
        actCore.setNextDispatcherNeeded(lastComputedCellIndex + 1);
      } else {
        actCore.setNextDispatcherNeeded(lastComputedCellIndex);
      }
      actCore.setNextDispatchedThreadID(actThread.getId());
      actCore.setLastThreadID(-5);
    } else {
      actCore.setLastThreadID(actThread.getId());
      fillUntilComputingPreemptive();
    }
    if (isSchedulerTime) {
      diagram.useSystemAtTimeslot(lastComputedCellIndex);
    } else {
      computePreemptiveThread(actCore, multiCore);
    }
  }

  /**
   * Wrapper fot the fill until methode to use the same method for both strategies which are
   * computed with different indexes
   */
  protected void fillUntilComputingPreemptive() {
    lastComputedCellIndex++;
    fillUntilComputing();
    lastComputedCellIndex--;
  }
}
