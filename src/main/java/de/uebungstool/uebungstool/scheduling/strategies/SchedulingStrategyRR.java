package de.uebungstool.uebungstool.scheduling.strategies;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.STRATEGIES_PRIORITY_DYNAMIC;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class SchedulingStrategyRR extends SchedulingStrategy {

  private static final Logger log = Logger.getLogger(SchedulingStrategyRR.class.getName());

  protected List<SchedulingKernelLevelThread> clockOrder;
  private int clockIndex = 0;
  private SchedulingKernelLevelThread actKernel = null;

  public SchedulingStrategyRR(SchedulingDiagram diagram) {
    super(diagram);
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: RR");
    if (diagram.isOwnUserLevelScheduler()) {
      buildClock();
      evaluateNonPreemptiveStrategy();
    } else {
      sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getPositionInQueue);
      evaluatePreemptiveStrategy();
    }
  }

  /**
   * sorts the waiting threads by their priority. threads with the same sorting value must be sorted
   * by their ids. The waiting threads must not be sorted without sorting the ids.
   */
  @Override
  protected void sortWaitingThreads() {
    if (diagram.isOwnUserLevelScheduler()) {
      waitingThreads.sort(secondaryIDSort);
      waitingThreads.sort(sortStrategy);
    }
  }

  /** Builds the clock before scheduling all processes. */
  private void buildClock() {
    clockOrder =
        diagram
            .accessAllKernelLevelThreadsWithoutScheduler(); // get all kernellevelthreads -> sort by
    // arrivaltime

    for (SchedulingUserLevelThread u : arrivalOrder) {
      Optional<SchedulingKernelLevelThread> optional =
          clockOrder.stream().filter(k -> k.getKernelthreadID() == u.getParentID()).findFirst();
      if (optional.isPresent()) {
        optional.get().addToUserLevelThreads(u);
        optional.get().alterComputingTimeSum(u.getComputingTime());
      }
    }

    clockOrder.forEach(SchedulingKernelLevelThread::initStartTime);
    if (!diagram.sortByPid) {
      clockOrder.sort(Comparator.comparingInt(SchedulingKernelLevelThread::getKernelthreadID));
      clockOrder.sort(Comparator.comparingInt(SchedulingKernelLevelThread::getStartTime));
    } else { // only one sorting strategy necessary -> every id is unique
      clockOrder.sort(Comparator.comparingInt(SchedulingKernelLevelThread::getKernelthreadID));
    }
    clockOrder.stream()
        .filter(c -> c.getUserLevelThreads().size() > 1)
        .forEach(c -> c.initUserLevelStrategy(diagram));
  }

  /**
   * evaluates a non preemptive Scheduling strategy and fills in the correction, color and
   * annotation attribute of the Diagram object.
   */
  @Override
  protected void evaluateNonPreemptiveStrategy() {

    while (!clockOrder.isEmpty()) {
      prepareCores();

      int lastComputedCellIndexBackup = this.lastComputedCellIndex;
      if (!assignActThread()) {
        return;
      }

      if (!actKernel.getUserLevelThreads().isEmpty()) {
        if (actKernel.getUserlevelStrategy() == null) {
          int fillUntil = Math.min(actKernel.getComputingTimeSum(), diagram.getTimeSliceLength());
          actThread = actKernel.getUserLevelThreads().get(0);
          if (fillUntilComputing()) {
            fillActUserThread(fillUntil, actKernel);
          }
        } else {
          computeActKernelThread(actKernel, diagram.getTimeSliceLength());
          computingCores.add(actCore);
          actCore.setComputingUntil(lastComputedCellIndex);
          waitingCores.remove(actCore);
        }
      }

      maintainClockOrder();
      this.lastComputedCellIndex = lastComputedCellIndexBackup;
    }
  }

  /**
   * deletes a thread from the clock order once it has finished computing Also adapts the index in
   * the lIst accordingly.
   */
  private void maintainClockOrder() {
    if (actKernel.getComputingTimeSum() <= 0) {
      clockOrder.remove(actKernel);
      if (0 == clockIndex) {
        clockIndex = clockOrder.size() - 1;
      } else {
        clockIndex--;
      }
    }
    if (clockOrder.size() > 1) {
      clockIndex = (clockIndex + 1) % clockOrder.size();
    }
  }

  /**
   * etermines all available cores for the next timeslot. If no core is available, hte last compted
   * cellindex gets incremented, until a core is available.
   */
  private void prepareCores() {
    updateWaitingUserThreadsComputingCores();
    addPrioritiesRemainingCells();
    while (waitingCores.isEmpty()) {
      lastComputedCellIndex++;
      updateWaitingUserThreadsComputingCores();
      addPrioritiesRemainingCells();
    }
    if (waitingCores.size() > 1) {
      waitingCores.sort(waitingCoresComparator);
    }
    actCore = waitingCores.get(0);
  }

  /**
   * Adds the Priority annotation for every cell at this timeslot which is not computed at this time
   * index.
   */
  private void addPrioritiesRemainingCells() {
    if (lastComputedCellIndex > 0) {
      lastComputedCellIndex--;

      clockOrder.stream()
          .filter(
              k -> k.getStrategy() == STRATEGIES_PRIORITY_DYNAMIC && k.isUserSchedulingPreemptive())
          .forEach(
              k ->
                  k.getUserLevelThreads().stream()
                      .filter(
                          u ->
                              u.getCells()[lastComputedCellIndex].getCorrectionIndex() == null
                                  && u.getStartTime() <= lastComputedCellIndex
                                  && (u.getDeadline() > lastComputedCellIndex
                                      || u.getDeadline() == -1)
                                  && u.getComputingTime() > 0)
                      .forEach(
                          u -> {
                            u.addToPriority(k.getPrioChangeWaitingKernel());
                            u.getCells()[lastComputedCellIndex].setCorrectionIndex(u.getPriority());
                          }));
      lastComputedCellIndex++;
    }
  }

  /**
   * Selects the next comptuing thread. the clockorder is iterated through until the first element
   * is reached again. If therefore no thread was ready to be computed, the lastcomputed cellindex
   * is incremented until a thread is ready to be computed.
   */
  @Override
  protected boolean assignActThread() {
    updateClockOrder();
    if (clockOrder.isEmpty()) {
      return false;
    }

    int iterator = 0;
    while (clockOrder.get(clockIndex).getStartTime() > lastComputedCellIndex
        && iterator <= clockOrder.size()) {
      clockIndex = (clockIndex + 1) % clockOrder.size();
      iterator++;
    }

    if (iterator > clockOrder.size() && !clockOrder.isEmpty()) {
      lastComputedCellIndex++;
      assignActThread();
    } else {
      prepareSchedulerRR();
    }

    return true;
  }

  /**
   * Removes all expired threads form the clock -> removes all threads which deadline was already.
   * clockindex gets updated accordingly.
   */
  private void updateClockOrder() {
    clockOrder.forEach(k -> k.cleanOutdatedUserLevelthreads(lastComputedCellIndex));
    int iterator = 0;
    int upperBound = clockIndex;
    while (iterator < upperBound) {
      if (clockOrder.get(iterator).getComputingTimeSum() <= 0) {
        clockIndex--;
      }
      iterator++;
    }
    clockOrder.removeIf(k -> k.getComputingTimeSum() <= 0);
    if (clockOrder.size() <= clockIndex) {
      clockIndex = clockOrder.size() - 1;
    }
  }

  /**
   * determines the actKernellevelThread as well as the actCore. Adds the scheduler/Dispatcher for
   * the next thread. If it is the first thread in this core and the initial scheduler is not
   * activated, the values are just adapted.
   */
  private void prepareSchedulerRR() {
    if (actKernel != null || diagram.isInitialScheduler()) {
      diagram.setInitialScheduler(false);
      if (actKernel != null) {
        actCore.setLastParentID(actKernel.getKernelthreadID());
      }
      actKernel = clockOrder.get(clockIndex);
      actThread = actKernel.getUserLevelThreads().get(0);
      actCore.setThisParentID(actKernel.getKernelthreadID());
      addSchedulerDispatcher();
    } else {
      actKernel = clockOrder.get(clockIndex);
      actCore.setThisParentID(actKernel.getKernelthreadID());
    }
  }

  /**
   * A wrapper for the computeNonPreemptiveThreadBody Method from SchedulingStrategy. Adapts the
   * remaining computing time for the thread.
   *
   * @param fillUntil Number of cells that should be filled in
   * @param actKernel act kernellevelthread
   */
  private void fillActUserThread(int fillUntil, SchedulingKernelLevelThread actKernel) {
    int remainingComputingTime = actThread.getComputingTime();

    computeNonPreemptiveThreadBody(fillUntil);

    if (actThread.getComputingTime() == remainingComputingTime) { // no io
      actKernel.alterComputingTimeSum(-1 * fillUntil);
      actThread.setComputingTime(actKernel.getComputingTimeSum());

      if (actThread.getIoAftern() >= diagram.getTimeSliceLength()) {
        actThread.setIoAftern(actThread.getIoAftern() - fillUntil);
      }
      if (actThread.getIoAftern() == 0) {

        actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength());
        actThread.setIoAftern(-1);
        manageIO();
      }
    } else {
      manageIO();
    }
    actCore.setComputingUntil(lastComputedCellIndex);

    computingCores.add(actCore);
    waitingCores.remove(actCore);
  }

  /** Manages all remaining times after an io interruption */
  private void manageIO() {
    actKernel.setStartTime(actThread.getStartTime());
    actThread.getCells()[actKernel.getStartTime() - 1].setCorrected("");
    actKernel.setComputingTimeSum(actThread.getComputingTime());
  }

  /**
   * Initiates that the Userlevelscheduler computes filluntill many cells.
   *
   * @param actKernel act kernellevelthread
   * @param fillUntil number of cells that should be computed.
   */
  private void computeActKernelThread(SchedulingKernelLevelThread actKernel, int fillUntil) {
    if (diagram.getCoreNumber() > 1) {
      lastComputedCellIndex =
          actKernel.computeTimeSlice(
              fillUntil, lastComputedCellIndex, "" + actKernel.getKernelthreadID());
    } else {
      lastComputedCellIndex = actKernel.computeTimeSlice(fillUntil, lastComputedCellIndex, "");
    }
    actKernel.recomputeComputingTimeSum();
  }
}
