package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategySJF extends SchedulingStrategy {

  private static final Logger log = Logger.getLogger(SchedulingStrategySJF.class.getName());

  public SchedulingStrategySJF(SchedulingDiagram diagram) {
    super(diagram);
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: SJF");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getComputingTime);
    evaluateNonPreemptiveStrategy();
  }
}
