package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategyFCFS extends SchedulingStrategy {

  private static final Logger log = Logger.getLogger(SchedulingStrategyFCFS.class.getName());

  public SchedulingStrategyFCFS(SchedulingDiagram diagram) {
    super(diagram);
  }

  /*
  Can be more efficient with it's own implementation -> unecessary copying between the two lists and unnecessary sortng of the second list
   */
  @Override
  public void evaluate() {
    log.finest("Strategy: FCFS");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getStartTime);
    evaluateNonPreemptiveStrategy();
  }
}
