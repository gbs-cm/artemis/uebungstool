package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

public class UserRR extends UserStrategy {

  private static final Logger log = Logger.getLogger(UserRR.class.getName());
  private int clockIndex = 0;
  private final int timeSliceLength;
  protected List<SchedulingUserLevelThread> clockOrder;
  private int remainingForNextRound = 0;

  public UserRR(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    super(diagram, context);
    log.finest("Strategy: User -  RR");
    timeSliceLength = context.getUserlevelTimeSliceLength();
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getComputingTime);
    buildClock(context.isUserlevelSortByPid());
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: User -  EDF - This function should not be called!!!");
  }

  @Override
  public int evaluate(int timeslot, int lastComputedCellIndex, String suffix) {
    initUserSlot(timeslot, lastComputedCellIndex, suffix);
    evaluateNonPreemptiveStrategy();
    return Math.min(
        this.lastComputedCellIndex, initialLastComputedCellIndex + diagram.getTimeSliceLength());
  }

  /** Builds the clock before scheduling all processes. */
  private void buildClock(boolean isSortedByPid) {
    clockOrder = arrivalOrder;

    clockOrder.sort(Comparator.comparingInt(SchedulingUserLevelThread::getId));
    if (isSortedByPid) {
      clockOrder.sort(Comparator.comparingInt(SchedulingUserLevelThread::getStartTime));
    }
  }

  @Override
  protected void evaluateNonPreemptiveStrategy() {

    while (!clockOrder.isEmpty()
        && lastComputedCellIndex - initialLastComputedCellIndex < diagram.getTimeSliceLength()) {

      updateClockOrder();
      if (clockOrder.stream().noneMatch(u -> u.getStartTime() <= lastComputedCellIndex)) {
        return;
      }
      if ((remainingForNextRound <= 0 || actThread != clockOrder.get(clockIndex))
          && !assignActThread()) {
        return;
      }

      int fillUntil = computeFillUntil();

      if (fillUntilComputing()) {
        fillActUserThread(fillUntil);
      }
      if (remainingForNextRound == 0) {
        maintainClockOrder();
      }
    }
  }

  /**
   * Computes the next number of cell that are computed, additionally saves any timeunits which
   * remain from the user time slice due to an expired kernel time slice.
   *
   * @return number of cells to compute
   */
  private int computeFillUntil() {
    int fillUntil;
    if (remainingForNextRound <= 0) {
      fillUntil = Math.min(actThread.getComputingTime(), timeSliceLength);
    } else {
      fillUntil = Math.min(remainingForNextRound, timeSliceLength);
      fillUntil = Math.min(actThread.getComputingTime(), fillUntil);
    }

    int remainingTime =
        diagram.getTimeSliceLength() - (lastComputedCellIndex - initialLastComputedCellIndex);
    if (remainingTime < fillUntil) {
      if (actThread.getIoAftern() != -1 && actThread.getIoAftern() <= remainingTime) {
        remainingForNextRound = 0;
      } else {
        this.remainingForNextRound = fillUntil - remainingTime;
        fillUntil = remainingTime;
      }
    } else {
      remainingForNextRound = 0;
    }
    return fillUntil;
  }

  /**
   * deletes a thread from the clock order once it has finished computing Also adapts the index in
   * the lIst accordingly.
   */
  private void maintainClockOrder() {
    if (actThread.getComputingTime() <= 0) {
      clockOrder.remove(actThread);
      if (0 == clockIndex) {
        clockIndex = clockOrder.size() - 1;
      } else {
        clockIndex--;
      }
    }
    if (clockOrder.size() > 1) {
      clockIndex = (clockIndex + 1) % clockOrder.size();
    }
  }

  /**
   * Selects the next computing thread. the clockorder is iterated through until the first element
   * is reached again. If therefore no thread was ready to be computed, the lastcomputed cellindex
   * is incremented until a thread is ready to be computed.
   */
  @Override
  protected boolean assignActThread() {
    if (clockOrder.isEmpty()) {
      return false;
    }

    int iterator = 0;
    while (clockOrder.get(clockIndex).getStartTime() > lastComputedCellIndex
        && iterator <= clockOrder.size()) {
      clockIndex = (clockIndex + 1) % clockOrder.size();
      iterator++;
    }

    if (iterator > clockOrder.size() && !clockOrder.isEmpty()) {
      return false;
    } else {
      prepareSchedulerRR();
    }

    return true;
  }

  /**
   * Removes all expired threads form the clock -> removes all threads which deadline was already.
   * clockindex gets updated accordingly.
   */
  private void updateClockOrder() {
    int iterator = 0;
    int upperBound = clockIndex;
    while (iterator < upperBound) {
      if (clockOrder.get(iterator).getComputingTime() <= 0
          || (clockOrder.get(iterator).getDeadline() > -1
              && clockOrder.get(iterator).getDeadline() <= lastComputedCellIndex)) {
        clockIndex--;
      }
      iterator++;
    }
    clockOrder.removeIf(
        k ->
            k.getComputingTime() <= 0
                || (k.getDeadline() > -1 && k.getDeadline() <= lastComputedCellIndex));
    if (!clockOrder.isEmpty() && clockOrder.size() <= clockIndex) {
      clockIndex = clockOrder.size() - 1;
    }
  }

  /**
   * determines the actKernellevelThread as well as the actCore. Adds the scheduler/Dispatcher for
   * the next thread. If it is the first thread in this core and the initial scheduler is not
   * activated, the values are just adapted.
   */
  private void prepareSchedulerRR() {
    if (actThread != null) {
      diagram.setInitialScheduler(false);
      actThread = clockOrder.get(clockIndex);
      addSchedulerDispatcher();
    } else {
      actThread = clockOrder.get(clockIndex);
    }
  }

  /**
   * A wrapper for the computeNonPreemptiveThreadBody Method from SchedulingStrategy. Adapts the
   * remaining computing time for the thread.
   *
   * @param fillUntil Number of cells that should be filled in
   */
  private void fillActUserThread(int fillUntil) {
    int remainingComputingTime = actThread.getComputingTime();

    computeNonPreemptiveThreadBody(fillUntil);

    if (actThread.getComputingTime() == remainingComputingTime) { // no io

      actThread.setComputingTime(actThread.getComputingTime() - fillUntil);
      if (actThread.getIoAftern() >= fillUntil) {
        actThread.setIoAftern(actThread.getIoAftern() - fillUntil);
      }
      if (actThread.getIoAftern() == 0) {
        actThread.setIoAftern(-1);
        manageIO();
      }
    } else {
      manageIO();
    }
  }

  /** Manages all remaining times after an io interruption */
  private void manageIO() {
    actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength());
    actThread.getCells()[actThread.getStartTime() - 1].setCorrected("");
  }

  /**
   * Includes the new thread to the arrival value and sorts the list afterwards. Values must not be
   * added to the arrival order without this function. Otherwise, functions to select the next
   * thread will terminate to early.
   */
  @Override
  protected void addToArrivalOrder() {
    // overwritten -> userRR uses the same List for the Arrival order and the clockorder -> must not
    // get sortet for any io interruption
  }
}
