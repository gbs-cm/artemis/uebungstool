package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategyEDF extends SchedulingStrategy {

  private static final Logger log = Logger.getLogger(SchedulingStrategyEDF.class.getName());

  public SchedulingStrategyEDF(SchedulingDiagram diagram) {
    super(diagram);
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: EDF");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getDeadline);
    if (diagram.isPreemptive()) {
      evaluatePreemptiveStrategy();
    } else {
      evaluateNonPreemptiveStrategy();
    }
  }
}
