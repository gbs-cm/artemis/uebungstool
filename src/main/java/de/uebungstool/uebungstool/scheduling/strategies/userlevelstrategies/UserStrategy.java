package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.STRATEGIES_PRIORITY_DYNAMIC;

import de.uebungstool.uebungstool.scheduling.strategies.SchedulingStrategy;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.ArrayList;
import java.util.Comparator;

public abstract class UserStrategy extends SchedulingStrategy {

  private final SchedulingKernelLevelThread context;
  protected int timeslot = 0;
  protected int initialLastComputedCellIndex = -1;
  protected String suffix;

  protected UserStrategy(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    this.diagram = diagram;
    lastComputedCellIndex = -1;
    isSchedulerTime = diagram.isUserSchedulerTime();
    this.waitingThreads = new ArrayList<>();
    arrivalOrder = new ArrayList<>(context.getUserLevelThreads());
    arrivalOrder.sort(Comparator.comparingInt(SchedulingUserLevelThread::getStartTime));
    this.context = context;
  }

  public abstract int evaluate(int timeslot, int lastComputedCellIndex, String suffix);

  /**
   * Must always be set before evaluate is called. preemptive: will have the length until the next
   * interruption nonpreemptive: timeslot will have the same length as timeslicelengthin the diagram
   */
  public void initUserSlot(int timeslot, int lastComputedCellIndex, String suffix) {
    this.suffix = suffix;
    this.timeslot = timeslot;
    this.lastComputedCellIndex = lastComputedCellIndex;
    this.initialLastComputedCellIndex = lastComputedCellIndex;
  }

  /**
   * evaluates a non preemptive Scheduling strategy and fills in the correction, color and
   * annotation attribute of the Diagram object.
   */
  @Override
  protected void evaluateNonPreemptiveStrategy() {
    while (arrivalOrder.size() + waitingThreads.size() > 0
        && lastComputedCellIndex - initialLastComputedCellIndex < diagram.getTimeSliceLength()) {
      if (actThread == null
          || actThread.getComputingTime() <= 0
          || actThread.getDeadline() <= lastComputedCellIndex
          || actThread.getStartTime() > lastComputedCellIndex) {
        if (!prepareNextThreadContainer()) {
          return;
        }
        sortWaitingThreads();
        assignActThread();
      }
      if (fillUntilComputing() && !fillInNonPreemptiveThread()) {
        return;
      }
      if (actThread.getComputingTime() < 1
          || (actThread.getDeadline() <= lastComputedCellIndex && actThread.getDeadline() > -1)) {

        arrivalOrder.remove(actThread);
        waitingThreads.remove(actThread);

        if (actThread.getDeadline() < lastComputedCellIndex && actThread.getDeadline() > -1) {
          actThread = null;
        }
      }
    }
  }

  protected boolean prepareNextThreadContainer() {
    waitingThreads.removeIf(t -> t.getDeadline() > -1 && t.getDeadline() <= lastComputedCellIndex);
    arrivalOrder.removeIf(t -> t.getDeadline() > -1 && t.getDeadline() <= lastComputedCellIndex);

    updateWaitingUserThreadsArrivalOrder();
    if (waitingThreads.isEmpty()) {
      if (!arrivalOrder.isEmpty()) {
        context.setStartTime(arrivalOrder.get(0).getStartTime());
      }
      return false;
    }
    newThreadsLastRound =
        waitingThreads.stream().anyMatch(u -> u.getStartTime() == lastComputedCellIndex);

    return lastComputedCellIndex - initialLastComputedCellIndex <= diagram.getTimeSliceLength();
  }

  @Override
  protected boolean assignActThread() {
    if (this.actThread != null && !this.actThread.equals(waitingThreads.get(0))) {
      addSchedulerDispatcher();
    }
    actThread = waitingThreads.get(0);
    return true;
  }

  protected boolean fillInNonPreemptiveThread() {
    int fillUntil =
        Math.min(
            actThread.getComputingTime(),
            this.timeslot - lastComputedCellIndex + initialLastComputedCellIndex);
    if (fillUntil <= 0) {
      return false;
    }

    computeNonPreemptiveThread(fillUntil);

    return true;
  }

  @Override
  protected void computeNonPreemptiveThread(int fillUntil) {
    boolean iohappening = false;
    if (actThread.getIoAftern() > -1) {
      if (actThread.getIoAftern() <= fillUntil) {
        fillUntil = actThread.getIoAftern();
        iohappening = true;
      } else {
        actThread.setIoAftern(actThread.getIoAftern() - fillUntil);
      }
    }
    if (actThread.getDeadline() > -1
        && actThread.getDeadline() < fillUntil + lastComputedCellIndex) {
      fillUntil = actThread.getDeadline() - lastComputedCellIndex;
    }
    fillCellsComputing(fillUntil);
    lastComputedCellIndex = fillUntil + lastComputedCellIndex;

    if (iohappening) {
      actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength());
      actThread.setComputingTime(actThread.getComputingTime() - actThread.getIoAftern());
      actThread.setIoAftern(-1);
      waitingThreads.remove(actThread);
      addToArrivalOrder();
    } else {
      actThread.setComputingTime(actThread.getComputingTime() - fillUntil);
    }
  }

  /**
   * Fills in all the computing cells for this specific thread. Different strategies for different
   * strategies are possible. Depending on the number of cores, this information is also added to
   * the solution.
   *
   * @param fillUntil number of cells that should be filled in
   */
  @Override
  protected void fillCellsComputing(int fillUntil) {
    for (int i = 0; i < fillUntil; i++) {
      actThread.getCells()[i + lastComputedCellIndex].setCorrected(
          SchedulingDiagramConstants.STATUS_COMPUTING, suffix);
    }
  }

  @Override
  protected boolean addSchedulerDispatcher() {
    if (diagram.isUserSchedulerTime()) {
      diagram.useSystemAtTimeslot(lastComputedCellIndex);
      lastComputedCellIndex++;
    }
    return true;
  }

  //   === Preemptive Strategies ===

  /**
   * Evaluates the diagram with one of the standard preemptive strategies. Simply computes all
   * threads in rounds. Each round computes one row in the diagram.
   */
  @Override
  protected void evaluatePreemptiveStrategy() {
    while (arrivalOrder.size() + waitingThreads.size() > 0
        && lastComputedCellIndex - initialLastComputedCellIndex < diagram.getTimeSliceLength()) {
      if (prepareNextThreadContainer()) {
        sortWaitingThreads();
        computePreemptiveThread();
        newThreadsLastRound = false;
      } else {
        break;
      }
      lastComputedCellIndex++;
    }
  }

  /**
   * Computes the global actthread on the given core for this round. Three different scenarios are
   * possible: 1. the thread is already in the context of the core -> the thread can be computed 2.
   * The Thread must be scheduled
   */
  protected void computePreemptiveThread() {
    if (actThread == null) {
      actThread = waitingThreads.get(0);
      newThreadsLastRound = false;
    }

    if (waitingThreads.get(0) == actThread && !newThreadsLastRound) { // next cell will be computed
      preemptiveComputing();
    } else { // Use Scheduler
      preemptiveScheduling();
    }
  }

  /**
   * sets the next cell as computing and manages the possibility that the thread has finished
   * computing or is interrupted for IO
   */
  private void preemptiveComputing() {
    fillUntilComputingPreemptive();
    actThread.getCells()[lastComputedCellIndex].setCorrected(
        SchedulingDiagramConstants.STATUS_COMPUTING);

    if (actThread.decrementComputingTime()) {
      waitingThreads.remove(actThread);
    } else {
      actThread.decrementIOAfter();

      if (actThread.getIoAftern() == 0) {
        actThread.setStartTime(lastComputedCellIndex + actThread.getIoLength() + 1);
        waitingThreads.remove(actThread);
        addToArrivalOrder();
        actThread.decrementIOAfter();
      }
    }
    if (context.getStrategy() == STRATEGIES_PRIORITY_DYNAMIC) {
      actThread.addToPriority(prioChangeComputing);
      actThread.getCells()[lastComputedCellIndex].setCorrectionIndex(actThread.getPriority());
    }
  }

  /**
   * Schedules the nex thread on the actcore. Prepares the dispatching for the next timeslot if
   * needed
   */
  private void preemptiveScheduling() {
    actThread = waitingThreads.get(0);
    if (isSchedulerTime) {
      diagram.useSystemAtTimeslot(lastComputedCellIndex);
      fillUntilComputingPreemptive();
    } else {
      preemptiveComputing();
    }
  }
}
