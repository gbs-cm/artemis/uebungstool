package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategySRTN extends SchedulingStrategy {

  private static final Logger log = Logger.getLogger(SchedulingStrategySRTN.class.getName());

  public SchedulingStrategySRTN(SchedulingDiagram diagram) {
    super(diagram);
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: SRTN");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getComputingTime);
    evaluatePreemptiveStrategy();
  }
}
