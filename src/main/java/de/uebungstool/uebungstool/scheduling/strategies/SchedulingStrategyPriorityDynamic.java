package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategyPriorityDynamic extends SchedulingStrategy {

  private static final Logger log =
      Logger.getLogger(SchedulingStrategyPriorityDynamic.class.getName());

  public SchedulingStrategyPriorityDynamic(SchedulingDiagram diagram) {
    super(diagram);
    prioChangeWaiting = diagram.getPrioChangeWaiting();
    prioChangeComputing = diagram.getPrioChangeComputing();
  }

  @Override
  public void evaluate() {

    if (diagram.isLowestPriorityFirst()) {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getPriority);
    } else {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getInvertedPriority);
    }

    log.finest(
        "Dynamic: Computing:    int: " + prioChangeComputing + "  Waiting: " + prioChangeWaiting);

    if (diagram.isPreemptive()) {
      log.finest("Strategy: Priority dynamic Preemptive");
      evaluatePreemptiveStrategy();
    } else {
      log.finest("Strategy: Priority dynamic Non-Preemptive");
      evaluateNonPreemptiveStrategy();
    }
  }

  /**
   * Evaluates the non Preemptive Strategy with dynamic priorities. Same structure as the Method
   * evaluateNonPreemptiveStrategy in Strategy, but is extended with the new, dynamic computation of
   * the Priorities.
   */
  @Override
  protected void evaluateNonPreemptiveStrategy() {
    while (arrivalOrder.size() + waitingThreads.size() > 0) {
      prepareNextThreadNon();
      alterPriorities();
      sortWaitingThreads();
      if (!waitingCores.isEmpty()) {
        waitingCores.sort(waitingCoresComparator);
      }
      int lastComputedCellIndexBackup = lastComputedCellIndex;

      SchedulingUserLevelThread nextThreadBackup = waitingThreads.get(0);
      if (assignActThread() && fillUntilComputing()) {
        alterPrioritiesThread(actThread);
        computeNonPreemptiveThread(actThread.getComputingTime());
        lastComputedCellIndex = lastComputedCellIndexBackup;
      } else {
        alterPrioritiesThread(nextThreadBackup);
      }
    }
  }

  /**
   * Fills in all the computing cells for this specific thread. Different strategies for different
   * strategies are possible. Depending on the number of cores, this information is also added to
   * the solution.
   *
   * @param fillUntil number of cells that should be filled in
   */
  @Override
  protected void fillCellsComputing(int fillUntil) {
    if (diagram.getCoreNumber() > 1) {
      for (int i = 0; i < fillUntil; i++) {
        actThread.getCells()[i + lastComputedCellIndex].setCorrected(
            SchedulingDiagramConstants.STATUS_COMPUTING, actCore.getCoreID());
        actThread.addToPriority(prioChangeComputing);
        actThread.getCells()[i + lastComputedCellIndex].setCorrectionIndex(actThread.getPriority());
      }
    } else {
      for (int i = 0; i < fillUntil; i++) {
        actThread.getCells()[i + lastComputedCellIndex].setCorrected(
            SchedulingDiagramConstants.STATUS_COMPUTING);
        actThread.addToPriority(prioChangeComputing);
        actThread.getCells()[i + lastComputedCellIndex].setCorrectionIndex(actThread.getPriority());
      }
    }
  }

  /**
   * All priorities of waiting processes get decreased until the position of the last computed Cell
   * is reached
   */
  private void alterPriorities() {
    waitingThreads.forEach(this::alterPrioritiesThread);
  }

  /**
   * alters the priorities of the given thread until the lastcomputed index.
   *
   * @param w thread to compute priorities.
   */
  private void alterPrioritiesThread(SchedulingUserLevelThread w) {
    for (int i = w.getStartTime(); i < lastComputedCellIndex; i++) {
      if (w.getCells()[i].getCorrectionIndex() == null
          && (w.getDeadline() > i || w.getDeadline() == -1)) {
        w.addToPriority(prioChangeWaiting);
        w.getCells()[i].setCorrectionIndex(w.getPriority());
      }
    }
  }

  //          === Preemptive Strategy ===

  @Override
  protected void evaluatePreemptiveStrategy() {
    computingCores.addAll(
        waitingCores); // Only used as a Backup mechanism in the preemptive methods

    while (arrivalOrder.size() + waitingThreads.size() > 0) {
      evaluatePreemptiveStrategyBody();
      recomputePriorities();
      newThreadsLastRound = false;
      lastComputedCellIndex++;
    }
  }

  /** recomputes the priorities for every waiting thread for the next round. */
  private void recomputePriorities() {
    waitingThreads.forEach(
        w -> {
          if (w.getCells()[lastComputedCellIndex].getCorrectionIndex() == null
              && (w.getDeadline() >= lastComputedCellIndex || w.getDeadline() == -1)) {
            w.addToPriority(prioChangeWaiting);
            w.getCells()[lastComputedCellIndex].setCorrectionIndex(w.getPriority());
          }
        });
  }
}
