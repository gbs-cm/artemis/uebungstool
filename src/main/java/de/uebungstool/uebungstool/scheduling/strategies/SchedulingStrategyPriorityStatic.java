package de.uebungstool.uebungstool.scheduling.strategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class SchedulingStrategyPriorityStatic extends SchedulingStrategy {

  private static final Logger log =
      Logger.getLogger(SchedulingStrategyPriorityStatic.class.getName());

  public SchedulingStrategyPriorityStatic(SchedulingDiagram diagram) {
    super(diagram);
  }

  @Override
  public void evaluate() {

    if (diagram.isLowestPriorityFirst()) {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getPriority);
    } else {
      sortStrategy = Comparator.comparingDouble(SchedulingUserLevelThread::getInvertedPriority);
    }

    log.finest("Strategy: Priority static");
    if (diagram.isPreemptive()) {
      evaluatePreemptiveStrategy();
    } else {
      evaluateNonPreemptiveStrategy();
    }
  }
}
