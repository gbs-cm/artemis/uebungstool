package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class UserEDF extends UserStrategy {

  private static final Logger log = Logger.getLogger(UserEDF.class.getName());
  private final boolean isPreemptive;

  public UserEDF(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    super(diagram, context);
    log.finest("Strategy: User -  EDF");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getDeadline);
    this.isPreemptive = context.isUserSchedulingPreemptive();
  }

  @Override
  public int evaluate(int timeslot, int lastComputedCellIndex, String suffix) {
    initUserSlot(timeslot, lastComputedCellIndex, suffix);
    if (isPreemptive) {
      evaluatePreemptiveStrategy();
    } else {
      evaluateNonPreemptiveStrategy();
    }
    return Math.min(
        this.lastComputedCellIndex, initialLastComputedCellIndex + diagram.getTimeSliceLength());
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: User -  EDF - This function should not be called!!!");
  }
}
