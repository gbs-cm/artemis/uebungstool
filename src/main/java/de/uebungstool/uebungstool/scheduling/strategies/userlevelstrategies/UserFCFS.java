package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class UserFCFS extends UserStrategy {

  private static final Logger log = Logger.getLogger(UserFCFS.class.getName());

  public UserFCFS(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    super(diagram, context);
    log.finest("Strategy: User -  FCFS");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getStartTime);
  }

  @Override
  public int evaluate(int timeslot, int lastComputedCellIndex, String suffix) {
    initUserSlot(timeslot, lastComputedCellIndex, suffix);
    evaluateNonPreemptiveStrategy();
    return Math.min(
        this.lastComputedCellIndex, initialLastComputedCellIndex + diagram.getTimeSliceLength());
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: User -  FCFS - This function should not be called!!!");
  }
}
