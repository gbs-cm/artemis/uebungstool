package de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies;

import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Comparator;
import java.util.logging.Logger;

public class UserSRTN extends UserStrategy {

  private static final Logger log = Logger.getLogger(UserSRTN.class.getName());

  public UserSRTN(SchedulingDiagram diagram, SchedulingKernelLevelThread context) {
    super(diagram, context);
    log.finest("Strategy: User -  SRTN");
    sortStrategy = Comparator.comparingInt(SchedulingUserLevelThread::getComputingTime);
  }

  @Override
  public int evaluate(int timeslot, int lastComputedCellIndex, String suffix) {
    initUserSlot(timeslot, lastComputedCellIndex, suffix);
    evaluatePreemptiveStrategy();

    return Math.min(
        this.lastComputedCellIndex, initialLastComputedCellIndex + diagram.getTimeSliceLength());
  }

  @Override
  public void evaluate() {
    log.finest("Strategy: User -  SJF - This function should not be called!!!");
  }
}
