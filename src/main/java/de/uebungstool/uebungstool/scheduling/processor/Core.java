package de.uebungstool.uebungstool.scheduling.processor;

public class Core {

  public int maxRemainingTimeSlice = -1;
  private int computingUntil;
  private final int coreID;
  private int lastParentID = -2;
  private int thisParentID = -3;
  private boolean firstRound = true;
  private int lastThreadID = -1;
  private int nextDispatcherNeeded = -1;
  private int nextDispatchedThreadid = -1;

  public Core(int coreID) {
    this.coreID = coreID;
  }

  public int getComputingUntil() {
    return computingUntil;
  }

  public void setComputingUntil(int computingUntil) {
    this.computingUntil = computingUntil;
  }

  public int getCoreID() {
    return coreID;
  }

  public void setLastParentID(int parentID) {
    this.lastParentID = parentID;
  }

  public int getLastParentID() {
    return lastParentID;
  }

  public boolean isFirstRound() {
    return firstRound;
  }

  public void deactivateFirstRound() {
    firstRound = false;
  }

  public int getLastThreadID() {
    return lastThreadID;
  }

  public void setLastThreadID(int lastThreadID) {
    this.lastThreadID = lastThreadID;
  }

  public void setNextDispatcherNeeded(int nextDispatcherNeeded) {
    this.nextDispatcherNeeded = nextDispatcherNeeded;
  }

  /**
   * Returns a cellindex, where the dispatcher should be active
   *
   * @return
   */
  public int nextDispatcherNeeded() {
    // nicht als boolean speichern, da ansonsten nicht getestet werden kann, ob mittendrinnen noch
    // ein anderer dranwar -> so wird der Wert ungültig, wenn es nicht dem lastcomputedcellindex
    // entspricht.
    return nextDispatcherNeeded;
  }

  public int getThisParentID() {
    return thisParentID;
  }

  public void setThisParentID(int thisParentID) {
    this.thisParentID = thisParentID;
  }

  public void setFirstRound(boolean firstRound) {
    this.firstRound = firstRound;
  }

  public void setNextDispatchedThreadID(int id) {
    this.nextDispatchedThreadid = id;
  }

  public int getNextDispatchedThreadid() {
    return nextDispatchedThreadid;
  }

  public void decrementMaxRemainingTimeSlice() {
    maxRemainingTimeSlice--;
  }
}
