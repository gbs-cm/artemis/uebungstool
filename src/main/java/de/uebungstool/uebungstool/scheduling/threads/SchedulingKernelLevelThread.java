package de.uebungstool.uebungstool.scheduling.threads;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.uebungstool.uebungstool.scheduling.strategies.userlevelstrategies.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SchedulingKernelLevelThread extends SchedulingGenericThread {

  @JsonProperty("kernelthreadID")
  private int kernelthreadID;

  @JsonProperty("userStrategy")
  private int strategy;

  @JsonProperty("startTime")
  private int startTime;

  @JsonProperty("userlevelTimeSliceLength")
  private int userlevelTimeSliceLength;

  @JsonProperty("lowestPriorityFirstKernel")
  private boolean lowestPriorityFirstKernel;

  @JsonProperty("prioChangeComputingKernel")
  private double prioChangeComputingKernel;

  @JsonProperty("prioChangeWaitingKernel")
  private double prioChangeWaitingKernel;

  @JsonProperty("userSchedulingPreemptive")
  private boolean userSchedulingPreemptive;

  @JsonProperty("userlevelSchedulerArrivalTimeRR")
  private boolean userlevelSortByPid = true;

  private UserStrategy userlevelStrategy;
  private final ArrayList<SchedulingUserLevelThread> userLevelThreads = new ArrayList<>();
  private int computingTimeSum = 0;

  public int getKernelthreadID() {
    return kernelthreadID;
  }

  public int getStrategy() {
    return strategy;
  }

  public int getStartTime() {
    return startTime;
  }

  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public List<SchedulingUserLevelThread> getUserLevelThreads() {
    return userLevelThreads;
  }

  public void addToUserLevelThreads(SchedulingUserLevelThread u) {
    userLevelThreads.add(u);
  }

  public void alterComputingTimeSum(int difference) {
    computingTimeSum += difference;
  }

  public void setComputingTimeSum(int computingTimeSum) {
    this.computingTimeSum = computingTimeSum;
  }

  public int getComputingTimeSum() {
    return computingTimeSum;
  }

  public boolean isLowestPriorityFirstKernel() {
    return lowestPriorityFirstKernel;
  }

  public double getPrioChangeComputingKernel() {
    return prioChangeComputingKernel;
  }

  public double getPrioChangeWaitingKernel() {
    return prioChangeWaitingKernel;
  }

  public int getUserlevelTimeSliceLength() {
    return userlevelTimeSliceLength;
  }

  public void setStartTime(int startTime) {
    this.startTime = startTime;
  }

  public void initUserLevelStrategy(SchedulingDiagram diagram) {
    userlevelStrategy =
        switch (strategy) {
          case STRATEGIES_SJF -> new UserSJF(diagram, this);
          case STRATEGIES_EDF -> new UserEDF(diagram, this);
          case STRATEGIES_SRTN -> new UserSRTN(diagram, this);
          case STRATEGIES_RR -> new UserRR(diagram, this);
          case STRATEGIES_PRIORITY -> new UserPriorityStatic(diagram, this);
          case STRATEGIES_PRIORITY_DYNAMIC -> new UserPriorityDynamic(diagram, this);
          default -> new UserFCFS(diagram, this);
        };
  }

  public int computeTimeSlice(int fillUntil, int lastComputedCellIndex, String suffix) {
    return userlevelStrategy.evaluate(fillUntil, lastComputedCellIndex, suffix);
  }

  /** Computes the computing time sum of all userlevelthreads of this kernellvelthread */
  public void recomputeComputingTimeSum() {
    computingTimeSum = 0;
    for (SchedulingUserLevelThread u : userLevelThreads) {
      computingTimeSum += u.getComputingTime();
    }
  }

  public boolean isUserSchedulingPreemptive() {
    return userSchedulingPreemptive;
  }

  /**
   * removes all userlevelthreads with an outdated deadline from this kernellevel thread
   *
   * @param lastCellIndex act cell index -> all smaller deadlines will be removed
   */
  public void cleanOutdatedUserLevelthreads(int lastCellIndex) {
    userLevelThreads.removeIf(
        u ->
            u.checkIfOutdated(
                lastCellIndex, strategy == STRATEGIES_PRIORITY_DYNAMIC, prioChangeWaitingKernel));
    recomputeComputingTimeSum();
    if (!userLevelThreads.isEmpty()) {
      initStartTime();
    }
  }

  /**
   * determines the minimum starttime of all userlevelthreads. Will be set as the
   * kernellevelstarttime.
   */
  public void initStartTime() {
    startTime =
        userLevelThreads.stream()
            .min(Comparator.comparing(SchedulingUserLevelThread::getStartTime))
            .orElseThrow()
            .getStartTime();
  }

  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public UserStrategy getUserlevelStrategy() {
    return userlevelStrategy;
  }

  public void setKernelthreadID(int kernelthreadID) {
    this.kernelthreadID = kernelthreadID;
  }

  public boolean isUserlevelSortByPid() {
    return userlevelSortByPid;
  }

  public String generateLogString() {
    return "SchedulingKernelLevelThread{"
        + "kerStrat="
        + strategy
        + ", lowprio="
        + lowestPriorityFirstKernel
        + ", prioComp="
        + prioChangeComputingKernel
        + ", prioWait="
        + prioChangeWaitingKernel
        + ", preemptive="
        + userSchedulingPreemptive
        + ", nrthreads="
        + userLevelThreads.size()
        + '}';
  }
}
