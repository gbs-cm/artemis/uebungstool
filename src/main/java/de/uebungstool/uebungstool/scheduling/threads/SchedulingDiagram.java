package de.uebungstool.uebungstool.scheduling.threads;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.STRATEGIES_FCFS;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SchedulingDiagram {

  public boolean sortByPid = true;
  private int strategy = STRATEGIES_FCFS;

  private int timeSliceLength = -1;

  @JsonProperty("allUserLevelThreads")
  public SchedulingUserLevelThread[] userLevelThreads;

  private SchedulingKernelLevelThread[] kernelLevelThreads;
  private boolean schedulerTime = true;
  private boolean dispatcherTime = false;
  private boolean ownUserLevelScheduler = true;
  private boolean preemptive = false;

  private double prioChangeWaiting = 2.0;
  private double prioChangeComputing = -1.0;
  private boolean lowestPriorityFirst = false;
  private boolean initialScheduler = true;
  private boolean userSchedulerTime = true;
  private int coreNumber = 1;

  public List<SchedulingUserLevelThread> accessAllUserLevelThreadsWithoutScheduler() {
    ArrayList<SchedulingUserLevelThread> retValue =
        new ArrayList<>(Arrays.asList(userLevelThreads));
    retValue.remove(userLevelThreads.length - 1);
    return retValue;
  }

  public List<SchedulingKernelLevelThread> accessAllKernelLevelThreadsWithoutScheduler() {
    ArrayList<SchedulingKernelLevelThread> retValue =
        new ArrayList<>(Arrays.asList(kernelLevelThreads));

    retValue.remove(kernelLevelThreads.length - 1);
    return retValue;
  }

  public void useSystemAtTimeslot(int lastComputedCellIndex) {
    userLevelThreads[userLevelThreads.length - 1].getCells()[lastComputedCellIndex].setCorrected(
        SchedulingDiagramConstants.STATUS_COMPUTING);
  }

  @JsonIgnore
  public int getStrategy() {
    return strategy;
  }

  @JsonProperty("strategy")
  public void setStrategy(int strategy) {
    this.strategy = strategy;
  }

  @JsonIgnore
  public int getTimeSliceLength() {
    return timeSliceLength;
  }

  @JsonProperty("timeSliceLength")
  public void setTimeSliceLength(int timeSliceLength) {
    this.timeSliceLength = timeSliceLength;
  }

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public SchedulingKernelLevelThread[] getKernelLevelThreads() {
    return kernelLevelThreads;
  }

  @JsonProperty("allKernelLevelThreads")
  @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "Not accessed by untrusted Code.")
  public void setKernelLevelThreads(SchedulingKernelLevelThread[] kernelLevelThreads) {
    this.kernelLevelThreads = kernelLevelThreads;
  }

  @JsonIgnore
  public boolean isSchedulerTime() {
    return schedulerTime;
  }

  @JsonProperty("schedulerTime")
  public void setSchedulerTime(boolean schedulerTime) {
    this.schedulerTime = schedulerTime;
  }

  @JsonIgnore
  public boolean isOwnUserLevelScheduler() {
    return ownUserLevelScheduler;
  }

  @JsonProperty("ownUserLevelScheduler")
  public void setOwnUserLevelScheduler(boolean ownUserLevelScheduler) {
    this.ownUserLevelScheduler = ownUserLevelScheduler;
  }

  @JsonIgnore
  public boolean isDispatcherTime() {
    return dispatcherTime;
  }

  @JsonProperty("dispatcherTime")
  public void setDispatcherTime(boolean dispatcherTime) {
    this.dispatcherTime = dispatcherTime;
  }

  @JsonIgnore
  public boolean isPreemptive() {
    return preemptive;
  }

  @JsonProperty("preemptive")
  public void setPreemptive(boolean preemptive) {
    this.preemptive = preemptive;
  }

  @JsonIgnore
  public double getPrioChangeWaiting() {
    return prioChangeWaiting;
  }

  @JsonProperty("prioChangeWaiting")
  public void setPrioChangeWaiting(double prioChangeWaiting) {
    this.prioChangeWaiting = prioChangeWaiting;
  }

  @JsonIgnore
  public double getPrioChangeComputing() {
    return prioChangeComputing;
  }

  @JsonProperty("prioChangeComputing")
  public void setPrioChangeComputing(double prioChangeComputing) {
    this.prioChangeComputing = prioChangeComputing;
  }

  @JsonIgnore
  public boolean isLowestPriorityFirst() {
    return lowestPriorityFirst;
  }

  @JsonProperty("lowestPriorityFirst")
  public void setLowestPriorityFirst(boolean lowestPriorityFirst) {
    this.lowestPriorityFirst = lowestPriorityFirst;
  }

  @JsonIgnore
  public boolean isInitialScheduler() {
    return initialScheduler;
  }

  @JsonProperty("initialScheduler")
  public void setInitialScheduler(boolean initialScheduler) {
    this.initialScheduler = initialScheduler;
  }

  @JsonIgnore
  public boolean isUserSchedulerTime() {
    return userSchedulerTime;
  }

  @JsonProperty("userSchedulerTime")
  public void setUserSchedulerTime(boolean userSchedulerTime) {
    this.userSchedulerTime = userSchedulerTime;
  }

  @JsonIgnore
  public int getCoreNumber() {
    return coreNumber;
  }

  @JsonIgnore
  public boolean isSortByPid() {
    return sortByPid;
  }

  @JsonProperty("rrOrder")
  public void setSortByPid(boolean sortByPid) {
    this.sortByPid = sortByPid;
  }

  @JsonProperty("coreNumber")
  @Size(min = 1, max = 8)
  public void setCoreNumber(int coreNumber) {
    this.coreNumber = coreNumber;
  }

  public boolean equalCorrection(SchedulingDiagram diagram) {
    for (int i = 0; i < userLevelThreads.length; i++) {
      if (!userLevelThreads[i].equalCorrection(diagram.userLevelThreads[i])) {
        return false;
      }
    }
    return true;
  }

  public String generateLogString() {

    return "strategy="
        + strategy
        + ", timeSliceLength="
        + timeSliceLength
        + ", schedulerTime="
        + schedulerTime
        + ", dispatcherTime="
        + dispatcherTime
        + ", ownUserLevelScheduler="
        + ownUserLevelScheduler
        + ", preemptive="
        + preemptive
        + ", prioChangeWaiting="
        + prioChangeWaiting
        + ", prioChangeComputing="
        + prioChangeComputing
        + ", lowestPriorityFirst="
        + lowestPriorityFirst
        + ", initialScheduler="
        + initialScheduler
        + ", userSchedulerTime="
        + userSchedulerTime
        + "sortByPid="
        + sortByPid
        + ", coreNumber="
        + coreNumber
        + toStringUserLevelThreads()
        + toStringKernelLevelThreads();
  }

  /**
   * Generates the Logging string for all userlevelthreads
   *
   * @return string in reduced JSON format
   */
  private String toStringUserLevelThreads() {
    StringBuilder retValue = new StringBuilder(";;");
    for (SchedulingUserLevelThread u : userLevelThreads) {
      retValue.append(u.generateLogString());
    }
    return retValue.toString();
  }

  /**
   * Generates the Logging string for all kernel-level-threads
   *
   * @return string in reduced JSON format
   */
  private String toStringKernelLevelThreads() {

    if (kernelLevelThreads == null) {
      return ";;";
    }
    StringBuilder retValue = new StringBuilder(";;");
    for (SchedulingKernelLevelThread k : kernelLevelThreads) {
      retValue.append(k.generateLogString());
    }
    return retValue.toString();
  }
}
