package de.uebungstool.uebungstool.scheduling.threads;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.COLOR_CORRECT;
import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.COLOR_FALSE;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SchedulingDiagramCell {

  private String status;
  private String corrected;
  private String color;
  private String correctionIndex;

  public void setCorrectedWithoutColor(String corrected) {
    this.corrected = corrected;
  }

  public void setCorrected(String corrected) {
    this.corrected = corrected;
    setCorrectedBody(corrected);
  }

  public void setCorrected(String corrected, int coreID) {
    this.corrected = corrected + coreID;
    setCorrectedBody(corrected);
  }

  public void setCorrected(String corrected, String suffix) {
    this.corrected = corrected + suffix;
    setCorrectedBody(corrected);
  }

  private void setCorrectedBody(String corrected) {
    if (corrected.equals(status)) {
      color = COLOR_CORRECT;
    } else {
      color = COLOR_FALSE;
    }
  }

  public void setWrongBeforeStart() {
    this.corrected = "";
    color = COLOR_FALSE;
  }

  public String getCorrected() {
    return corrected;
  }

  public String getColor() {
    return color;
  }

  public String getCorrectionIndex() {
    return this.correctionIndex;
  }

  public void setCorrectionIndex(Double correctionIndex) {
    this.correctionIndex = "" + correctionIndex;
  }

  @JsonIgnore
  public String getStatus() {
    return status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  public void setColor(String colorCorrect) {
    color = colorCorrect;
  }
}
