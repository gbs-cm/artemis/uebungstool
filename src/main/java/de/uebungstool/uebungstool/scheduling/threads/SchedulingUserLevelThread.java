package de.uebungstool.uebungstool.scheduling.threads;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Arrays;
import java.util.logging.Logger;

public class SchedulingUserLevelThread extends SchedulingGenericThread {

  private static final Logger log = Logger.getLogger(SchedulingUserLevelThread.class.getName());

  private int id;
  private int computingTime;
  private int startTime;
  private SchedulingDiagramCell[] cells;
  private int deadline;
  private int ioLength;
  private int ioAftern;
  private int parentID;
  private double priority = 0.0;
  private boolean marked = false;
  private int positionInQueue = -1;

  @JsonIgnore
  public double getInvertedPriority() {
    return -1 * priority;
  }

  public boolean decrementComputingTime() {
    computingTime--;
    return computingTime <= 0;
  }

  /** Sets all uncorrected cells to the corect corretion color */
  public void correctRemainingCells() {
    Arrays.stream(cells)
        .filter(c -> c.getCorrected() == null || c.getCorrected().equals(STATUS_CLEAR))
        .forEach(
            c -> {
              if (!c.getStatus().equals(STATUS_CLEAR)) {
                c.setWrongBeforeStart();
              } else {
                c.setColor(COLOR_CORRECT);
              }
            });
  }

  public void addToPriority(double newPriority) {
    priority = (Math.round((priority + newPriority) * 10.0)) / 10.0;
  }

  public void decrementIOAfter() {
    ioAftern--;
  }

  @JsonIgnore
  public int getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(int id) {
    this.id = id;
  }

  @JsonIgnore
  public int getComputingTime() {
    return computingTime;
  }

  @JsonProperty("computingTime")
  public void setComputingTime(int computingTime) {
    this.computingTime = computingTime;
  }

  @JsonIgnore
  public int getStartTime() {
    return startTime;
  }

  @JsonProperty("startTime")
  public void setStartTime(int startTime) {
    this.startTime = startTime;
  }

  @JsonProperty("cells")
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public SchedulingDiagramCell[] getCells() {
    return cells;
  }

  @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "Not accessed by untrusted Code.")
  public void setCells(SchedulingDiagramCell[] cells) {
    this.cells = cells;
  }

  @JsonIgnore
  public int getDeadline() {
    return deadline;
  }

  @JsonProperty("deadline")
  public void setDeadline(int deadline) {
    this.deadline = deadline;
  }

  @JsonIgnore
  public int getIoLength() {
    return ioLength;
  }

  @JsonProperty("ioLength")
  public void setIoLength(int ioLength) {
    this.ioLength = ioLength;
  }

  @JsonIgnore
  public int getIoAftern() {
    return ioAftern;
  }

  @JsonProperty("ioAftern")
  public void setIoAftern(int ioAftern) {
    this.ioAftern = ioAftern;
  }

  @JsonIgnore
  public int getParentID() {
    return parentID;
  }

  @JsonProperty("parentid")
  public void setParentID(int parentID) {
    this.parentID = parentID;
  }

  @JsonIgnore
  public double getPriority() {
    return priority;
  }

  @JsonProperty("priority")
  public void setPriority(double priority) {
    this.priority = priority;
  }

  @JsonIgnore
  public void markForRound() {
    this.marked = true;
  }

  @JsonIgnore
  public void unmark() {
    this.marked = false;
  }

  @JsonIgnore
  public boolean getMark() {
    return marked;
  }

  /**
   * compares all cells with the cells of the given userlevelthread to detect differences null and
   * "" imply the same value.
   *
   * @param userLevelThread Userlevelthread tom compare to
   * @return true if both threads have the equal values in their corrected field
   */
  public boolean equalCorrection(SchedulingUserLevelThread userLevelThread) {
    for (int i = 0; i < cells.length; i++) {
      if (!(cells[i].getCorrected() == null
          && userLevelThread.getCells()[i].getCorrected() == null)) {

        if (cells[i].getCorrected() == null
            || userLevelThread.getCells()[i].getCorrected() == null) {
          if ((cells[i].getCorrected() == null
                  && !userLevelThread.getCells()[i].getCorrected().equals(""))
              || (userLevelThread.getCells()[i].getCorrected() == null
                  && !cells[i].getCorrected().equals(""))) {
            log.info(
                "Difference in Correction: Actually: "
                    + cells[i].getCorrected()
                    + "  Expected: "
                    + userLevelThread.getCells()[i].getCorrected());
            return false;
          }
        } else if (!cells[i].getCorrected().equals(userLevelThread.getCells()[i].getCorrected())) {
          log.info(
              "Difference in Correction: Actually: "
                  + cells[i].getCorrected()
                  + "  Expected: "
                  + userLevelThread.getCells()[i].getCorrected());
          return false;
        }
      }

      if (cells[i].getColor() == null || !cells[i].getColor().equals(COLOR_CORRECT)) {
        log.info(
            "Wrong color in a Cell: Actually: "
                + cells[i].getColor()
                + " Expected: "
                + COLOR_CORRECT);
        return false;
      }
    }
    return true;
  }

  /**
   * Checks whether the deadline of this userlevelthread has run up and whether it must subsequently
   * been deleted from the list in the kernelthread. All remaining cells are corrected accordingly.
   *
   * @param lastCellIndex cell index of the act computing time
   * @return true if the deadline did not already run up
   */
  public boolean checkIfOutdated(int lastCellIndex, boolean dynamic, double priochangeWaiting) {
    if (deadline > -1 && deadline <= lastCellIndex) {
      int initialDeadline = deadline;

      deadline--;
      while (deadline >= startTime && cells[deadline].getCorrected() == null) {
        cells[deadline].setCorrected(STATUS_WAITING);
        deadline--;
      }

      if (dynamic) {
        for (int i = deadline + 1; i < initialDeadline; i++) {
          if (cells[i].getCorrectionIndex() == null) {
            addToPriority(priochangeWaiting);
            getCells()[i].setCorrectionIndex(priority);
          }
        }
      }
      return true;
    }
    return computingTime <= 0;
  }

  public String generateLogString() {
    return "(cTime="
        + computingTime
        + ", sTime="
        + startTime
        + ", deadline="
        + deadline
        + ", ioLen="
        + ioLength
        + ", ioAftern="
        + ioAftern
        + ", prio="
        + priority
        + ')';
  }

  public int getPositionInQueue() {
    return positionInQueue;
  }

  public void setPositionInQueue(int positionInQueue) {
    this.positionInQueue = positionInQueue;
  }
}
