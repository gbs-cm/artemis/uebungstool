package de.uebungstool.uebungstool.scheduling.threads;

public class SchedulingDiagramConstants {

  public static final int STRATEGIES_FCFS = 0;
  public static final int STRATEGIES_SJF = 1;
  public static final int STRATEGIES_SRTN = 2;
  public static final int STRATEGIES_RR = 3;
  public static final int STRATEGIES_EDF = 4;
  public static final int STRATEGIES_PRIORITY = 5;
  public static final int STRATEGIES_PRIORITY_DYNAMIC = 6;

  public static final String STATUS_COMPUTING = "X";
  public static final String STATUS_WAITING = "-";
  public static final String STATUS_CLEAR = "";

  public static final String COLOR_GREY = "grey";
  public static final String COLOR_TRANSPARENT = "transparent";
  public static final String COLOR_FALSE = "var(--danger-color)";
  public static final String COLOR_CORRECT = "var(--green-correction)";

  public static final int MAX_AMOUNT_THREADS = 50;
  public static final int MAX_AMOUNT_CORES = 8;
  public static final int MAX_COMPUTING_TIME = 50;
  public static final int MAX_START_TIME = 50;
}
