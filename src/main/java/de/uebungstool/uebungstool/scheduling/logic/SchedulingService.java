package de.uebungstool.uebungstool.scheduling.logic;

import static de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagramConstants.*;

import de.uebungstool.uebungstool.scheduling.strategies.*;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingDiagram;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingKernelLevelThread;
import de.uebungstool.uebungstool.scheduling.threads.SchedulingUserLevelThread;
import java.util.Arrays;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

@Service
public class SchedulingService {

  private static final Logger log = Logger.getLogger(SchedulingService.class.getName());
  private static final boolean DEBUG_ACTIVATED = false;

  public boolean evaluate(SchedulingDiagram diagram) {
    SchedulingStrategy strategy;
    if (!checkIfDiagramIsValid(diagram)) {
      log.info("Invalid Diagram: No solution will be computed.");
      return false;
    }

    strategy =
        switch (diagram.getStrategy()) {
          case STRATEGIES_SJF -> new SchedulingStrategySJF(diagram);
          case STRATEGIES_EDF -> new SchedulingStrategyEDF(diagram);
          case STRATEGIES_SRTN -> new SchedulingStrategySRTN(diagram);
          case STRATEGIES_PRIORITY -> new SchedulingStrategyPriorityStatic(diagram);
          case STRATEGIES_PRIORITY_DYNAMIC -> new SchedulingStrategyPriorityDynamic(diagram);
          case STRATEGIES_RR -> new SchedulingStrategyRR(diagram);
          default -> new SchedulingStrategyFCFS(diagram);
        };

    if (DEBUG_ACTIVATED) {
      return evaluateShowResults(strategy, diagram);
    } else {
      return evaluteShowToFirstError(strategy, diagram);
    }
  }

  /**
   * Marks the line with the first difference between the computed result and the students solution.
   * The corresponding column will be set to the correction color red, all columns before the faulty
   * one are set to green and the remaining columns will stay transparent.
   *
   * @param strategy Strategy which is used to evaluate the configuration
   * @param diagram with the remaining configuration
   * @return whether it could be successfully computed (Alsways true in this methode; Alternative
   *     methode may return false)
   */
  private boolean evaluteShowToFirstError(SchedulingStrategy strategy, SchedulingDiagram diagram) {
    try {
      strategy.evaluate();
      for (SchedulingUserLevelThread userLevelThread : diagram.userLevelThreads) {
        userLevelThread.correctRemainingCells();
      }
    } catch (IndexOutOfBoundsException ignored) {
    } // Exception does not need to be handled -> diagram from the students solution is shorter than
    // the computed solution -> student must have an error before the end since the diagram
    // automatically expands

    for (int i = 0; i < diagram.userLevelThreads[0].getCells().length; i++) {
      for (SchedulingUserLevelThread u : diagram.userLevelThreads) {
        if (u.getCells()[i].getColor().equals(COLOR_FALSE)) {
          setRemainingCells(i, diagram);

          return true;
        }
      }
    }
    return true;
  }

  /**
   * Sets the first column at the index to the color false and all other afterwards to transparent
   *
   * @param i
   * @param diagram
   */
  private void setRemainingCells(int i, SchedulingDiagram diagram) {
    for (SchedulingUserLevelThread k : diagram.userLevelThreads) {
      k.getCells()[i].setColor(COLOR_FALSE);
      k.getCells()[i].setCorrectedWithoutColor("");
      k.getCells()[i].setCorrectionIndex(null);
    }
    for (int j = i + 1; j < diagram.userLevelThreads[0].getCells().length; j++) {
      for (SchedulingUserLevelThread k : diagram.userLevelThreads) {
        k.getCells()[j].setColor(COLOR_TRANSPARENT);
        k.getCells()[j].setCorrectedWithoutColor("");

        k.getCells()[j].setCorrectionIndex(null);
      }
    }
  }

  /**
   * Alternative Methode to compute the solution without setting the first wrong column to false
   *
   * @param strategy Strategy which is used to evaluate the configuration
   * @param diagram with the remaining configuration
   * @return whether it could be successfully computed
   */
  private boolean evaluateShowResults(SchedulingStrategy strategy, SchedulingDiagram diagram) {
    try {
      strategy.evaluate();
      for (SchedulingUserLevelThread userLevelThread : diagram.userLevelThreads) {
        userLevelThread.correctRemainingCells();
      }
    } catch (IndexOutOfBoundsException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  /** All parameters in the diagram musst make sense -> prevent harmful inputs. */
  private boolean checkIfDiagramIsValid(SchedulingDiagram diagram) {

    return checkDiagram(diagram) && checkUserlevelthreads(diagram) && checkRRSpecial(diagram);
  }

  private boolean checkRRSpecial(SchedulingDiagram diagram) {
    return diagram.getStrategy() != STRATEGIES_RR || checkKernelLevelThreads(diagram);
  }

  private boolean checkUserlevelthreads(SchedulingDiagram diagram) {
    for (SchedulingUserLevelThread u : diagram.accessAllUserLevelThreadsWithoutScheduler()) {
      if (u.getComputingTime() < 1 || u.getComputingTime() > MAX_COMPUTING_TIME) {
        return false;
      }
      if (u.getStartTime() < 0 || u.getStartTime() > MAX_START_TIME) {
        return false;
      }
      if (u.getDeadline() != -1 && u.getDeadline() <= u.getStartTime()) {
        return false;
      }
      if (u.getCells() == null
          || u.getCells().length == 0
          || u.getCells().length
              > MAX_START_TIME
                  + MAX_AMOUNT_THREADS
                      * MAX_COMPUTING_TIME
                      * 3) { // maxNumberOfTHreads * max computingTimeperThread * schedulingFaktor
        return false;
      }

      if (!(u.getIoAftern() == -1 && u.getIoLength() == -1)) {
        if (u.getIoAftern() == -1 || u.getIoLength() == -1) {
          return false;
        }
        if (u.getIoAftern() > u.getComputingTime() || u.getIoLength() < 0) {
          return false;
        }
      }

      if (Arrays.stream(u.getCells())
          .anyMatch(
              c ->
                  c == null || c.getStatus() == null)) { // cells contain null as status or are null
        return false;
      }
    }
    return true;
  }

  private boolean checkDiagram(SchedulingDiagram diagram) {
    // All other strategies can be preemptive or nonpreemptive
    if (diagram.getStrategy() == STRATEGIES_FCFS || diagram.getStrategy() == STRATEGIES_SJF) {
      diagram.setPreemptive(false);
    } else if (diagram.getStrategy() == STRATEGIES_SRTN) {
      diagram.setPreemptive(true);
    }

    if (diagram.userLevelThreads == null
        || diagram.userLevelThreads.length < 2
        || diagram.userLevelThreads.length
            > MAX_AMOUNT_THREADS) { // must at least contain a Scheduler and one Thread
      return false;
    }

    if (Arrays.asList(diagram.userLevelThreads).contains(null)) {
      return false;
    }
    if (Arrays.stream(diagram.userLevelThreads).filter(u -> u.getId() == -1).toArray().length
        != 1) { // Exactly one scheduler exists

      log.info("Error: Diagram does not has exactly one scheduler!");
      return false;
    }

    if (diagram.getStrategy() == STRATEGIES_RR && diagram.getTimeSliceLength() < 0) {
      return false;
    }

    if (diagram.getCoreNumber() < 1) {
      diagram.setCoreNumber(1);
    } else if (diagram.getCoreNumber() > MAX_AMOUNT_CORES) {
      diagram.setCoreNumber(MAX_AMOUNT_CORES);
    }
    return true;
  }

  /**
   * @return false if the kernellevel Threads are not in the correct format
   */
  private boolean checkKernelLevelThreads(SchedulingDiagram diagram) {
    if ((diagram.getKernelLevelThreads() != null)) {
      if (diagram.getKernelLevelThreads().length < 2
          || diagram.getKernelLevelThreads().length
              > MAX_AMOUNT_THREADS) { // must at least contain a Scheduler and one Thread
        return false;
      }

      if (Arrays.asList(diagram.getKernelLevelThreads()).contains(null)) {
        return false;
      }
      if (Arrays.stream(diagram.getKernelLevelThreads())
              .filter(u -> u.getKernelthreadID() == -1)
              .toArray()
              .length
          != 1) { // Exactly one scheduler exists

        log.info("Error: Diagram does not has exactly one scheduler!");
        return false;
      }
      for (SchedulingKernelLevelThread k : diagram.getKernelLevelThreads()) {

        if (k.getKernelthreadID() == -1) {
          continue;
        }
        if (k.getStartTime() < 0 || k.getStartTime() > MAX_START_TIME) {
          return false;
        }
      }
      return true;
    }
    return !diagram.isOwnUserLevelScheduler();
  }
}
