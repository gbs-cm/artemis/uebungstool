package de.uebungstool.uebungstool.paging.services;

import de.uebungstool.uebungstool.paging.PagingController;
import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import de.uebungstool.uebungstool.paging.strategies.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

@Service
public class PagingService {
  private static final String CLOCK = "clock";
  public static final List<String> flagAlgorithms = List.of(CLOCK, "lru", "nfu");
  public static final List<String> doubleFlagAlgorithms = List.of(CLOCK);

  private PagingService() {}

  /**
   * evaluate students submission
   *
   * @param pagingDiagram students submission
   * @param wrongPageFaults empty Set for page faults mistakes
   * @param wrongWriteBacks empty Set for write back mistakes
   * @return List of TableCell objects for each mistake
   */
  public static List<TableCell> evaluate(
      PagingDiagram pagingDiagram, Set<Integer> wrongPageFaults, Set<Integer> wrongWriteBacks) {
    Strategy strategy =
        switch (pagingDiagram.getStrategy()) {
          case CLOCK -> new Clock();
          case "fifo" -> new FirstInFirstOut();
          case "lru" -> new LeastRecentlyUsed();
          case "nfu" -> new NotFrequentlyUsed();
          default -> null;
        };

    if (strategy == null) {
      Logger.getLogger(PagingController.class.getName())
          .warning("Could not find Paging-Strategy " + pagingDiagram.getStrategy() + ".");
      return null;
    } else {
      List<List<TableCell>> solution = new ArrayList<>();
      strategy.calculateSolution(pagingDiagram, solution, wrongPageFaults, wrongWriteBacks);
      return Strategy.checkTable(pagingDiagram.getTable(), solution, pagingDiagram.getStrategy());
    }
  }
}
