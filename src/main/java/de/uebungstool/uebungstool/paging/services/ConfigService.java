package de.uebungstool.uebungstool.paging.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uebungstool.uebungstool.paging.data.Config;
import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class ConfigService {
  private static final Logger log = Logger.getLogger(ConfigService.class.getName());

  private ConfigService() {}

  /**
   * loads configuration examples from file system
   *
   * @return List<Config> of configuration examples
   */
  public static List<Config> getSampleConfigs() {
    ArrayList<Config> configList = new ArrayList<>();
    File folder = new File("PagingSampleFiles");
    log.finest("Loading CSV-Files from: " + folder.getAbsolutePath());
    if ((folder.canRead() && !folder.isHidden())) {
      getConfigsFromFile(configList, folder);
    } else {
      log.warning("Folder read protected");
    }
    return configList;
  }

  /**
   * recursively loads all configurations from file system including subfolder
   *
   * @param configList a List to add loaded configurations
   * @param folder the File object of the current folder
   */
  private static void getConfigsFromFile(List<Config> configList, final File folder) {
    File[] files = folder.listFiles();

    if (files == null) return;

    for (File fileEntry : files) {
      if (fileEntry.canRead() && !fileEntry.isHidden()) {
        if (fileEntry.isDirectory()) getConfigsFromFile(configList, fileEntry);
        else if (fileEntry.isFile() && fileEntry.getName().length() > 7) {
          String name = fileEntry.getName();

          if (name.substring(name.length() - 7).equalsIgnoreCase(".sample")) {
            // Config file
            String configName = name.substring(0, name.length() - 7);
            configList.add(new Config(configName, getFileContentsConfig(fileEntry)));
          }
        }
      }
    }
  }

  /**
   * load file contents as PagingDiagram object
   *
   * @param file the File object to scan for configuration
   * @return PagingDiagram object
   */
  private static PagingDiagram getFileContentsConfig(File file) {
    try (Scanner scanner = new Scanner(file, StandardCharsets.UTF_8)) {
      String content = "";
      if (scanner.hasNextLine()) {
        content = (scanner.nextLine());
      }
      if (content.length() > 0) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(content, PagingDiagram.class);
      }
    } catch (JsonProcessingException e) {
      log.info("Failed parsing file " + file.getAbsolutePath() + ": " + e.getMessage());
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      log.warning("File not Found:");
      e.printStackTrace();
    } catch (IOException e) {
      log.warning("Config file defect: " + file.getAbsolutePath());
      e.printStackTrace();
    }
    log.warning("Using null Diagram");
    return null;
  }
}
