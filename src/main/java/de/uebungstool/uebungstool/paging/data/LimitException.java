package de.uebungstool.uebungstool.paging.data;

public class LimitException extends Exception {
  public LimitException(String message) {
    super(message);
  }
}
