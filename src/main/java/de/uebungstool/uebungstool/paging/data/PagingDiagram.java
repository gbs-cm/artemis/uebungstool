package de.uebungstool.uebungstool.paging.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PagingDiagram {

  @JsonProperty("callList")
  private List<Integer> callList;

  @JsonProperty("strategy")
  private String strategy;

  @JsonProperty("frameCount")
  private int frameCount;

  @JsonProperty("diagram")
  private List<List<TableCell>> diagram;

  @JsonProperty("pageFaults")
  private Set<Integer> pageFaults;

  @JsonProperty("writeBacks")
  private Set<Integer> writeBacks;

  @JsonProperty("rwList")
  private List<Boolean> rwList;

  private static final int MIN_PAGE_COUNT = 1; // Min-limit for Number of Pages
  private static final int MAX_PAGE_COUNT = 32; // Max-limit for Number of Pages
  private static final int MIN_FRAME_COUNT = 1; // Min-limit for Number of Frames
  private static final int MIN_ROUND_COUNT = 1; // Min-limit for Paging
  private static final int MAX_ROUND_COUNT = 128; // Max-limit for Paging

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public List<Integer> getCallList() {
    return callList;
  }

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public List<Boolean> getRWList() {
    return rwList;
  }

  @JsonIgnore
  public String getStrategy() {
    return strategy;
  }

  @JsonIgnore
  public int getFrameCount() {
    return frameCount;
  }

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public List<List<TableCell>> getTable() {
    return diagram;
  }

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public Set<Integer> getPageFaults() {
    return pageFaults;
  }

  @JsonIgnore
  @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Not accessed by untrusted Code.")
  public Set<Integer> getWriteBacks() {
    return writeBacks;
  }

  /**
   * checks if all parameters fit together
   *
   * @throws LimitException
   */
  public void selfCheck() throws LimitException {
    checkPages();
    checkRounds();
    checkFrames();
  }

  /**
   * checks if frames are within given limits
   *
   * @throws LimitException
   */
  private void checkFrames() throws LimitException {
    if (frameCount < MIN_FRAME_COUNT) {
      throw new LimitException("Too few frames!");
    } else if (frameCount > new HashSet<>(callList).size()) {
      throw new LimitException("Too many frames!");
    }
  }

  /**
   * checks if rounds are within given limits
   *
   * @throws LimitException
   */
  private void checkRounds() throws LimitException {
    if (callList.size() < MIN_ROUND_COUNT) {
      throw new LimitException("Too few calls!");
    } else if (callList.size() > MAX_ROUND_COUNT) {
      throw new LimitException("Too many calls!");
    }
  }

  /**
   * checks if number of pages is within given limits
   *
   * @throws LimitException
   */
  private void checkPages() throws LimitException {
    int distinctObjects = new HashSet<>(callList).size();
    if (distinctObjects > MAX_PAGE_COUNT) {
      throw new LimitException("Too many distinct pages!");
    } else if (distinctObjects < MIN_PAGE_COUNT) {
      throw new LimitException("Too few distinct pages!");
    }
  }

  public String generateLogString() {
    return "PagingDiagram{" + ", strategy='" + strategy + '\'' + ", frameCount=" + frameCount + '}';
  }
}
