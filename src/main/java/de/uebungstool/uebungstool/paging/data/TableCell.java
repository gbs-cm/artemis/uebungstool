package de.uebungstool.uebungstool.paging.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.uebungstool.uebungstool.paging.PagingController;
import java.util.logging.Logger;

public class TableCell {
  @JsonProperty("page")
  private int page = -1;

  @JsonProperty("flag")
  private int flag = -1;

  @JsonProperty("i")
  private int i = -1;

  @JsonProperty("j")
  private int j = -1;

  public static final int NOT_USED = -2; // Value for empty frames
  public static final int PLACEHOLDER = -1; // Value for unchanged Frames
  public static final int EQUAL = 0;
  public static final int DIFFERENT = 1;
  public static final int WRONG_INDEX = -3;

  public TableCell(int page, int flag, int i, int j) {
    this.page = page;
    this.flag = flag;
    this.i = i;
    this.j = j;
  }

  public TableCell() {}

  public int getPage() {
    return page;
  }

  public int getFlag() {
    return flag;
  }

  /**
   * compares two values to find out if they are equal, if a placeholder is involved or if they are
   * different
   *
   * @param first
   * @param second
   * @return Indicator for comparison of values
   */
  private int checkValues(int first, int second) {
    if (first == second) {
      return EQUAL;
    }
    if (first < 0 && second < 0) {
      return EQUAL;
    }
    if (first < 0 || second < 0) {
      return PLACEHOLDER;
    }
    return DIFFERENT;
  }

  /**
   * compares two TableCell objects
   *
   * @param t a TableCell to compare with the object
   * @return Indicator for comparison
   */
  public int compare(TableCell t) {
    if (!(this.i == t.i && this.j == t.j)) { // Prüfe ob der Index gelich ist)
      Logger.getLogger(PagingController.class.getName()).finest("Paging: Index-Fehler");
      return WRONG_INDEX;
    }
    int p = checkValues(this.page, t.page);
    int f = checkValues(this.flag, t.flag);
    if (p == f) return p;
    else if (p > 0 || f > 0) return Math.max(p, f);
    else return Math.min(p, f);
  }

  /**
   * compares two TableCell objects
   *
   * @param t a TableCell to compare with the object
   * @return a new TableCell object
   */
  public TableCell differsInPoint(TableCell t) {
    return new TableCell(
        (this.page == t.page) ? -1 : 1, (this.flag == t.flag) ? -1 : 1, this.i, this.j);
  }
}
