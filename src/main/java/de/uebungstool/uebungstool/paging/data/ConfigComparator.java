package de.uebungstool.uebungstool.paging.data;

import java.io.Serializable;
import java.util.Comparator;

public class ConfigComparator implements Comparator<Config>, Serializable {
  /**
   * Compares two Config elements by name parameter
   *
   * @param c1
   * @param c2
   * @return integer of name comparison
   */
  @Override
  public int compare(Config c1, Config c2) {
    return c1.getName().compareTo(c2.getName());
  }
}
