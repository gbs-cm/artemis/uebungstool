package de.uebungstool.uebungstool.paging.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Config {
  @JsonProperty("name")
  private String name;

  @JsonProperty("diagram")
  private PagingDiagram pagingDiagram;

  public Config(String name, PagingDiagram pagingDiagram) {
    this.name = name;
    this.pagingDiagram = pagingDiagram;
  }

  public String getName() {
    return name;
  }

  public PagingDiagram getDiagram() {
    return pagingDiagram;
  }
}
