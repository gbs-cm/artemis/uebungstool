package de.uebungstool.uebungstool.paging.strategies;

import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FirstInFirstOut implements Strategy {
  @Override
  public void calculateSolution(
      PagingDiagram pagingDiagram,
      List<List<TableCell>> solution,
      Set<Integer> wrongPageFaults,
      Set<Integer> wrongWriteBacks) {
    calculateRounds(
        solution,
        pagingDiagram,
        new ArrayList<>(pagingDiagram.getCallList()),
        -1,
        new ArrayList<>(),
        wrongPageFaults);
  }

  /**
   * recursively calculates solution
   *
   * @param lastFrame index of last used frame
   * @param solution List that takes the solution
   * @param pagingDiagram students submission
   * @param callList List of remaining calls
   * @param frames List of frames
   * @param wrongPageFaults List of page fault mistakes
   */
  private void calculateRounds(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      int lastFrame,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults) {
    if (!callList.isEmpty()) {
      int frameIndex = (lastFrame + 1) % pagingDiagram.getFrameCount();
      ArrayList<TableCell> newRow = new ArrayList<>();
      int row = solution.size();
      int next = callList.get(0);
      callList.remove(0);

      if (frames.size() > pagingDiagram.getFrameCount()) {
        frames.remove(0);
      }
      if (frames.contains(next)) {
        frameIndex--;
      }

      for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
        if (j == frameIndex && !frames.contains(next)) {
          newRow.add(new TableCell(next, TableCell.NOT_USED, row, j));
          frames.add(next);
          if (!pagingDiagram.getPageFaults().contains(row)) wrongPageFaults.add(row);
        } else {
          if (j == frameIndex && pagingDiagram.getPageFaults().contains(row))
            wrongPageFaults.add(row);
          newRow.add(new TableCell(TableCell.PLACEHOLDER, TableCell.NOT_USED, row, j));
        }
      }

      solution.add(newRow);
      calculateRounds(solution, pagingDiagram, callList, frameIndex, frames, wrongPageFaults);
    }
  }
}
