package de.uebungstool.uebungstool.paging.strategies;

import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class LeastRecentlyUsed implements Strategy {
  @Override
  public void calculateSolution(
      PagingDiagram pagingDiagram,
      List<List<TableCell>> solution,
      Set<Integer> wrongPageFaults,
      Set<Integer> wrongWriteBacks) {
    ArrayList<Integer> callList = new ArrayList<>(pagingDiagram.getCallList());
    ArrayList<Integer> frames = new ArrayList<>();
    firstRound(solution, pagingDiagram, callList, frames, wrongPageFaults);
    calculateRounds(solution, pagingDiagram, callList, frames, wrongPageFaults);
  }

  /**
   * initializes the recursive solution calculation with the first round
   *
   * @param solution empty List that takes the solution
   * @param pagingDiagram students submission
   * @param callList list of calls
   * @param frames empty List of frames
   * @param wrongPageFaults empty List of page fault mistakes
   */
  private void firstRound(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults) {
    ArrayList<TableCell> newRow = new ArrayList<>();
    int next = callList.get(0);
    callList.remove(0);
    frames.add(next);

    newRow.add(new TableCell(next, 1, 0, 0));
    while (newRow.size() < pagingDiagram.getFrameCount()) {
      newRow.add(new TableCell(-1, -1, 0, newRow.size()));
    }
    if (!pagingDiagram.getPageFaults().contains(0)) wrongPageFaults.add(0);

    solution.add(newRow);
  }

  /**
   * recursively calculates remaining rounds for solution
   *
   * @param solution initialized List that takes the solution
   * @param pagingDiagram students submission
   * @param callList List of remaining calls
   * @param frames List of frames
   * @param wrongPageFaults List of page fault mistakes
   */
  private void calculateRounds(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults) {
    if (!callList.isEmpty()) {
      ArrayList<TableCell> newRow = new ArrayList<>();
      int row = solution.size();
      int next = callList.get(0);
      callList.remove(0);

      if (frames.size() > pagingDiagram.getFrameCount()) {
        frames.remove(0);
      }
      if (frames.contains(next)) {
        // Add next round as Update of last row
        frames.remove((Integer) next);
        frames.add(next);

        for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
          if (solution.get(row - 1).get(j).getPage() == next)
            newRow.add(new TableCell(next, row + 1, row, j));
          else {
            TableCell prev = solution.get(row - 1).get(j);
            newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, j));
          }
        }
      } else {
        // Add next round while replacing LRU
        frames.add(next);

        if (!pagingDiagram.getPageFaults().contains(row)) wrongPageFaults.add(row);

        int index = Strategy.getMinFlag(solution.get(row - 1));
        for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
          if (j == index) newRow.add(new TableCell(next, row + 1, row, j));
          else {
            TableCell prev = solution.get(row - 1).get(j);
            newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, j));
          }
        }
      }

      solution.add(newRow);
      calculateRounds(solution, pagingDiagram, callList, frames, wrongPageFaults);
    }
  }
}
