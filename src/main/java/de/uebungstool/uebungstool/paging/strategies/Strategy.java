package de.uebungstool.uebungstool.paging.strategies;

import de.uebungstool.uebungstool.paging.PagingController;
import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import de.uebungstool.uebungstool.paging.services.PagingService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public interface Strategy {
  /**
   * Calculates the solution for the given Config. Implemented by strategies
   *
   * @param pagingDiagram object containing the config
   * @param solution empty List to take the solution
   * @param wrongPageFaults empty List to take indices of wrong defined page faults
   * @param wrongWriteBacks empty List to take indices of wrong defined write backs
   */
  void calculateSolution(
      PagingDiagram pagingDiagram,
      List<List<TableCell>> solution,
      Set<Integer> wrongPageFaults,
      Set<Integer> wrongWriteBacks);

  /**
   * compares students solution with systems solution
   *
   * @param input students solution
   * @param solution systems solution
   * @param algorithm chosen strategy
   * @return List of mistakes
   */
  static List<TableCell> checkTable(
      List<List<TableCell>> input, List<List<TableCell>> solution, String algorithm) {
    if (solution.size() != input.size() || solution.get(0).size() != input.get(0).size()) {
      Logger.getLogger(PagingController.class.getName())
          .warning("Paging-Error: Input and Solution have different sizes.");
    }

    ArrayList<TableCell> mistakesList = new ArrayList<>();

    for (int i = 0; i < solution.size() && i < input.size(); i++) {
      for (int j = 0; j < solution.get(i).size() && j < input.get(i).size(); j++) {
        TableCell sol = solution.get(i).get(j);
        TableCell inp = input.get(i).get(j);
        int compare = solution.get(i).get(j).compare(input.get(i).get(j));

        switch (compare) {
          case TableCell.DIFFERENT:
            mistakesList.add(sol.differsInPoint(inp));
            break;
          case TableCell.PLACEHOLDER:
            if (!checkPlaceholderPage(solution, inp, sol, i - 1, j)
                || (PagingService.flagAlgorithms.contains(algorithm)
                    && !checkPlaceholderFlag(solution, inp, sol, i - 1, j, algorithm))) {
              mistakesList.add(sol.differsInPoint(inp));
            }
            break;
          case TableCell.WRONG_INDEX:
            Logger.getLogger(PagingController.class.getName())
                .warning("Paging-Error: Different Index at " + i + " " + j);
            break;
          default:
            break;
        }
      }
    }
    return mistakesList;
  }

  /**
   * checks whether flag is similar
   *
   * @param table PagingDiagramm table
   * @param inputCell current cell of the students solution
   * @param solCell current cell of the systems solution
   * @param i index
   * @param j index
   * @param algorithm chosen strategy
   * @return
   */
  private static boolean checkPlaceholderFlag(
      List<List<TableCell>> table,
      TableCell inputCell,
      TableCell solCell,
      int i,
      int j,
      String algorithm) {
    if (i < 0) {
      return false;
    }
    TableCell tempCell = table.get(i).get(j);
    return (inputCell.getFlag() == solCell.getFlag())
        || (inputCell.getFlag() < 0 && tempCell.getFlag() == solCell.getFlag())
        || (inputCell.getFlag() <= 0
            && solCell.getFlag() <= 0
            && PagingService.doubleFlagAlgorithms.contains(algorithm));
  }

  /**
   * checks whether page is similar
   *
   * @param table PagingDiagramm table
   * @param inputCell current cell of the students solution
   * @param solCell current cell of the systems solution
   * @param i index
   * @param j index
   * @return
   */
  private static boolean checkPlaceholderPage(
      List<List<TableCell>> table, TableCell inputCell, TableCell solCell, int i, int j) {
    if (i < 0) {
      return false;
    }
    TableCell tempCell = table.get(i).get(j);
    return (inputCell.getPage() == solCell.getPage())
        || (inputCell.getPage() < 0 && tempCell.getPage() == solCell.getPage());
  }

  /**
   * determines the index of the smallest flag
   *
   * @param solutionRow the row of TableCell objects to check
   * @return index of minimal flag
   */
  static int getMinFlag(List<TableCell> solutionRow) {
    int minFlag = Integer.MAX_VALUE;
    int index = -1;

    for (int j = 0; j < solutionRow.size(); j++) {
      TableCell t = solutionRow.get(j);
      if (t.getFlag() < minFlag) {
        minFlag = t.getFlag();
        index = j;
      }
      if (minFlag < 0) break;
    }

    return index;
  }
}
