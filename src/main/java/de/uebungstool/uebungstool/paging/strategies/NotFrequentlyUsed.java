package de.uebungstool.uebungstool.paging.strategies;

import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class NotFrequentlyUsed implements Strategy {
  @Override
  public void calculateSolution(
      PagingDiagram pagingDiagram,
      List<List<TableCell>> solution,
      Set<Integer> wrongPageFaults,
      Set<Integer> wrongWriteBacks) {
    ArrayList<Integer> callList = new ArrayList<>(pagingDiagram.getCallList());
    ArrayList<Integer> frames = new ArrayList<>();
    calculateRounds(solution, pagingDiagram, callList, frames, wrongPageFaults);
  }

  /**
   * recursively calculates remaining rounds for solution
   *
   * @param solution initialized List that takes the solution
   * @param pagingDiagram students submission
   * @param callList List of remaining calls
   * @param frames List of frames
   * @param wrongPageFaults List of page fault mistakes
   */
  private void calculateRounds(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults) {
    if (!callList.isEmpty()) {
      ArrayList<TableCell> newRow = new ArrayList<>();
      int row = solution.size();
      int next = callList.get(0);
      callList.remove(0);

      if (frames.contains(next)) {
        // Add next round as Update of last row

        for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
          TableCell prev = solution.get(row - 1).get(j);

          if (prev.getPage() == next) newRow.add(new TableCell(next, prev.getFlag() + 1, row, j));
          else {
            newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, j));
          }
        }
      } else {
        // Add next round while replacing NFU or using empty frame
        int indexToAdd = row > 0 ? Strategy.getMinFlag(solution.get(row - 1)) : 0;

        if (frames.size() < pagingDiagram.getFrameCount()) {
          frames.add(next);
        } else {
          frames.remove(indexToAdd);
          frames.add(indexToAdd, next);
        }

        if (!pagingDiagram.getPageFaults().contains(row)) wrongPageFaults.add(row);

        for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
          if (j == indexToAdd) newRow.add(new TableCell(next, 1, row, j));
          else if (row > 0) {
            TableCell prev = solution.get(row - 1).get(j);
            newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, j));
          } else newRow.add(new TableCell(-1, -1, 0, j));
        }
      }

      solution.add(newRow);
      calculateRounds(solution, pagingDiagram, callList, frames, wrongPageFaults);
    }
  }
}
