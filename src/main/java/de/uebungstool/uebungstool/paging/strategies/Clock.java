package de.uebungstool.uebungstool.paging.strategies;

import de.uebungstool.uebungstool.paging.data.PagingDiagram;
import de.uebungstool.uebungstool.paging.data.TableCell;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Clock implements Strategy {
  private static final int R_BIT = 1;
  private static final int M_BIT = 10;
  private static final int RM_BITS = R_BIT + M_BIT;

  @Override
  public void calculateSolution(
      PagingDiagram pagingDiagram,
      List<List<TableCell>> solution,
      Set<Integer> wrongPageFaults,
      Set<Integer> wrongWriteBacks) {
    ArrayList<Integer> callList = new ArrayList<>(pagingDiagram.getCallList());
    ArrayList<Integer> frames = new ArrayList<>();
    ArrayList<Boolean> rwList = new ArrayList<>(pagingDiagram.getRWList());
    firstRound(solution, pagingDiagram, callList, frames, wrongPageFaults, rwList, wrongWriteBacks);
    calculateRounds(
        solution, pagingDiagram, callList, frames, wrongPageFaults, rwList, wrongWriteBacks, 1);
  }

  /**
   * initializes the recursive solution calculation with the first round
   *
   * @param solution empty List that takes the solution
   * @param pagingDiagram students submission
   * @param callList list of calls
   * @param frames empty List of frames
   * @param wrongPageFaults empty List of page fault mistakes
   * @param rwlist list of r/w access types
   * @param wrongWriteBacks empty List of write back mistakes
   */
  private void firstRound(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults,
      ArrayList<Boolean> rwlist,
      Set<Integer> wrongWriteBacks) {
    ArrayList<TableCell> newRow = new ArrayList<>();
    int next = callList.get(0);
    boolean isWrite = rwlist.get(0);
    callList.remove(0);
    rwlist.remove(0);
    frames.add(next);

    newRow.add(new TableCell(next, R_BIT + (isWrite ? M_BIT : 0), 0, 0));

    while (newRow.size() < pagingDiagram.getFrameCount()) {
      newRow.add(new TableCell(-1, -1, 0, newRow.size()));
    }
    if (!pagingDiagram.getPageFaults().contains(0)) wrongPageFaults.add(0);
    if (pagingDiagram.getWriteBacks().contains(0)) wrongWriteBacks.add(0);

    solution.add(newRow);
  }

  /**
   * recursively calculates remaining rounds for solution
   *
   * @param solution initialized List that takes the solution
   * @param pagingDiagram students submission
   * @param callList List of remaining calls
   * @param frames List of frames
   * @param wrongPageFaults List of page fault mistakes
   * @param rwlist list of remaining r/w access types
   * @param wrongWriteBacks List of write back mistakes
   * @param pointer index
   */
  private void calculateRounds(
      List<List<TableCell>> solution,
      PagingDiagram pagingDiagram,
      ArrayList<Integer> callList,
      ArrayList<Integer> frames,
      Set<Integer> wrongPageFaults,
      ArrayList<Boolean> rwlist,
      Set<Integer> wrongWriteBacks,
      int pointer) {
    ArrayList<TableCell> newRow = new ArrayList<>();
    int row = solution.size();
    int next = callList.get(0);
    boolean isWrite = rwlist.get(0);
    boolean hasWriteBack = false;
    callList.remove(0);
    rwlist.remove(0);

    if (frames.contains(next)) {
      if (pagingDiagram.getPageFaults().contains(row)) wrongPageFaults.add(row);
      for (TableCell prev : solution.get(row - 1)) {
        if (prev.getPage() == next) {
          int newFlag = isWrite ? RM_BITS : (prev.getFlag() >= M_BIT ? RM_BITS : R_BIT);
          newRow.add(new TableCell(next, newFlag, row, newRow.size()));
        } else newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, newRow.size()));
      }
    } else {
      if (!pagingDiagram.getPageFaults().contains(row)) wrongPageFaults.add(row);
      for (int j = 0; j < pagingDiagram.getFrameCount(); j++) {
        TableCell prev = solution.get(row - 1).get(j);
        if (j < pointer)
          newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, j)); // Keep prev TableCell
        else if (prev.getFlag() < 0) { // Frame is empty
          newRow.add(
              new TableCell(next, isWrite ? RM_BITS : R_BIT, row, j)); // Add to next empty frame
          pointer++;
          frames.add(next);
          break;
        } else if (prev.getFlag() % 2 > 0) {
          newRow.add(
              new TableCell(
                  prev.getPage(), prev.getFlag() >= M_BIT ? M_BIT : 0, row, j)); // Delete R-bit
          pointer++;
        } else { // r=0 overwrite frame
          newRow.add(new TableCell(next, isWrite ? RM_BITS : R_BIT, row, newRow.size()));
          pointer++;
          frames.remove((Integer) prev.getPage());
          frames.add(next);
          if (prev.getFlag() >= M_BIT) hasWriteBack = true;
          break;
        }
      }
      while (newRow.size()
          < pagingDiagram.getFrameCount()) { // Complete whole row after adding call
        TableCell prev = solution.get(row - 1).get(newRow.size());
        newRow.add(new TableCell(prev.getPage(), prev.getFlag(), row, newRow.size()));
      }
      if (!frames.contains(next)) {
        pointer = 0;
        for (int j = 0; j < newRow.size(); j++) {
          TableCell temp = newRow.get(j);
          if (temp.getFlag() % 2 > 0) {
            newRow.remove(j);
            newRow.add(
                j,
                new TableCell(
                    temp.getPage(), temp.getFlag() >= M_BIT ? M_BIT : 0, row, j)); // Delete R-bit
            pointer++;
          } else {
            newRow.remove(j);
            newRow.add(j, new TableCell(next, isWrite ? RM_BITS : R_BIT, row, j));
            pointer++;
            frames.remove((Integer) temp.getPage());
            frames.add(next);
            if (temp.getFlag() >= M_BIT) hasWriteBack = true;
            break;
          }
        }
      }
    }
    solution.add(newRow);

    if (hasWriteBack != pagingDiagram.getWriteBacks().contains(row)) wrongWriteBacks.add(row);
    if (!callList.isEmpty()) {
      calculateRounds(
          solution,
          pagingDiagram,
          callList,
          frames,
          wrongPageFaults,
          rwlist,
          wrongWriteBacks,
          pointer % pagingDiagram.getFrameCount());
    }
  }
}
