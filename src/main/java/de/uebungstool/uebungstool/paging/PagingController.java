package de.uebungstool.uebungstool.paging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uebungstool.uebungstool.paging.data.*;
import de.uebungstool.uebungstool.paging.services.ConfigService;
import de.uebungstool.uebungstool.paging.services.PagingService;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "paging")
public class PagingController {
  private static final Logger log = Logger.getLogger(PagingController.class.getName());

  @Autowired
  public PagingController() {
    log.info("Paging-Server started");
  }

  /**
   * API for students diagram submission
   *
   * @param string Client request
   * @return Server response
   */
  @PostMapping()
  public ResponseEntity<String> checkDiagram(@RequestBody String string) {
    log.finest("Received request " + string);
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      Set<Integer> wrongPageFaults = new HashSet<>();
      Set<Integer> wrongWriteBacks = new HashSet<>();
      PagingDiagram pagingDiagram = objectMapper.readValue(string, PagingDiagram.class);
      log.info(pagingDiagram.generateLogString());
      pagingDiagram.selfCheck();
      List<TableCell> mistakesList =
          PagingService.evaluate(pagingDiagram, wrongPageFaults, wrongWriteBacks);
      Object[] result = {mistakesList, wrongPageFaults, wrongWriteBacks};
      String response = objectMapper.writeValueAsString(result);
      log.finest("Given response: " + response);
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (JsonProcessingException e) {
      log.finest("Failed parsing request: " + e.getMessage());
      e.printStackTrace();
      return new ResponseEntity<>(
          "JSON String from Request could not be parsed into Paging-Structure",
          HttpStatus.BAD_REQUEST);
    } catch (LimitException e) {
      log.finest("Values does not fit to limit: " + e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
      return handleGenericException(e);
    }
  }

  /**
   * API for configurations
   *
   * @return Server response
   */
  @PostMapping(path = "config")
  public ResponseEntity<String> getSampleConfig() {
    log.finest("Received csv-request");
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      List<Config> configList = ConfigService.getSampleConfigs();
      configList.sort(new ConfigComparator());
      String response = objectMapper.writeValueAsString(configList);
      log.finest("Given config response: " + response);
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      return handleGenericException(e);
    }
  }

  private ResponseEntity<String> handleGenericException(Exception e) {
    log.warning("Failed Paging REST with error: " + e.getMessage());
    e.printStackTrace();
    return new ResponseEntity<>("Fatal Error", HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
