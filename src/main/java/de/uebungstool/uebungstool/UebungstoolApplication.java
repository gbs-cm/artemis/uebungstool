package de.uebungstool.uebungstool;

import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
public class UebungstoolApplication {

  public static void main(String[] args) {
    SpringApplication.run(UebungstoolApplication.class, args);
  }

  /**
   * Filter to allow Http requests from the domain of the Angular client (localhost:4200) reduce
   * number of possible requests to the actual needed requests
   *
   * @return CorsFilter
   */
  @Bean
  public CorsFilter corsFilter() {
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowCredentials(true);
    ArrayList<String> allowedOrigins = new ArrayList<>();
    allowedOrigins.add("http://localhost*");

    allowedOrigins.add("https://vmott42.in.tum.de*");

    allowedOrigins.add("https://uebungsplatform.cs.in.tum.edu*");
    allowedOrigins.add("https://uebungsplatform.cm.in.tum.de*");

    corsConfiguration.setAllowedOriginPatterns(allowedOrigins);
    corsConfiguration.setAllowedHeaders(
        Arrays.asList(
            "Origin",
            "Access-Control-Allow-Origin",
            "Content-Type",
            "Accept",
            "Authorization",
            "Origin, Accept",
            "X-Requested-With",
            "Access-Control-Request-Method",
            "Access-Control-Request-Headers"));
    corsConfiguration.setExposedHeaders(
        Arrays.asList(
            "Origin",
            "Content-Type",
            "Accept",
            "Authorization",
            "Access-Control-Allow-Origin",
            "Access-Control-Allow-Origin",
            "Access-Control-Allow-Credentials"));
    corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
    UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource =
        new UrlBasedCorsConfigurationSource();
    urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
    return new CorsFilter(urlBasedCorsConfigurationSource);
  }
}
