import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class HexdumpService {
  private apiUrl = 'http://131.159.24.224:8080/api/data';

  private dataSubject = new BehaviorSubject<any | null>(null);
  public data$ = this.dataSubject.asObservable();

  private markedCellsSubject = new BehaviorSubject<Set<number>>(new Set<number>());
  public markedCells$ = this.markedCellsSubject.asObservable();

  correctCells: Set<number> = new Set<number>();
  falseCells: Set<number> = new Set<number>();

  private highlightedLineSubject = new BehaviorSubject<number | null>(null);
  highlightedLine$ = this.highlightedLineSubject.asObservable();

  setHighlightedLine(lineNumber: number): void {
    this.highlightedLineSubject.next(lineNumber);
  }

  clearCells(): void {
    this.markedCellsSubject.next(new Set<number>());
    this.correctCells.clear();
    this.falseCells.clear();
  }

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  fetchData(): Observable<any> {
    if (!this.dataSubject.value) {
      this.refreshData();
    }
    return this.data$;
  }

  refreshData(): void {
    this.route.queryParams.subscribe((params) => {
      const seed = params.seed || '';
      const url = seed ? `${this.apiUrl}?seed=${seed}` : this.apiUrl;
      this.http.get(url).subscribe({
        next: (response) => this.dataSubject.next(response),
        error: (err) => console.error('Error refreshing data:', err),
      });
    });
  }

  sendPostData(data: { isBigEndian: boolean; is16Bit: boolean; seed: string }): void {
    this.http.post(`${this.apiUrl}`, data).subscribe({
      next: (response) => this.dataSubject.next(response),
      error: (err) => console.error('Error refreshing data:', err),
    });
  }

  getLineSequence(): Observable<number[]> {
    return this.data$.pipe(map((data) => data?.lineSequence || []));
  }

  getHexdump(): Observable<number[]> {
    return this.data$.pipe(map((data) => data?.hexdump || []));
  }

  getBaseAddress(): Observable<number> {
    return this.data$.pipe(map((data) => data?.baseAddress || 0));
  }

  getStringPresentation(): Observable<string> {
    return this.data$.pipe(map((data) => data?.struct?.string_presentation || ''));
  }

  getMsg(): Observable<string> {
    return this.data$.pipe(map((data) => data?.msg || ''));
  }

  getIsBigEndian(): Observable<boolean> {
    return this.data$.pipe(map((data) => data?.isBigEndian || false));
  }

  getIs16Bit(): Observable<boolean> {
    return this.data$.pipe(map((data) => data?.is16Bit || false));
  }

  getSeed(): Observable<string> {
    return this.data$.pipe(map((data) => data?.seed || ''));
  }

  getSolution(): Observable<Array<[string, string | number[]]>> {
    return this.data$.pipe(map((data) => data?.solution || []));
  }

  addMarkedCell(index: number): void {
    const markedCells = new Set(this.markedCellsSubject.value);
    markedCells.add(index);
    this.markedCellsSubject.next(markedCells);
  }

  deleteMarkedCell(index: number): void {
    const markedCells = new Set(this.markedCellsSubject.value);
    markedCells.delete(index);
    this.markedCellsSubject.next(markedCells);
  }

  get markedCells(): Set<number> {
    return this.markedCellsSubject.value;
  }
}
