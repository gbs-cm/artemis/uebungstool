import { TestBed } from '@angular/core/testing';

import { HexdumpService } from './hexdump.service';

describe('HexdumpService', () => {
  let service: HexdumpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HexdumpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
