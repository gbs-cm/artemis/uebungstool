import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoggingService {
  private logUrl = 'http://131.159.24.224:8080/api/logs';

  constructor(private http: HttpClient) {}

  logEvent(logInfo: string) {
    const postData = {
      logInfo: logInfo,
    };
    console.log('Logging:', logInfo);
    this.http.post(this.logUrl, postData).subscribe({
      error: (err) => console.error('Failed to send log:', err),
    });
  }

  private ipApiUrl = 'https://api.ipify.org?format=json';

  getHashedClientIp(): Observable<string> {
    return this.http.get<{ ip: string }>(this.ipApiUrl).pipe(
      map((data) => this.extractLastPart(data.ip)),
      switchMap((lastPart) => from(this.hashString(lastPart)))
    );
  }

  private extractLastPart(ip: string): string {
    const ipParts = ip.split('.');
    return ipParts[ipParts.length - 1];
  }

  private async hashString(value: string): Promise<string> {
    const encoder = new TextEncoder();
    const data = encoder.encode(value);
    const hashBuffer = await crypto.subtle.digest('SHA-256', data);
    return Array.from(new Uint8Array(hashBuffer))
      .map((byte) => byte.toString(16).padStart(2, '0'))
      .join('');
  }
}
