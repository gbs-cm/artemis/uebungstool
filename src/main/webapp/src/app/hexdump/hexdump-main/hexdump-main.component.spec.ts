import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexdumpMainComponent } from './hexdump-main.component';

describe('HexdumpMainComponent', () => {
  let component: HexdumpMainComponent;
  let fixture: ComponentFixture<HexdumpMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HexdumpMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexdumpMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
