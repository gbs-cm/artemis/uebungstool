import { Component, OnInit } from '@angular/core';
import { HexdumpService } from '../hexdump-logic/services/hexdump.service';

@Component({
  selector: 'app-hexdump-main',
  templateUrl: './hexdump-main.component.html',
  styleUrls: ['./hexdump-main.component.scss'],
})
export class HexdumpMainComponent implements OnInit {
  constructor(private hexdumpService: HexdumpService) {}

  ngOnInit(): void {
    this.hexdumpService.fetchData().subscribe({
      next: () => {
        console.log('Data fetched successfully');
      },
      error: (err) => {
        console.error('Error fetching data:', err);
      },
    });
  }
}
