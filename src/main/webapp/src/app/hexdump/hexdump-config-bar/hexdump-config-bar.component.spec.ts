import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexdumpConfigBarComponent } from './hexdump-config-bar.component';

describe('HexdumpConfigBarComponent', () => {
  let component: HexdumpConfigBarComponent;
  let fixture: ComponentFixture<HexdumpConfigBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HexdumpConfigBarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexdumpConfigBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
