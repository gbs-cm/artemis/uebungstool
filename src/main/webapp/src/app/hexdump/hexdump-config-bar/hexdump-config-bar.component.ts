import { Component, OnInit } from '@angular/core';
import { HexdumpService } from '../hexdump-logic/services/hexdump.service';
import { faRandom, faCopy, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hexdump-config-bar',
  templateUrl: './hexdump-config-bar.component.html',
  styleUrls: ['./hexdump-config-bar.component.scss'],
})
export class HexdumpConfigBarComponent implements OnInit {
  constructor(private hexdumpService: HexdumpService, private route: ActivatedRoute, private router: Router) {}

  public isBigEndian = false;
  public is16Bit = false;
  public seed = '';

  faRandom = faRandom;
  faCopy = faCopy;
  faCheck = faCheck;
  copied = false;

  ngOnInit(): void {
    this.update_config();

    this.route.queryParams.subscribe((params) => {
      this.seed = params.seed || '';
    });
  }

  update_config(): void {
    this.hexdumpService.getIsBigEndian().subscribe({
      next: (data) => {
        this.isBigEndian = data;
      },
      error: (err) => {
        console.error('Error fetching config:', err);
      },
    });

    this.hexdumpService.getIs16Bit().subscribe({
      next: (data) => {
        this.is16Bit = data;
      },
      error: (err) => {
        console.error('Error fetching config:', err);
      },
    });

    this.hexdumpService.getSeed().subscribe({
      next: (data) => {
        this.seed = data;
      },
      error: (err) => {
        console.error('Error fetching config:', err);
      },
    });
  }

  generateRandomSeed(): void {
    const randomSeed = (Math.floor(Math.random() * (9999999 - 11 + 1)) + 11) * 10 + (Math.floor(Math.random() * 4) + 1);
    this.seed = randomSeed.toString();
  }

  clearSeed(event: Event): void {
    this.seed = '';
    this.submitData();
  }

  copyUrlToClipboard() {
    let url = 'https://vmott42.in.tum.de/hexdump' + '?seed=' + this.seed;
    if (this.hexdumpService.markedCells.size > 0) {
      url += '&cells=' + this.markedCellsToHex();
    }
    navigator.clipboard.writeText(url).then(() => {
      this.copied = true;

      setTimeout(() => {
        this.copied = false;
      }, 2000);
    });
  }

  private markedCellsToHex(): string {
    let binaryString = '0'.repeat(256);

    this.hexdumpService.markedCells.forEach((index) => {
      if (index >= 0 && index < 256) {
        binaryString = binaryString.substring(0, index) + '1' + binaryString.substring(index + 1);
      }
    });

    const hexString = this.binaryToHex(binaryString);

    return hexString;
  }

  private binaryToHex(binaryString: string): string {
    let hexString = '';
    for (let i = 0; i < binaryString.length; i += 4) {
      const chunk = binaryString.substring(i, i + 4);
      const hexDigit = parseInt(chunk, 2).toString(16);
      hexString += hexDigit;
    }

    return hexString.replace(/^0+/, '') || '0';
  }

  submitData(): void {
    this.hexdumpService.clearCells();

    const postData = {
      isBigEndian: this.isBigEndian,
      is16Bit: this.is16Bit,
      seed: this.seed,
    };
    this.hexdumpService.sendPostData(postData);
    this.router.navigate([], {
      queryParams: {},
      queryParamsHandling: '',
    });
  }
}
