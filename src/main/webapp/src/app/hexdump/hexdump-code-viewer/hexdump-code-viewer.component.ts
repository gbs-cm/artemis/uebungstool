import { Component, OnInit } from '@angular/core';
import { HexdumpService } from '../hexdump-logic/services/hexdump.service';

@Component({
  selector: 'app-hexdump-code-viewer',
  templateUrl: './hexdump-code-viewer.component.html',
  styleUrls: ['./hexdump-code-viewer.component.scss'],
})
export class HexdumpCodeViewerComponent implements OnInit {
  code = '';
  highlightedLine: number | null = null;

  constructor(private hexdumpService: HexdumpService) {}

  ngOnInit(): void {
    this.hexdumpService.getStringPresentation().subscribe({
      next: (data) => {
        this.code = data + '\n';
      },
      error: (err) => {
        console.error('Error fetching string presentation:', err);
      },
    });

    this.hexdumpService.getMsg().subscribe({
      next: (data) => {
        this.code += data;
      },
      error: (err) => {
        console.error('Error fetching string presentation:', err);
      },
    });

    this.hexdumpService.highlightedLine$.subscribe((lineNumber) => {
      this.highlightedLine = lineNumber;
      this.updateArrowPosition();
    });
  }

  updateArrowPosition(): void {
    let lines = this.code.split('\n');
    lines = lines.map((line) => line.replace(/\s*⮜⮜⮜$/, ''));
    if (this.highlightedLine >= 1 && this.highlightedLine <= lines.length) {
      lines[this.highlightedLine - 1] += ' ⮜⮜⮜';
    }

    this.code = lines.join('\n');
  }
}
