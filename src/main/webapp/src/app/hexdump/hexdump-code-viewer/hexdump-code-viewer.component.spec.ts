import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexdumpCodeViewerComponent } from './hexdump-code-viewer.component';

describe('HexdumpConfigBarComponent', () => {
  let component: HexdumpCodeViewerComponent;
  let fixture: ComponentFixture<HexdumpCodeViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HexdumpCodeViewerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexdumpCodeViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
