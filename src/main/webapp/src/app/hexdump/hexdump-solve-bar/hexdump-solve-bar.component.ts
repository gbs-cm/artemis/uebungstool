import { Component, OnInit } from '@angular/core';
import { HexdumpService } from '../hexdump-logic/services/hexdump.service';
import { LoggingService } from '../hexdump-logic/services/logging.service';

@Component({
  selector: 'app-hexdump-solve-bar',
  templateUrl: './hexdump-solve-bar.component.html',
  styleUrls: ['./hexdump-solve-bar.component.scss'],
})
export class HexdumpSolveBarComponent implements OnInit {
  question = '';
  userAnswer = '';
  feedback = '';
  isCorrect: boolean | null = null;
  hexInput = '';
  decimalOutput: number | null = null;
  solution: Array<[string, string | number[]]> = [];
  lineSequence: number[] = [];
  currentQuestionIndex = 0;

  toolRating = 0;
  isFeedbackSent = false;

  isUserAnswerDisabled = true;
  isSolveButtonDisabled = false;
  isCheckButtonDisabled = false;
  isNextButtonDisabled = false;

  asciiTable: { char: string; hex: string }[][] = [];
  isAsciiTableVisible = false;

  hashedLastPart = '';

  constructor(private hexdumpService: HexdumpService, private loggingService: LoggingService) {
    this.generateAsciiTable();
  }

  ngOnInit(): void {
    this.hexdumpService.getSolution().subscribe((solution) => {
      this.solution = solution;
      if (this.solution.length > 0) {
        this.isSolveButtonDisabled = false;
        this.isCheckButtonDisabled = false;
        this.isNextButtonDisabled = false;
        this.isUserAnswerDisabled = true;
        this.currentQuestionIndex = 0;
        this.question = this.solution[this.currentQuestionIndex][0];
        this.userAnswer = '';
      }
    });

    this.hexdumpService.getLineSequence().subscribe((lineSequence) => {
      this.lineSequence = lineSequence;
      this.hexdumpService.setHighlightedLine(this.lineSequence[this.currentQuestionIndex]);
    });

    this.hexdumpService.markedCells$.subscribe((markedCells) => {
      if (markedCells.size > 0) {
        const minCell = Math.min(...Array.from(markedCells));
        const maxCell = Math.max(...Array.from(markedCells));
        if (this.isUserAnswerDisabled) {
          this.userAnswer = `${minCell} - ${maxCell}`;
        }
      } else {
        this.userAnswer = '';
      }
    });

    this.loggingService.getHashedClientIp().subscribe(
      (hashedIp) => {
        this.hashedLastPart = hashedIp;
      },
      (error) => {
        console.error('Error fetching or hashing IP:', error);
      }
    );
  }

  nextQuestion(): void {
    this.loggingService.logEvent(
      'next_pressed: ' +
        this.question +
        ' | correct: ' +
        this.isCorrect +
        ' | wasSolved: ' +
        this.isSolveButtonDisabled +
        ' | ip: ' +
        this.hashedLastPart
    );
    this.isSolveButtonDisabled = false;
    this.isCheckButtonDisabled = false;
    this.isNextButtonDisabled = false;
    this.isUserAnswerDisabled = false;
    this.hexdumpService.clearCells();
    this.currentQuestionIndex++;
    if (this.currentQuestionIndex >= this.solution.length) {
      this.question = 'Done!';
      this.isUserAnswerDisabled = true;
      this.isSolveButtonDisabled = true;
      this.isCheckButtonDisabled = true;
      this.isNextButtonDisabled = true;
      this.hexdumpService.setHighlightedLine(-1);
    } else {
      const answer = this.solution[this.currentQuestionIndex][1];
      if (typeof answer === 'string') {
        this.isUserAnswerDisabled = false;
      } else if (Array.isArray(answer)) {
        this.isUserAnswerDisabled = true;
      }
      this.question = this.solution[this.currentQuestionIndex][0];
      this.hexdumpService.setHighlightedLine(this.lineSequence[this.currentQuestionIndex]);
    }
    this.userAnswer = '';
    this.isCorrect = null;
  }

  solveQuestion(): void {
    this.isCorrect = null;
    this.isSolveButtonDisabled = true;
    this.isCheckButtonDisabled = true;
    this.isNextButtonDisabled = false;
    this.isUserAnswerDisabled = true;
    this.hexdumpService.clearCells();
    const answer = this.solution[this.currentQuestionIndex][1];
    if (typeof answer === 'string') {
      this.userAnswer = answer;
    } else if (Array.isArray(answer)) {
      answer.forEach((cell) => {
        this.hexdumpService.correctCells.add(cell);
      });
    }

    this.loggingService.logEvent('solved_pressed: ' + this.question + ' | ip: ' + this.hashedLastPart);
  }

  checkAnswer(): void {
    this.hexdumpService.falseCells.clear();
    const answer = this.solution[this.currentQuestionIndex][1];
    if (typeof answer === 'string') {
      this.isCorrect = this.userAnswer === answer;
    } else if (Array.isArray(answer)) {
      this.hexdumpService.markedCells.forEach((cell) => {
        if (answer.includes(cell)) {
          this.hexdumpService.correctCells.add(cell);
        } else {
          this.hexdumpService.falseCells.add(cell);
        }
        this.hexdumpService.markedCells.delete(cell);
      });
      if (this.hexdumpService.correctCells.size === answer.length && this.hexdumpService.falseCells.size === 0) {
        this.isCorrect = true;
      } else {
        this.isCorrect = false;
      }
    }

    if (this.isCorrect) {
      this.isCheckButtonDisabled = true;
      this.isSolveButtonDisabled = true;
      this.isUserAnswerDisabled = true;
    }
    this.loggingService.logEvent(
      'check_pressed: ' + this.question + ' | correct: ' + this.isCorrect + ' | ip: ' + this.hashedLastPart
    );
  }

  sendFeedback(): void {
    this.loggingService.logEvent(
      'feedback: ' + this.feedback + ' | rating: ' + this.toolRating + ' | ip: ' + this.hashedLastPart
    );
    this.feedback = '';
    this.isFeedbackSent = true;
    this.feedback = 'Thank you for your feedback!';
  }

  updateDecimalOutput(): void {
    const isValidHex = /^[0-9a-fA-F]*$/.test(this.hexInput);
    if (isValidHex) {
      this.decimalOutput = this.hexInput ? parseInt(this.hexInput, 16) : null;
    } else {
      this.decimalOutput = null;
    }
  }

  toggleAsciiTable(): void {
    this.isAsciiTableVisible = !this.isAsciiTableVisible;
  }

  private generateAsciiTable(): void {
    const asciiChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const rows = Array.from(asciiChars).map((char) => ({
      char,
      hex: char.charCodeAt(0).toString(16).toUpperCase(),
    }));

    const columns = 7;
    for (let i = 0; i < rows.length; i += columns) {
      this.asciiTable.push(rows.slice(i, i + columns));
    }
  }
}
