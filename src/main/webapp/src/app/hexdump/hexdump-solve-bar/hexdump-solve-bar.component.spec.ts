import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexdumpSolveBarComponent } from './hexdump-solve-bar.component';

describe('HexdumpSolveBarComponent', () => {
  let component: HexdumpSolveBarComponent;
  let fixture: ComponentFixture<HexdumpSolveBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HexdumpSolveBarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexdumpSolveBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
