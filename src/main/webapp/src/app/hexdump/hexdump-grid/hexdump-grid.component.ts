import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HexdumpService } from '../hexdump-logic/services/hexdump.service';

@Component({
  selector: 'app-hexdump-grid',
  templateUrl: './hexdump-grid.component.html',
  styleUrls: ['./hexdump-grid.component.scss'],
})
export class HexdumpGridComponent implements OnInit {
  cells: string[] = Array.from({ length: 256 }, (_, i) => i.toString(16).toUpperCase().padStart(2, '0'));

  baseAddress = 0xbfffc700;

  isMouseDown = false;
  toggleMode = true;

  constructor(private hexdumpService: HexdumpService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.hexdumpService.getHexdump().subscribe({
      next: (hexdump) => {
        this.cells = hexdump.map((num) => num.toString(16).toLowerCase().padStart(2, '0'));
      },
      error: (error) => {
        console.error('Error fetching hexdump:', error);
      },
    });

    this.hexdumpService.getBaseAddress().subscribe({
      next: (data) => {
        this.baseAddress = data;
      },
      error: (error) => {
        console.error('Error fetching hexdump:', error);
      },
    });

    this.route.queryParams.subscribe((params) => {
      const cells = params.cells;

      if (cells && /^[0-9a-fA-F]{1,64}$/.test(cells)) {
        const binaryString = this.hexTo256BitBinary(cells);

        this.populateMarkedCellsFromBinary(binaryString);
      }
    });
  }

  private hexTo256BitBinary(hex: string): string {
    while (hex.length < 64) {
      hex = '0' + hex;
    }

    let binaryString = '';
    for (let i = 0; i < hex.length; i++) {
      const hexChar = hex[i];
      const decimal = parseInt(hexChar, 16);
      const binary = decimal.toString(2).padStart(4, '0');
      binaryString += binary;
    }

    return binaryString;
  }

  private populateMarkedCellsFromBinary(binaryString: string): void {
    for (let i = 0; i < binaryString.length; i++) {
      if (binaryString[i] === '1') {
        this.hexdumpService.addMarkedCell(i);
      }
    }
  }

  get markedCells(): Set<number> {
    return this.hexdumpService.markedCells;
  }

  get correctCells(): Set<number> {
    return this.hexdumpService.correctCells;
  }

  get falseCells(): Set<number> {
    return this.hexdumpService.falseCells;
  }

  getRowAddress(rowIndex: number): string {
    return `0x${(this.baseAddress + rowIndex * 16).toString(16).toLowerCase().padStart(8, '0')}:`;
  }

  onMouseDown(index: number): void {
    this.isMouseDown = true;
    this.toggleMode =
      !this.hexdumpService.markedCells.has(index) &&
      !this.hexdumpService.correctCells.has(index) &&
      !this.hexdumpService.falseCells.has(index);
    this.updateMark(index);
  }

  @HostListener('document:mouseup')
  onMouseUp(): void {
    this.isMouseDown = false;
  }

  onMouseEnter(index: number): void {
    if (this.isMouseDown) {
      this.updateMark(index);
    }
  }

  private updateMark(index: number): void {
    if (this.toggleMode) {
      this.hexdumpService.addMarkedCell(index);
    } else {
      this.hexdumpService.deleteMarkedCell(index);
      this.hexdumpService.correctCells.delete(index);
      this.hexdumpService.falseCells.delete(index);
    }
  }
}
