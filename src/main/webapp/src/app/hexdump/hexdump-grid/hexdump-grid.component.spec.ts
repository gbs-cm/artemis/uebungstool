import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexdumpGridComponent } from './hexdump-grid.component';

describe('HexdumpConfigBarComponent', () => {
  let component: HexdumpGridComponent;
  let fixture: ComponentFixture<HexdumpGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HexdumpGridComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexdumpGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
