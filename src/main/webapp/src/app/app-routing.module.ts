import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchedulingMainComponent } from './scheduling/scheduling-main/scheduling-main.component';
import { PagingMainComponent } from './paging/paging-main/paging-main.component';
import { DefaultPageComponent } from './default-page/default-page.component';
import { HexdumpMainComponent } from './hexdump/hexdump-main/hexdump-main.component';

const routes: Routes = [
  { path: 'hexdump', component: HexdumpMainComponent },
  { path: 'scheduling', component: SchedulingMainComponent },
  { path: 'paging', component: PagingMainComponent },
  { path: '**', component: DefaultPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
