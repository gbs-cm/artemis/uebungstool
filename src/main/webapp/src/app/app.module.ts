import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SchedulingMainComponent } from './scheduling/scheduling-main/scheduling-main.component';
import { PagingMainComponent } from './paging/paging-main/paging-main.component';
import { SchedulingConfigurationComponent } from './scheduling/scheduling-configuration-general/scheduling-configuration.component';
import { SchedulingDiagramComponent } from './scheduling/scheduling-diagram/scheduling-diagram.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragToSelectModule } from 'ngx-drag-to-select';
import { PagingConfigurationComponent } from './paging/paging-configuration/paging-configuration.component';
import { PagingDiagramComponent } from './paging/paging-diagram/paging-diagram.component';
import { ThreadConfigurationComponent } from './scheduling/scheduling-configuration-thread/thread-configuration.component';
import { PagingClockComponent } from './paging/paging-clock/paging-clock.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DefaultPageComponent } from './default-page/default-page.component';
import { HexdumpMainComponent } from './hexdump/hexdump-main/hexdump-main.component';
import { HexdumpGridComponent } from './hexdump/hexdump-grid/hexdump-grid.component';
import { HexdumpCodeViewerComponent } from './hexdump/hexdump-code-viewer/hexdump-code-viewer.component';
import { ChunkPipe } from './hexdump/hexdump-grid/chunk.pipe';
import { HexdumpConfigBarComponent } from './hexdump/hexdump-config-bar/hexdump-config-bar.component';
import { HexdumpSolveBarComponent } from './hexdump/hexdump-solve-bar/hexdump-solve-bar.component';

// AOT compilation support
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HexdumpMainComponent,
    SchedulingMainComponent,
    PagingMainComponent,
    HexdumpCodeViewerComponent,
    HexdumpGridComponent,
    ChunkPipe,
    SchedulingConfigurationComponent,
    SchedulingDiagramComponent,
    ThreadConfigurationComponent,
    PagingConfigurationComponent,
    PagingDiagramComponent,
    PagingClockComponent,
    DefaultPageComponent,
    HexdumpConfigBarComponent,
    HexdumpSolveBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule,
    ClipboardModule,
    DragToSelectModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [TranslateModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
