import { Component, OnInit } from '@angular/core';
import { PagingService } from '../paging-service/paging.service';
import { Diagram } from '../paging-service/diagram/Diagram';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { faQuestion, faCheck, faRandom } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-paging-configuration',
  templateUrl: './paging-configuration.component.html',
  styleUrls: ['./paging-configuration.component.scss'],
})
export class PagingConfigurationComponent implements OnInit {
  readonly faQuestion = faQuestion;
  readonly faCheck = faCheck;
  readonly faRandom = faRandom;
  constructor(public pagingService: PagingService, private activatedRoute: ActivatedRoute) {
    this.pagingService.updateDiagramSizes(this.pagingService.roundCount, this.pagingService.frameCount);
    this.getConfigFromURL(activatedRoute.snapshot.queryParamMap);
  }

  public configType = false;
  public showManual = false;

  /**
   * updates the browsers url bar with new link
   * @param link the new url bars content
   * @private
   */
  private static updateURL(link: string): void {
    window.history.pushState(null, document.title, link);
  }

  ngOnInit(): void {
    // Stays empty
  }

  /**
   * changes the configuration type between csv and simple values
   * @param werte false sets simple values, true csv
   */
  changeConfigType(werte: boolean): void {
    if (this.configType !== werte) {
      this.configType = werte;
    }
  }

  /**
   * generates and sets new random configuration
   */
  loadRandomConfig(): void {
    this.pagingService.clear();
    this.pagingService.setRandomCallList(-1, -1);
    this.pagingService.setRandomStrategy();
    this.pagingService.setRandomFrameCount();
    this.pagingService.updateDiagramSizes(this.pagingService.roundCount, this.pagingService.frameCount);
  }

  /**
   * Load sample from Server*/
  loadDiagramFromConfig(key: string): void {
    this.pagingService.clear();
    const configDiagram: Diagram = this.pagingService.sampleConfigList.get(key);

    if (configDiagram === null) {
      console.log('Fatal error. Could not load ' + key);
      return;
    }

    let hasRandomValue = false;

    if (configDiagram.strategy !== null && this.pagingService.algorithms.has(configDiagram.strategy)) {
      this.pagingService.diagram.strategy = configDiagram.strategy;
    } else {
      this.pagingService.setRandomStrategy();
      hasRandomValue = true;
    }

    if (configDiagram.callList !== null && configDiagram.callList.length > 0) {
      this.pagingService.updateCSV(configDiagram.callList.join());
    } else {
      this.pagingService.setRandomCallList(-1, -1);
      hasRandomValue = true;
    }

    if (configDiagram.rwList !== null && configDiagram.rwList.length > 0) {
      this.pagingService.updateRWList(configDiagram.rwList.join());
    } else {
      if (this.pagingService.doubleFlagAlgorithms.includes(this.pagingService.diagram.strategy)) {
        hasRandomValue = true;
      }
      this.pagingService.setRandomRWList();
    }

    if (configDiagram.frameCount > 0) {
      this.pagingService.updateFrameCount(configDiagram.frameCount);
    } else {
      this.pagingService.setRandomFrameCount();
      hasRandomValue = true;
    }

    if (!hasRandomValue) {
      this.loadDiagramWithoutRandomValues(configDiagram);
    }
  }

  /**
   * Loads a given table
   * @param configDiagram
   * @private
   */
  private loadDiagramWithoutRandomValues(configDiagram: Diagram): void {
    if (
      configDiagram.diagram !== null &&
      configDiagram.diagram.length > 0 &&
      configDiagram.diagram[0] !== null &&
      configDiagram.diagram[0].length > 0
    ) {
      for (const row of configDiagram.diagram) {
        for (const tableCell of row) {
          if (tableCell.page > 0) {
            this.pagingService.updateDatum(tableCell.i, tableCell.j, tableCell.page);
          }
          if (tableCell.flag > -1) {
            this.pagingService.updateFlag(tableCell.i, tableCell.j, tableCell.flag);
          }
        }
      }
      this.pagingService.updateDiagramSizes(configDiagram.callList.length, configDiagram.frameCount);
    }
    this.setFaultsFromConfig(configDiagram);
  }

  /**
   * Sets page faults and write backs as given in configuration diagram
   * @param configDiagram
   * @private
   */
  private setFaultsFromConfig(configDiagram: Diagram): void {
    if (configDiagram.pageFaults !== null && configDiagram.pageFaults.size > 0) {
      this.pagingService.diagram.pageFaults = configDiagram.pageFaults;
    }
    if (configDiagram.writeBacks !== null && configDiagram.writeBacks.size > 0) {
      this.pagingService.diagram.writeBacks = configDiagram.writeBacks;
    }
  }

  /**
   * generates link from active configuration
   * @return string link
   */
  getConfigLink(): string {
    const splitArray = window.location.href.split('?');
    let link = splitArray.length > 0 ? splitArray[0] : '';
    link += '?iscsv=' + this.configType;
    link += '&strategy=' + this.pagingService.diagram.strategy;
    link += '&frames=' + this.pagingService.frameCount;
    if (this.configType) {
      link += '&csv=' + this.pagingService.diagram.callList.join();
      if (this.pagingService.rwAlgorithms.includes(this.pagingService.diagram.strategy)) {
        link += '&rw=' + this.pagingService.diagram.getRWListString();
      }
    } else {
      link += '&pages=' + this.pagingService.pageCount;
      link += '&rounds=' + this.pagingService.roundCount;
    }

    PagingConfigurationComponent.updateURL(link);

    return link;
  }

  /**
   * generates configuration from url
   * @param queryParamMap url parameters
   * @private
   */
  private getConfigFromURL(queryParamMap: ParamMap): void {
    if (queryParamMap.has('iscsv')) {
      this.changeConfigType('true' === queryParamMap.get('iscsv'));
    }

    if (queryParamMap.has('strategy') && this.pagingService.algorithms.has(queryParamMap.get('strategy'))) {
      this.pagingService.diagram.strategy = queryParamMap.get('strategy');
    }

    if (this.configType) {
      if (queryParamMap.has('csv')) {
        this.pagingService.updateCSV(queryParamMap.get('csv'));
      }

      if (queryParamMap.has('rw')) {
        this.pagingService.updateRWList(queryParamMap.get('rw'));
      }
    } else {
      if (queryParamMap.has('pages')) {
        this.pagingService.updatePageCount(+queryParamMap.get('pages'));
      }

      if (queryParamMap.has('rounds')) {
        this.pagingService.updateRoundCount(+queryParamMap.get('rounds'));
      }
    }

    if (queryParamMap.has('frames')) {
      this.pagingService.updateFrameCount(+queryParamMap.get('frames'));
    }
  }

  /**
   * enables and disables the guide
   */
  toggleManual(): void {
    this.showManual = !this.showManual;
  }
}
