import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingConfigurationComponent } from './paging-configuration.component';

describe('PagingConfigurationComponent', () => {
  let component: PagingConfigurationComponent;
  let fixture: ComponentFixture<PagingConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PagingConfigurationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
