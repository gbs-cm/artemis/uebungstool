import { AfterViewInit, Component, Renderer2 } from '@angular/core';
import { PagingService } from '../paging-service/paging.service';
import { PagingDiagramComponent } from '../paging-diagram/paging-diagram.component';

@Component({
  selector: 'app-paging-clock',
  templateUrl: './paging-clock.component.html',
  styleUrls: ['./paging-clock.component.scss'],
})
export class PagingClockComponent implements AfterViewInit {
  round = 0;

  constructor(
    public pagingService: PagingService,
    private renderer: Renderer2,
    public pagingDiagram: PagingDiagramComponent
  ) {}

  ngAfterViewInit(): void {
    this.setActiveClockCall(0);
  }

  /**
   * return the offset height of the clock divs
   */
  getHeight(): number {
    return document.getElementsByClassName('clock-div').length > 0
      ? (document.getElementsByClassName('clock-div').item(0) as HTMLElement).offsetHeight
      : 0;
  }

  /**
   * Sets the clock to selected round
   * @param round
   */
  selectRound(round: number): void {
    this.round = round;
    let frame = -1;
    for (let i = round; i >= 0; i--) {
      if (frame >= 0) {
        break;
      }
      for (let j = 0; j < this.pagingService.diagram.frameCount; j++) {
        if (this.pagingService.diagram.diagram[i][j].page > -1) {
          if (this.pagingService.diagram.diagram[i][j].page !== this.pagingService.placeholderDiagram[i][j].page) {
            frame = j;
            break;
          }
        }
      }
    }
    if (frame > -1) {
      const step = 360 / this.pagingService.diagram.frameCount;
      const rotation = frame * step;
      this.renderer.setStyle(document.getElementById('clock-mid'), 'transform', 'rotate(' + (rotation + step) + 'deg');
    } else {
      this.renderer.setStyle(document.getElementById('clock-mid'), 'transform', 'rotate(0deg');
    }
    this.setActiveClockCall(round);
  }

  /**
   * Sets the next or previous round to active
   * @param prev optional parameter to set the previous round if true
   */
  selectNextRound(prev = false): void {
    if (prev && this.round - 1 >= 0) {
      this.selectRound(this.round - 1);
    } else if (!prev && this.round + 1 < this.pagingService.diagram.callList.length) {
      this.selectRound(this.round + 1);
    }
  }

  /**
   * marks the navigation button for the new active round
   * @param round
   */
  setActiveClockCall(round: number): void {
    const list = document.getElementsByClassName('btn btn-secondary button-call active');
    for (let i = 0; i < list.length; i++) {
      list.item(i).classList.remove('active');
    }
    document.getElementById('button-call-' + round).classList.add('active');
  }

  /**
   * Updates a TableCell within the clock display and updates arrow direction
   * @param i index round
   * @param j index frame
   * @param target input field
   * @param round
   */
  updateDatum(i: number, j: number, target: EventTarget, round: number): void {
    this.pagingService.updateDatum(i, j, +(target as HTMLInputElement).value);
    this.selectRound(round);
  }
}
