import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingClockComponent } from './paging-clock.component';

describe('PagingClockComponent', () => {
  let component: PagingClockComponent;
  let fixture: ComponentFixture<PagingClockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PagingClockComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
