import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingMainComponent } from './paging-main.component';

describe('PagingMainComponent', () => {
  let component: PagingMainComponent;
  let fixture: ComponentFixture<PagingMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PagingMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
