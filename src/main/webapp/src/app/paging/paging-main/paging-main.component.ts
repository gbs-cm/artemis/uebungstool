import { Component, OnInit } from '@angular/core';
import { PagingService } from '../paging-service/paging.service';

@Component({
  selector: 'app-paging-main',
  templateUrl: './paging-main.component.html',
  styleUrls: ['./paging-main.component.scss'],
})
export class PagingMainComponent implements OnInit {
  constructor(public pagingService: PagingService) {}

  ngOnInit(): void {
    // Stays empty
  }
}
