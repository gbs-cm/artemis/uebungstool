import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Diagram } from './diagram/Diagram';
import { TableCell } from './diagram/TableCell';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Config } from './diagram/Config';

@Injectable({
  providedIn: 'root',
})
export class PagingService {
  constructor(private http: HttpClient, public diagram: Diagram) {
    this.getConfigurationSamples();
  }

  get frameCount(): number {
    return this.diagram.frameCount;
  }

  set frameCount(value: number) {
    this.diagram.frameCount = value;
  }

  public placeholderDiagram: TableCell[][] = [[]];
  public wrongPageFaults: Set<number> = new Set();
  public wrongWriteBacks: Set<number> = new Set();
  public pageCount = 5; // Number of Pages to handle
  public minPageCount = 1; // Min-limit for Number of Pages
  public maxPageCount = 32; // Max-limit for Number of Pages
  public minFrameCount = 1; // Min-limit for Number of Frames
  public roundCount = 10; // Number of Paging actions
  public minRoundCount = 1; // Min-limit for Paging
  public maxRoundCount = 128; // Max-limit for Paging
  public sampleConfigList: Map<string, Diagram> = new Map();
  public pageFaultsCorrected = false;
  public writeBacksCorrected = false;
  public algorithms: Map<string, string> = new Map([
    // ["age","Aging"],
    ['clock', 'Clock'],
    ['fifo', 'First-in, First-out'],
    ['lru', 'Least recently used'],
    // ["nru","Not recently used"],
    ['nfu', 'Not frequently used'],
    // ["sc","Second chance"],
    // ["opt","Theoretically optimal page replacement"]
  ]);
  public flagAlgorithms: string[] = ['clock', 'lru', 'nfu'];
  public doubleFlagAlgorithms: string[] = ['clock'];
  public rwAlgorithms: string[] = ['clock'];

  /*Helping functions*/
  /**
   * helper function for JSON conversion
   * @param key
   * @param value
   * @private
   * @return string[]
   */
  private static replacer(key, value): string[] {
    if (key === '_wrong' || key === 'wrong') {
      return undefined;
    } else if (key === 'pageFaults' || key === 'writeBacks') {
      return [...value.keys()];
    }
    return value;
  }

  /**
   * limits values to given boundaries
   * @param testValue
   * @param minValue
   * @param maxValue
   * @return checked number
   */
  public static bound(testValue: number, minValue: number, maxValue: number): number {
    if (testValue >= minValue) {
      if (testValue <= maxValue) {
        return testValue;
      }
      return maxValue;
    }
    return minValue;
  }

  /**
   * generates a random number from 1 to max
   * @param max
   * @return random number
   */
  public static random(max: number): number {
    return Math.floor(Math.random() * max);
  }

  /*Server communication generals:*/
  /**
   * Error handling
   * @param error
   * @private
   */
  private static handleError(error: HttpErrorResponse): Observable<never> {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

  /**
   * updates the callList to given length
   * @param newRounds
   */
  public updateCallList(newRounds: number): void {
    if (newRounds !== this.diagram.callList.length) {
      // No CSV-Call -> generate CSV
      if (newRounds < this.diagram.callList.length) {
        this.diagram.callList.length = newRounds; // Array shortening
        this.diagram.rwList.length = newRounds;
        this.diagram.trimPageFaults(newRounds);
      } else {
        while (newRounds > this.diagram.callList.length) {
          // Neue Zufällige Seite an Liste anfügen
          this.diagram.callList.push(PagingService.random(this.pageCount) + 1);
          this.diagram.addOneRWItem();
        }
      }
    }
  }

  /*Diagram size management*/
  /**
   * sets new sizes for the diagram
   * @param newRounds
   * @param newFrames
   */
  public updateDiagramSizes(newRounds: number, newFrames: number): void {
    if (
      this.diagram.diagram === null ||
      this.diagram.diagram.length !== this.roundCount ||
      this.roundCount !== newRounds ||
      this.diagram.diagram[0] === null ||
      this.diagram.diagram[0].length !== this.frameCount ||
      this.frameCount !== newFrames
    ) {
      this.updateCallList(newRounds);
      this.updateDiagramSizesHelper(newRounds, newFrames, this.diagram.diagram);
      this.updateDiagramSizesHelper(newRounds, newFrames, this.placeholderDiagram);
      this.updateDiagramRoundSize(newRounds, newFrames, this.diagram.diagram);
      this.updatePlaceholderDiagramRoundSize(newRounds, newFrames, this.placeholderDiagram, this.diagram.diagram);
    }
  }

  public updateDiagramSizesHelper(newRounds: number, newFrames: number, diagram: TableCell[][]): void {
    // Passe das Diagram an die neue Größe an, mit möglichst wenig Datenverlust.
    if (diagram.length > newRounds) {
      // Zu viele Rounds -> kürzen
      diagram.length = newRounds;
    }
    if (diagram[0].length > newFrames) {
      // Zu viele Frames
      for (const frame of diagram) {
        frame.length = newFrames;
      }
    }
    while (diagram[0].length < newFrames) {
      // Zu wenig Frames
      const j = diagram[0].length;
      for (let i = 0; i < diagram.length; i++) {
        diagram[i].push(new TableCell(-1, -1, i, j));
      }
    }
  }

  /**
   * updates diagrams length to fit callList length
   * @param newRounds
   * @param newFrames
   * @param diagram
   */
  public updateDiagramRoundSize(newRounds: number, newFrames: number, diagram: TableCell[][]): void {
    while (diagram.length < newRounds) {
      // Zu wenig Rounds -> anhängen
      const temp: TableCell[] = [];
      const i = diagram.length;
      for (let j = 0; j < newFrames; j++) {
        temp.push(new TableCell(-1, -1, i, j));
      }
      diagram.push(temp);
    }
  }

  /**
   * updates placeholder diagrams length to fit callList length
   * @param newRounds
   * @param newFrames
   * @param placeholderDiagram
   * @param diagram
   */
  updatePlaceholderDiagramRoundSize(
    newRounds: number,
    newFrames: number,
    placeholderDiagram: TableCell[][],
    diagram: TableCell[][]
  ): void {
    while (placeholderDiagram.length < newRounds) {
      // Zu wenig Rounds -> anhängen
      const temp: TableCell[] = [];
      let tempCell: TableCell;
      const i = placeholderDiagram.length;

      for (let j = 0; j < newFrames; j++) {
        if (i < 1) {
          tempCell = new TableCell(-1, -1, i, j);
        } else if (diagram[i - 1][j].page < 0) {
          tempCell = new TableCell(placeholderDiagram[i - 1][j].page, placeholderDiagram[i - 1][j].flag, i, j);
        } else {
          tempCell = new TableCell(diagram[i - 1][j].page, -1, i, j);
        }
        temp.push(tempCell);
      }
      placeholderDiagram.push(temp);
    }
  }

  /*Update Diagram Fields*/
  /**
   * set specific TableCell to value
   * @param i
   * @param j
   * @param value
   */
  updateDatum(i: number, j: number, value: number): void {
    if (this.diagram.diagram[i][j].wrong === -1) {
      this.switchAllWrongStates(0);
    }
    if (value >= this.minPageCount && value <= this.maxPageCount) {
      this.diagram.diagram[i][j] = new TableCell(value, this.diagram.diagram[i][j].flag, i, j);
      this.updatePlaceholderDatum(i, j, value);
    } else {
      this.diagram.diagram[i][j] = new TableCell(-1, -1, i, j);
      if (i > 0) {
        this.updatePlaceholderDatum(i, j, this.placeholderDiagram[i][j].page);
      } else {
        this.updatePlaceholderDatum(i, j, -1);
      }
    }
  }

  /**
   * convert HTMLInputElement to number and update TableCell
   * @param i
   * @param j
   * @param target
   */
  updateDatumTarget(i: number, j: number, target: EventTarget): void {
    this.updateDatum(i, j, +(target as HTMLInputElement).value);
  }

  /**
   * updates placeholder TableCell
   * @param i
   * @param j
   * @param value
   * @private
   */
  private updatePlaceholderDatum(i: number, j: number, value: number): void {
    i++;
    while (this.diagram.diagram.length > i && this.diagram.diagram[i][j].page < 0) {
      // Rand der Tabelle nicht erreicht und Feld nicht voll
      this.placeholderDiagram[i][j] = new TableCell(value, -1, i, j);
      i++;
    }
  }

  /**
   * updates flag and sets correction state
   * @param i
   * @param j
   * @param flag
   */
  updateFlag(i: number, j: number, flag: number): void {
    const oldCell = this.diagram.diagram[i][j];
    if (this.diagram.diagram[i][j].wrong === -1) {
      this.switchAllWrongStates(0);
    }
    if (flag >= 0 && flag <= 99) {
      this.diagram.diagram[i][j] = new TableCell(oldCell.page, flag, i, j);
    } else {
      this.diagram.diagram[i][j] = new TableCell(oldCell.page, -1, i, j);
    }
  }

  /*Reset Table*/
  clear(): void {
    this.diagram.diagram = [[]];
    this.placeholderDiagram = [[]];
    this.diagram.pageFaults = new Set();
    this.diagram.writeBacks = new Set();
    this.wrongPageFaults = new Set();
    this.updateDiagramSizes(this.roundCount, this.frameCount);
    this.pageFaultsCorrected = false;
  }

  /*Manage visual parts for evaluation*/
  switchAllWrongStates(indicator: number): void {
    for (const row of this.diagram.diagram) {
      for (const cell of row) {
        cell.wrong = indicator;
      }
    }
  }

  trimWrongPageFaults(newRounds: number): void {
    this.wrongPageFaults.forEach((x, y, s) => {
      if (x >= newRounds) {
        s.delete(x);
      }
    });
  }

  /*Update values with conditions*/
  /**
   * sets new callList via csv string
   * @param newValue
   */
  updateCSV(newValue: string): void {
    // Just one call from the functions above to minimize the processing costs
    this.diagram.updateCallList(
      newValue
        .trim()
        .split(',')
        .map((x) => +x)
    );
    this.wrongPageFaults = new Set();
    this.roundCount = PagingService.bound(this.diagram.callList.length, this.minRoundCount, this.maxRoundCount);
    this.pageCount = PagingService.bound(Math.max(...this.diagram.callList), this.minPageCount, this.maxRoundCount);
    this.updateFrameCount(this.frameCount); // Update diagrams with new maxFrameCount (roundCount)
    this.updateDiagramSizes(this.roundCount, this.frameCount);
  }

  /**
   * updates callList via csv HTML EventTarget
   * @param target
   */
  updateCSVTarget(target: EventTarget): void {
    this.updateCSV((target as HTMLInputElement).value);
  }

  /**
   * updates r/w list via csv string
   * @param newValue
   */
  updateRWList(newValue: string): void {
    this.wrongWriteBacks = new Set();
    this.diagram.rwList = newValue
      .trim()
      .split(',')
      .map((x) => x === 'w' || x === 'true');

    while (this.diagram.rwList.length < this.diagram.callList.length) {
      this.diagram.addOneRWItem();
    }
    this.diagram.rwList.length = this.diagram.callList.length;
  }

  /**
   * updates r/w list via csv HTML EventTarget
   * @param target
   */
  updateRWListTarget(target: EventTarget): void {
    this.updateRWList((target as HTMLInputElement).value);
  }

  /**
   * sets new number of pages
   * @param newPages
   */
  updatePageCount(newPages: number): void {
    const temp = this.pageCount;
    this.pageCount = PagingService.bound(newPages, this.minPageCount, this.maxPageCount); // Set new PageCount with limits
    if (this.pageCount !== temp) {
      this.clear();
      this.setRandomCallList(this.roundCount, this.pageCount);
    }
    this.updateFrameCount(this.frameCount); // Update FrameCount to manage values above the limit
  }

  /**
   * sets new number of pages via EventTarget
   * @param target
   */
  updatePageCountTarget(target: EventTarget): void {
    this.updatePageCount(+(target as HTMLInputElement).value);
  }

  /**
   * sets new number of frames
   * @param newValue
   */
  updateFrameCount(newValue: number): void {
    const temp = this.frameCount;
    this.frameCount = PagingService.bound(newValue, this.minFrameCount, this.pageCount);
    if (temp !== this.frameCount) {
      // Nur reagieren, wenn Änderungen geschehen
      this.wrongPageFaults = new Set();
      this.switchAllWrongStates(0);
      this.updateDiagramSizes(this.roundCount, this.frameCount);
    }
  }

  /**
   * set new number of frames via EventTarget
   * @param target
   */
  updateFrameCountTarget(target: EventTarget): void {
    this.updateFrameCount(+(target as HTMLInputElement).value);
  }

  /**
   * sets new number of rounds
   * @param newValue
   */
  updateRoundCount(newValue: number): void {
    const temp = this.roundCount;
    this.roundCount = PagingService.bound(newValue, this.minRoundCount, this.maxRoundCount);
    if (temp !== this.roundCount) {
      // Nur reagieren, wenn Änderungen geschehen
      this.updateDiagramSizes(this.roundCount, this.frameCount);
      this.diagram.trimPageFaults(this.roundCount);
      this.trimWrongPageFaults(this.roundCount);
    }
  }

  /**
   * set new number of rounds via EventTarget
   * @param target
   */
  updateRoundCountTarget(target: EventTarget): void {
    this.updateRoundCount(+(target as HTMLInputElement).value);
  }

  /**
   * updates the chosen strategy
   * @param target
   */
  updateStrategy(target: EventTarget): void {
    const newStrategy = (target as HTMLInputElement).value;
    if (this.algorithms.has(newStrategy) && newStrategy !== this.diagram.strategy) {
      this.diagram.strategy = newStrategy;
      if (!this.flagAlgorithms.includes(newStrategy) || this.doubleFlagAlgorithms.includes(newStrategy)) {
        this.clear();
      }
    }
  }

  /*Set random values*/
  /**
   * sets new random callList
   * @param newRounds
   * @param newPages
   */
  setRandomCallList(newRounds: number = this.roundCount, newPages: number = this.pageCount): void {
    if (newRounds < 1) {
      newRounds = PagingService.random(30) + 10;
    }
    if (newPages < 1) {
      newPages = Math.floor(newRounds / 3);
    }

    let randomCSV: string = '' + (1 + PagingService.random(newPages));

    for (let i = 1; i < newRounds; i++) {
      randomCSV += ',' + (1 + PagingService.random(newPages));
    }

    this.updateCSV(randomCSV);
  }

  /**
   * sets random strategy
   */
  setRandomStrategy(): void {
    const strategies = Array.from(this.algorithms.keys());
    const newStrategy = strategies[PagingService.random(strategies.length)];
    if (newStrategy !== this.diagram.strategy) {
      this.diagram.strategy = newStrategy;
      if (!this.flagAlgorithms.includes(newStrategy) || this.doubleFlagAlgorithms.includes(newStrategy)) {
        this.clear();
      }
    }
  }

  /**
   * sets random number of frames
   */
  setRandomFrameCount(): void {
    this.updateFrameCount(2 + PagingService.random(14)); // Start with 2 for relevant values
  }

  /**
   * sets random r/w list
   */
  setRandomRWList(): void {
    this.diagram.rwList = [];
    while (this.diagram.rwList.length < this.diagram.callList.length) {
      this.diagram.rwList.push(Math.random() < 0.5);
    }
  }

  /*Configuration communication:*/
  /**
   * loads configurations from server
   */
  getConfigurationSamples(): void {
    // Lade die Beispiele für die Config vom Server
    this.makeConfigRequest().subscribe((data: Config[]) => {
      this.handleConfigResponse(data);
    });
  }

  /**
   * requests configuration from server
   */
  makeConfigRequest(): Observable<Config[]> {
    return this.http
      .post<Config[]>(environment.apiBaseUrl + '/paging/config', '')
      .pipe(retry(5), catchError(PagingService.handleError));
  }

  /**
   * handles configuration response from server
   * @param response
   */
  handleConfigResponse(response: Config[]): void {
    this.sampleConfigList = new Map();
    response.forEach((config) => this.sampleConfigList.set(config.name, this.propertiesToSet(config)));
  }

  /**
   * converts configuration into Diagram object
   * @param config
   */
  propertiesToSet(config: Config): Diagram {
    const set: Set<number> = new Set();
    const set2: Set<number> = new Set();

    if (config.diagram.pageFaults !== null) {
      for (const rowIndex of config.diagram.pageFaults) {
        set.add(rowIndex);
      }
    }

    if (config.diagram.writeBacks !== null) {
      for (const rowIndex of config.diagram.writeBacks) {
        set2.add(rowIndex);
      }
    }

    config.diagram.pageFaults = set;
    config.diagram.writeBacks = set2;

    return config.diagram;
  }

  /*Diagram evaluation:*/
  /**
   * submits the current input
   */
  submit(): void {
    // Hier soll das Diagram an den Server zur Überprüfung geschickt werden.
    const json = JSON.stringify(this.diagram, PagingService.replacer);
    console.log(json);
    this.makeEvaluationRequest(json).subscribe((response: [][]) => {
      this.handleEvaluationResponse(response);
    });
  }

  /**
   * sends current input to server
   * @param postDiagram
   */
  makeEvaluationRequest(postDiagram: string): Observable<[][]> {
    return this.http
      .post<[][]>(environment.apiBaseUrl + '/paging', postDiagram, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        retry(5), // retry a failed request up to 5 times
        catchError(PagingService.handleError) // then handle the error
      );
  }

  /**
   * handles submission response
   * @param response
   */
  handleEvaluationResponse(response: [][]): void {
    const mistakesList: TableCell[] = response[0];
    this.wrongPageFaults = new Set(response[1]);
    this.wrongWriteBacks = new Set(response[2]);
    this.pageFaultsCorrected = true;
    this.writeBacksCorrected = true;
    if (mistakesList.length === 0) {
      this.switchAllWrongStates(-1);
    } else {
      while (mistakesList.length > 0) {
        const tempCell = mistakesList.pop();
        this.diagram.diagram[tempCell.i][tempCell.j].wrong = 1;
      }
    }
  }

  /**
   * toggles the second flag for double flag algorithms
   * @param i
   * @param j
   */
  toggleSecFlag(i: number, j: number): void {
    if (this.diagram.diagram[i][j].wrong === 1) {
      this.diagram.diagram[i][j].wrong = 0;
    }
    let flag = this.diagram.diagram[i][j].flag;
    if (flag >= 10) {
      flag -= 10;
    } else if (flag > 0) {
      flag = flag + 10;
    } else {
      flag = 10;
    }
    this.diagram.diagram[i][j].flag = flag;
  }

  /**
   * toggles flag
   * @param i
   * @param j
   */
  toggleFlag(i: number, j: number): void {
    if (this.diagram.diagram[i][j].wrong === 1) {
      this.diagram.diagram[i][j].wrong = 0;
    }
    let flag = this.diagram.diagram[i][j].flag;
    if (flag < 1) {
      flag = 1;
    } else if (flag < 10) {
      flag = 0;
    } else if (flag < 11) {
      flag = 11;
    } else {
      flag = 10;
    }
    this.diagram.diagram[i][j].flag = flag;
  }

  /**
   * returns flag as boolean
   * @param i
   * @param j
   * @param sec
   */
  getBoolFlag(i: number, j: number, sec = false): boolean {
    const flag = this.diagram.diagram[i][j].flag;
    if (!sec) {
      if (flag === 1 || flag === 11) {
        return true;
      }
    } else {
      if (flag >= 10) {
        return true;
      }
    }
    return false;
  }
}
