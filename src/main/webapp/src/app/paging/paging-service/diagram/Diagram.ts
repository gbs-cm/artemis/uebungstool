import { Injectable } from '@angular/core';
import { TableCell } from './TableCell';

@Injectable({
  providedIn: 'root',
})
export class Diagram {
  diagram: TableCell[][] = [[]];
  callList: number[] = [];
  rwList: boolean[] = [];
  pageFaults: Set<number> = new Set();
  writeBacks: Set<number> = new Set();
  strategy = 'fifo'; // Default
  frameCount = 4; // Default

  constructor() {
    // Stays empty
  }

  /**
   * Sets given CallList and optionally RWList
   * @param newCallList
   * @param newRWList
   */
  updateCallList(newCallList: number[], newRWList: boolean[] = null): void {
    this.callList = newCallList;
    this.pageFaults = new Set();
    if (newRWList !== null) {
      this.rwList = newRWList;
    } else {
      this.rwList = [];
      while (this.rwList.length < this.callList.length) {
        this.addOneRWItem();
      }
    }
  }

  /**
   * returns RWList as CSV
   */
  getRWListString(): string {
    return this.rwList
      .map((rw) => {
        return rw ? 'w' : 'r';
      })
      .join();
  }

  /**
   * adds one R/W state to RW List
   */
  addOneRWItem(): void {
    this.rwList.push(Math.random() < 0.5);
  }

  /**
   * reduces the PageFaults Set to new length
   * @param newRounds
   */
  trimPageFaults(newRounds: number): void {
    this.pageFaults.forEach((x, y, s) => {
      if (x >= newRounds) {
        s.delete(x);
      }
    });
  }
}
