import { Injectable } from '@angular/core';
import { Diagram } from './Diagram';

@Injectable({
  providedIn: 'root',
})
export class Config {
  name = 'Default Sample'; // Default
  diagram: Diagram = null;

  constructor() {
    // Stays empty
  }
}
