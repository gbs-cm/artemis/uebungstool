import { Injectable } from '@angular/core';

const DEFAULT = 0;

@Injectable({
  providedIn: 'root',
})
export class TableCell {
  get wrong(): number {
    return this._wrong;
  }

  set wrong(value: number) {
    this._wrong = value;
  }

  page: number;
  flag: number;
  i: number;
  j: number;
  private _wrong: number = DEFAULT;

  constructor(page: number, flag: number, i: number, j: number) {
    this.page = page;
    this.flag = flag;
    this.i = i;
    this.j = j;
  }

  getFlagBool(): boolean {
    return this.flag === 1 || this.flag === 11;
  }

  getSecFlagBool(): boolean {
    return this.flag === 10 || this.flag === 11;
  }
}
