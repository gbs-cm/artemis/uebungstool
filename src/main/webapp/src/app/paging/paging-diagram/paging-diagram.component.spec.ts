import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingDiagramComponent } from './paging-diagram.component';

describe('PagingDiagramComponent', () => {
  let component: PagingDiagramComponent;
  let fixture: ComponentFixture<PagingDiagramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PagingDiagramComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
