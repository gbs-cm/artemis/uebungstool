import { Component, Injectable, OnInit } from '@angular/core';
import { PagingService } from '../paging-service/paging.service';
import { faQuestion, faCheck, faRecycle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-paging-diagram',
  templateUrl: './paging-diagram.component.html',
  styleUrls: ['./paging-diagram.component.scss'],
})
@Injectable({
  providedIn: 'root',
})
export class PagingDiagramComponent implements OnInit {
  readonly faQuestion = faQuestion;
  readonly faCheck = faCheck;
  readonly faRecycle = faRecycle;

  public tableclass = '';
  public showManual = false;

  constructor(public pagingService: PagingService) {}

  ngOnInit(): void {
    // Stays empty
  }

  /**
   * returns value if not a placeholder
   * @param value
   * @param isFlag
   * @return string
   */
  valueCheck(value: number, isFlag = false): string {
    if (value < 0) {
      if (isFlag) {
        return 't';
      }
      return '';
    }
    return '' + value;
  }

  /**
   * returns the class for a TableCell to display correction information
   * @param i
   * @param j
   * @return string
   */
  public getTableDatumClass(i: number, j: number): string {
    switch (this.pagingService.diagram.diagram[i][j].wrong) {
      case 1:
        return 'form-control is-invalid';
      case 0:
        return 'form-control';
      case -1:
        return 'form-control is-valid correct-table-cell';
    }
  }

  /**
   * transposes the diagram table
   */
  transpose(): void {
    if (this.tableclass === '') {
      this.tableclass = 'transpose';
    } else {
      this.tableclass = '';
    }
  }

  /**
   * generates a new CallList
   */
  generateNewCallList(): void {
    this.pagingService.diagram.updateCallList([]);
    this.pagingService.wrongPageFaults = new Set();
    this.pagingService.updateCallList(this.pagingService.roundCount);
  }

  /**
   * returns whether the given page fault is marked
   * @param i
   * @return boolean
   */
  getPageFaultState(i: number): boolean {
    return this.pagingService.diagram.pageFaults.has(i);
  }

  /**
   * returns the number of page faults set up to current index
   * @param i
   * @return number
   */
  getPageFaults(i: number): number {
    if (i === -1) {
      return 0;
    } else if (this.pagingService.diagram.pageFaults.has(i)) {
      return 1 + this.getPageFaults(i - 1);
    } else {
      return this.getPageFaults(i - 1);
    }
  }

  /**
   * adds or removes the page fault
   * @param i
   */
  updatePageFaults(i: number): void {
    this.pagingService.pageFaultsCorrected = false;
    if (this.pagingService.diagram.pageFaults.has(i)) {
      this.pagingService.diagram.pageFaults.delete(i);
    } else {
      this.pagingService.diagram.pageFaults.add(i);
    }
    this.pagingService.wrongPageFaults.delete(i);
  }

  /**
   * return the class of the indexed page fault
   * @param i
   * @return string
   */
  getPageFaultsClass(i: number): string {
    if (this.pagingService.wrongPageFaults.has(i)) {
      return ' form-check-input-wrong';
    }
    if (this.pagingService.pageFaultsCorrected && this.pagingService.wrongPageFaults.size === 0) {
      return ' form-check-input-correct';
    }
    return '';
  }

  /**
   * adds or removes the write back
   * @param i
   */
  updateWriteBack(i: number): void {
    this.pagingService.writeBacksCorrected = false;
    if (this.pagingService.diagram.writeBacks.has(i)) {
      this.pagingService.diagram.writeBacks.delete(i);
    } else {
      this.pagingService.diagram.writeBacks.add(i);
    }
    this.pagingService.wrongWriteBacks.delete(i);
  }

  /**
   * returns the clss of the indexed write back
   * @param i
   * @return string
   */
  getWriteBackClass(i: number): string {
    if (this.pagingService.wrongWriteBacks.has(i)) {
      return ' form-check-input-wrong';
    }
    if (this.pagingService.writeBacksCorrected && this.pagingService.wrongWriteBacks.size === 0) {
      return ' form-check-input-correct';
    }
    return '';
  }

  /**
   * returns whether the indexed write back is set
   * @param i
   * @return boolean
   */
  getWriteBackState(i: number): boolean {
    return this.pagingService.diagram.writeBacks.has(i);
  }

  /**
   * return the class of the double flag check boxes
   * @param i
   * @param j
   * @return string
   */
  getDoubleFlagClass(i: number, j: number): string {
    switch (this.pagingService.diagram.diagram[i][j].wrong) {
      case 1:
        return ' form-check-input-wrong';
      case 0:
        return '';
      case -1:
        return ' form-check-input-correct';
    }
  }

  /**
   * returns number if >0 or else -1
   * @param target
   * @return number
   */
  checkFlagTarget(target: EventTarget): number {
    const flag = +(target as HTMLInputElement).value;
    return flag > 0 ? flag : -1;
  }

  /**
   * toggles guide
   */
  toggleManual(): void {
    this.showManual = !this.showManual;
  }
}
