import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faGlobeAmericas } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'webapp';
  isCollapsed = true;
  german = true;

  faGlobe = faGlobeAmericas;
  faGitlab = faGitlab;

  constructor(public translate: TranslateService) {
    translate.setDefaultLang('de');
  }

  useLanguage(language: string): void {
    this.translate.use(language);
  }

  changeLanguage(): void {
    if (this.german) {
      this.translate.use('en');
    } else {
      this.translate.use('de');
    }
    this.german = !this.german;
  }
}
