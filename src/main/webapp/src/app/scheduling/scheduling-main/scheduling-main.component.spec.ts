import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulingMainComponent } from './scheduling-main.component';

describe('SchedulerMainComponent', () => {
  let component: SchedulingMainComponent;
  let fixture: ComponentFixture<SchedulingMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SchedulingMainComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulingMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
