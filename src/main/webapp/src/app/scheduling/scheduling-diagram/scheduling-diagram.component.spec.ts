import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulingDiagramComponent } from './scheduling-diagram.component';

describe('SchedulerDiagramComponent', () => {
  let component: SchedulingDiagramComponent;
  let fixture: ComponentFixture<SchedulingDiagramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SchedulingDiagramComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulingDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
