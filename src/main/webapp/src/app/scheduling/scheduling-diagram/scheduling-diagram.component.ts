import { Component, OnInit } from '@angular/core';
import { cellColors, cellStatus, DiagramCell } from '../scheduling-logic/diagram/DiagramCell';
import { Diagram, Strategies } from '../scheduling-logic/diagram/Diagram';
import { SchedulingService } from '../scheduling-logic/scheduling.service';
import { UserLevelThread } from '../scheduling-logic/diagram/UserLevelThread';
import { faShareAlt, faQuestion, faCheck, faRecycle, faCopy } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-scheduler-diagram',
  templateUrl: './scheduling-diagram.component.html',
  styleUrls: ['./scheduling-diagram.component.scss'],
})
export class SchedulingDiagramComponent implements OnInit {
  faShare = faShareAlt; // Icons which are used in this component
  faQuestion = faQuestion;
  faCheck = faCheck;
  faRecycle = faRecycle;
  faCopy = faCopy;

  selectedCells: Array<DiagramCell> = []; // variables for the drag-select functionality
  selectOnDrag = false;
  selectMode = false;
  disable = false;
  disableRangeSelection = false;
  selectWithShortcut = false;

  userLevelThreads: UserLevelThread[] = [];
  serverAnswer: string;
  corrected: boolean;
  diagramJSON = '';
  diagramJSONKernelthreads = '';
  correctionInput = false;
  Strategies = Strategies;
  showHelp = false;
  correctionVisible = false;
  debugActivated = false; // Set to true when debugging in orer to show the servers result
  configurationString = '';
  showCopied = false;

  constructor(public diagram: Diagram, public schedulerService: SchedulingService) {
    this.serverAnswer = 'Nothing to report';
    this.diagramJSON = '';
  }

  ngOnInit(): void {
    this.diagram.getAllUserLevelThreads().subscribe((next) => (this.userLevelThreads = next));
  }

  /**
   * handels the event when one cell specifically was clicked.
   * If this cell is part of the last 5 cells, the diagram is extendend.
   * @param cell
   * @param userLevelThread
   */
  cellClickedIndividual(cell: DiagramCell, userLevelThread: UserLevelThread): void {
    if (userLevelThread.cells.indexOf(cell) >= userLevelThread.cells.length - 5) {
      this.diagram.pushadMoreCollumns(5);
    }
    this.cellClicked(cell);
  }

  /**
   * handles the drag click event and forwards it to the individual cells
   * @param $event
   */
  dragSelected($event: DiagramCell[]): void {
    if ($event.length > 1) {
      for (const diagramCell of $event) {
        this.cellClicked(diagramCell);
      }
    }
  }

  cellClicked(cell: DiagramCell): void {
    if (cell.status === cellStatus.COMPUTING) {
      cell.status = cellStatus.WAITING;
    } else if (cell.status === cellStatus.WAITING) {
      cell.status = cellStatus.CLEAR;
    } else {
      cell.status = cellStatus.COMPUTING;
    }
  }

  checkExcercise(): void {
    this.corrected = true;
    this.schedulerService.evaluateDiagram();
  }

  printJSON(): void {
    this.diagramJSON = this.schedulerService.prepareJSONString();
  }

  printCustomTransport(): void {
    this.diagramJSON = this.diagram.customTransportFormat();
  }

  /**
   * returns the style properties for the specific cell.
   * A cell can either be without special values, have a blue borer
   * to signal the start time or a yellow border to signal
   * the deadline.
   * @param cell The style will be computed for this cell.
   */
  cellStyle(cell: DiagramCell): {
    background: string;
    'border-left-color': string;
    'border-left-style': string;
    'border-left-width': string;
  } {
    if (this.diagram.showHelpGuides) {
      if (cell.start) {
        return {
          background: cell.color,
          'border-left-color': 'var(--blue-correction)',
          'border-left-style': 'solid',
          'border-left-width': '4px',
        };
      }
      if (cell.end) {
        return {
          background: cell.color,
          'border-left-color': 'var(--orange-correction)',
          'border-left-style': 'solid',
          'border-left-width': '4px',
        };
      }
    }
    if (cell.color === cellColors.TRANSPARENT) {
      return;
    }
    return {
      background: cell.color,
      'border-left-color': 'grey',
      'border-left-style': 'solid',
      'border-left-width': '1px',
    };
  }

  toggleHelp(): void {
    this.showHelp = !this.showHelp;
  }

  toggleCorrectionVisible(): void {
    this.correctionVisible = !this.correctionVisible;
  }

  printJSONUserlevelThreads(): void {
    const s = this.schedulerService.prepareJSONStringUserLevelThreads();
    this.diagramJSON = s.substring(s.indexOf(':') + 1, s.length - 1);

    if (this.diagram.strategy === Strategies.RR) {
      const k = this.schedulerService.prepareJSONStringKernelLevelThreads();
      this.diagramJSONKernelthreads = k.substring(k.indexOf(':') + 1, k.length - 1);
    }
  }

  resetDiagram(): void {
    this.diagram.resetDiagram();
  }

  toggleDebugActivated(): void {
    this.debugActivated = !this.debugActivated;
    this.diagramJSON = '';
    this.diagramJSONKernelthreads = '';
  }

  printConfig(): void {
    this.configurationString = this.schedulerService.printConfig();
  }

  exportDiagram(): void {
    this.configurationString = this.schedulerService.exportDiagram();
  }

  toggleConfigurationActivated(): void {
    this.configurationString = '';
  }

  toggleCorrectionTextActivated(): void {
    this.diagram.correctionText = '';
  }

  /**
   * Displays a small popup next to the copy button to give a visual feedback to the user
   */
  showCopyPopup(): void {
    this.showCopied = true;
    setTimeout(() => {
      this.showCopied = false;
    }, 2000);
  }
}
