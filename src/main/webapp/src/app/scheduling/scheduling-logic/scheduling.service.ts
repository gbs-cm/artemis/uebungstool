import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Diagram, Strategies } from './diagram/Diagram';
import { TranslateService } from '@ngx-translate/core';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SchedulingService {
  apiServerUrl = environment.apiBaseUrl;
  correction: string;

  constructor(private http: HttpClient, private diagram: Diagram, private translate: TranslateService) {}

  /**
   * Parses the current configuration in an JSON String and sends it to the server.
   * Once the server sends the answer, the diagram will be adapted to the new solution
   */
  public evaluateDiagram(): void {
    this.http.post<Diagram>(`${this.apiServerUrl}/scheduling`, this.prepareJSONString()).subscribe(
      (response: Diagram) => {
        for (let i = 0; i < this.diagram.allUserLevelThreads.length; i++) {
          const u = this.diagram.allUserLevelThreads[i];
          for (let j = 0; j < u.cells.length; j++) {
            u.cells[j].corrected = response.allUserLevelThreads[i].cells[j].corrected;
            if (response.allUserLevelThreads[i].cells[j].correctionIndex !== 'null') {
              u.cells[j].correctionIndex = response.allUserLevelThreads[i].cells[j].correctionIndex;
            }
            u.cells[j].color = response.allUserLevelThreads[i].cells[j].color;
          }
        }
        this.diagram.atLeastOnceCorrected = true;
        if (this.diagram.strategy === Strategies.Priority || this.diagram.strategy === Strategies.Priority_dynamic) {
          this.diagram.correctionText = this.translate.instant('scheduling.diagram.helperdialogPriorities');
        }

        if (this.diagram.strategy === Strategies.Priority_dynamic) {
          this.diagram.correctionText =
            this.diagram.correctionText + this.translate.instant('scheduling.diagram.helperdialogRounded');
        } else {
          this.diagram.correctionText = '';
        }
        console.log(response.allUserLevelThreads);
      },
      (error: HttpErrorResponse) => {
        this.handleError(error);
      }
    );
  }

  /**
   * Configures the JSON string which includes the configuration which will be sent to the server.
   * Only the attributes which are needed to compute the solution for this configuration are included
   * in order to keep the message size smaller.
   */
  public prepareJSONString(): string {
    const replacer = [
      'strategy',
      'schedulerTime',
      'dispatcherTime',
      'ownUserLevelScheduler',
      'userSchedulerTime',
      'preemptive',
      'initialScheduler',
      'allUserLevelThreads',
      'coreNumber',
      'id',
      'parentid',
      'ioAftern',
      'ioLength',
      'deadline',
      'startTime',
      'computingTime',
      'userlevelTimeSliceLength',
      'status',
      'timeSliceLength',
      'cells',
      'rrOrder',
    ];
    if (this.diagram.ownUserLevelScheduler) {
      replacer.push(
        'allKernelLevelThreads',
        'kernelthreadID',
        'userStrategy',
        'startTime',
        'userSchedulerTime',
        'prioChangeComputingKernel',
        'prioChangeWaitingKernel',
        'lowestPriorityFirst',
        'priority',
        'userSchedulingPreemptive',
        'userlevelSchedulerArrivalTimeRR'
      );
    } else if (this.diagram.strategy === Strategies.Priority || this.diagram.strategy === Strategies.Priority_dynamic) {
      replacer.push('prioChangeComputing', 'prioChangeWaiting', 'lowestPriorityFirst', 'priority');
    }
    if (this.diagram.strategy in [Strategies.EDF, Strategies.Priority, Strategies.Priority_dynamic]) {
      replacer.push('userSchedulingPreemptive');
    }
    return JSON.stringify(this.diagram, replacer);
  }

  /**
   * Prints the JSON string of the configuration with the cells with which tests can be generated in the server.
   */
  prepareJSONStringUserLevelThreads(): string {
    const replacer = [
      'allUserLevelThreads',
      'parentid',
      'ioAftern',
      'ioLength',
      'deadline',
      'cells',
      'startTime',
      'computingTime',
      'id',
      'status',
      'priority',
      'rrOrder',
    ];
    return JSON.stringify(this.diagram, replacer);
  }

  /**
   * Prints the separate configuration string for the kernellevelthreads which is necessary to generate
   * tests for the RR strategy
   */
  prepareJSONStringKernelLevelThreads(): string {
    const replacer = [
      'allKernelLevelThreads',
      'kernelthreadID',
      'userStrategy',
      'startTime',
      'userlevelTimeSliceLength',
      'lowestPriorityFirstKernel',
      'prioChangeComputingKernel',
      'prioChangeWaitingKernel',
      'userSchedulingPreemptive',
      'userlevelSchedulerArrivalTimeRR',
    ];
    return JSON.stringify(this.diagram, replacer);
  }

  /**
   *  Error handling routine for all requests to the server.
   *  The frame was adapted from: https://angular.io/guide/http
   */
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      alert(this.translate.instant('scheduling.diagram.serverunreachable'));
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      alert(this.translate.instant('scheduling.diagram.serverError') + error.status + ')');
      console.error(`Backend returned code ` + error.status + `body was: ` + error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(this.translate.instant('scheduling.diagram.unknownError'));
  }

  /**
   * returns a JSON String of the complete diagram without the cells. Every other relevant attribute is included.
   */
  printConfig(): string {
    const replacer = [
      'strategy',
      'schedulerTime',
      'dispatcherTime',
      'userSchedulerTime',
      'ownUserLevelScheduler',
      'preemptive',
      'initialScheduler',
      'allUserLevelThreads',
      'coreNumber',
      'timeSliceLength',
      'prioChangeComputing',
      'prioChangeWaiting',
      'lowestPriorityFirst',
      'allKernelLevelThreads',
      'userSchedulerTime',
      'nextID',
      'nextID',
      'highestStartingTime',
      'advancedConfigurationOptions',
      'showHelpGuides',
      'parentid',
      'ioAftern',
      'ioLength',
      'deadline',
      'startTime',
      'computingTime',
      'id',
      'name',
      'startTimePrevious',
      'deadlinePrevious',
      'IOInterruptionActivated',
      'priority',
      'deadlineBackup',
      'userID',
      'kernelThreadName',
      'kernelthreadID',
      'userStrategy',
      'startTime',
      'lowestUserThreadID',
      'userlevelTimeSliceLength',
      'lowestPriorityFirstKernel',
      'prioChangeComputingKernel',
      'prioChangeWaitingKernel',
      'userSchedulingPreemptive',
      'userlevelSchedulerArrivalTimeRR',
      'rrOrder',
    ];
    return JSON.stringify(this.diagram, replacer);
  }

  exportDiagram(): string {
    const size = Math.min(this.diagram.allUserLevelThreads[0].cells.length, 29);
    let process = '';

    for (let i = 0; i < this.diagram.allUserLevelThreads.length; i++) {
      process =
        process +
        '\n' +
        '    \\ganttbar[inline=false]{$' +
        this.diagram.allUserLevelThreads[i].name +
        '$}{0}{0}\n' +
        '\t\\fillgantt{' +
        this.diagram.allUserLevelThreads[i].convertAllCellsGanttExportFormat() +
        '}\n';
      if (i < this.diagram.allUserLevelThreads.length - 1) {
        process = process + '\t\\\\';
      }
    }

    const prefix =
      '\\documentclass{standalone}\n' +
      '\n' +
      '\\usepackage{pgfgantt}\n' +
      '\\usepackage{amsmath}\n' +
      '\\usepackage{xstring}\n' +
      '\n' +
      '% Use this command to fill a row in a gantt diagram with dashes and crosses.\n' +
      '% Examples:\n' +
      '%     \\fillgantt{....----xxx---xx}\n' +
      '%     \\fillgantt{...x...x...x.x}\n' +
      '% Syntax:\n' +
      "%     '.' (dot) will leave the field blank\n" +
      "%     'x' (lowercase x) will generate a bold \\times symbol in the field\n" +
      "%     '-' (minus) will generate a bold minus symbol in the field\n" +
      '% Any other symbols will be printed as themselves, but in bold.\n' +
      '\\newcommand{\\fillgantt}[1] {\n' +
      '    \\foreach \\n [evaluate=\\n as \\npluseins using int(\\n+1)] in {0,...,' +
      size +
      '} {\n' +
      '        \\ganttbar {$\\color{blue}{\\boldsymbol{\\StrChar{#1}{\\npluseins}[\\char]\\IfStrEq{\\char}{x}{\\times}{\\IfStrEq{\\char}{.}{}{\\char}}}}$}{\\n}{\\n}\n' +
      '    }\n' +
      '}\n' +
      '\n' +
      '\n' +
      '\\begin{document}\n' +
      '\n' +
      '\\setganttlinklabel{f-s}{}\n' +
      '\n' +
      '\\begin{ganttchart}[\n' +
      '        hgrid=true,\n' +
      '        vgrid=true,\n' +
      '        inline,\n' +
      '\t\ttitle height = 1,\n' +
      '\t\ty unit title = 0.7cm,\n' +
      '\t\ty unit chart = 1.0cm,\n' +
      '\t\tvgrid = {*4{dotted}, *1{solid}},\n' +
      '\t\tbar/.append style={draw opacity=0, fill opacity=0},\n' +
      '        %vrule/.style={thin, black},\n' +
      '    ]{0}{' +
      size +
      '}\n' +
      '    \\gantttitle{Scheduling}{' +
      (size + 1) +
      '} \\\\\n' +
      '    \\gantttitlelist{0,...,' +
      size +
      '}{1} \\\\\n' +
      '\n';

    const suffix = '\\end{ganttchart}\n' + '\n' + '\\end{document}\n';
    return prefix + '\n' + process + '\n' + suffix;
  }
}
