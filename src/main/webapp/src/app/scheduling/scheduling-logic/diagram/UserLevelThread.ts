import { cellColors, cellStatus, DiagramCell } from './DiagramCell';

export class UserLevelThread {
  /**
   * Userlevel attributes. All attributes which are relevant for correcting the diagram must be added in
   * scheduling-service.ts -> prepareJSONString() to ensure that they are transmitted to the server or are used in an configuration string.
   * Additionally, new attributes should be added in this -> applyConfiguration() to move them between two instances.
   */
  id: number;
  computingTime: number;
  startTime: number;
  cells: DiagramCell[] = [];
  deadline = -1;
  ioLength = -1;
  ioAftern = -1;
  IOInterruptionActivated = false;
  parentid: number;
  priority: number;

  /**
   * Local attributes which are only relevant to style and maintain the diagram in the browser.
   * These values do not need to be transmitted to a server
   */
  private deadlineBackup = -1;
  deadlinePrevious = 0;
  startTimePrevious = 0;
  name: string;
  minComputingTime = 1;

  constructor(
    id: number,
    name: string,
    computingTime: number,
    startTime: number,
    numberOfCells: number,
    parentid: number
  ) {
    this.id = id;
    this.name = name;
    this.computingTime = computingTime;
    this.deadlineBackup = -1;
    this.priority = Math.floor(Math.random() * 20);
    this.startTime = startTime;
    this.startTimePrevious = startTime;
    this.deadline = -1;
    this.deadlinePrevious = -1;
    this.cells = Array(numberOfCells)
      .fill(null)
      .map(() => new DiagramCell());
    this.parentid = parentid;
    if (this.id !== -1) {
      this.cells[startTime].start = true;
    }
  }

  /**
   * Sets the deadline for this thread.
   * If this thread hasn't have had an deadline until now, a new one
   * is generated randomly.
   *
   * If the thread has had an deadline, this previous deadline will be reloaded from the backup.
   */
  setDeadline(): void {
    if (this.deadlineBackup === -1) {
      this.deadline = this.computingTime + this.startTime + Math.floor(Math.random() * 10);
      this.deadlineBackup = this.deadline;
      this.deadlinePrevious = this.deadline;
    } else {
      this.deadline = this.deadlineBackup;
    }
    this.cells[this.deadline].end = true;
  }

  /**
   * Removes the deadline from a Thread but stores it as a Backup to be able to reload the same value.
   */
  resetDeadline(): void {
    this.deadlineBackup = this.deadline;
    if (this.cells[this.deadline]) {
      this.cells[this.deadline].end = false;
    }
    this.deadline = -1;
  }

  /**
   * Prints the custom transport format.
   * This method is not used at the moment
   */
  customTransportFormat(): string {
    return (
      '' +
      this.id +
      this.computingTime +
      this.startTime +
      this.deadline +
      this.priority +
      this.ioLength +
      this.ioAftern +
      this.IOInterruptionActivated +
      this.parentid +
      this.convertAllCells()
    );
  }

  /**
   * Prints the custom transport format for all cells.
   * This method is not used at the moment
   */
  private convertAllCells(): string {
    let retValue = '';
    this.cells.forEach((c) => {
      retValue = retValue += c.customTransportFormat();
    });
    return retValue;
  }

  /**
   * Prints the custom transport format for all cells.
   * This method is not used at the moment
   */
  convertAllCellsGanttExportFormat(): string {
    let retValue = '';
    this.cells.forEach((c) => {
      retValue = retValue += c.ganttExportFormat();
    });
    return retValue;
  }

  /**
   * Adds a IO Interruption to this thread and initialises the Values ioAftern and IOLength in the given Range
   */
  addIOInterruption(): void {
    if (this.ioAftern === -1) {
      this.ioAftern = Math.floor(Math.random() * (this.computingTime - 1)) + 1;
      this.ioLength = Math.floor(Math.random() * 4) + 1;
    }
    if (this.computingTime === this.ioAftern) {
      this.computingTime = this.computingTime + 1;
    }
    this.IOInterruptionActivated = !this.IOInterruptionActivated;
    if (this.IOInterruptionActivated) {
      this.minComputingTime = 2;
    } else {
      this.minComputingTime = 1;
    }
  }

  /**
   * Adds/Removes the deadline from this UserlevelThread and also alters the button text for this thread
   * to either remove or add the deadline with the next button click.
   */
  toggleDeadline(): void {
    if (this.deadline === -1) {
      this.setDeadline();
    } else {
      this.resetDeadline();
    }
  }

  /**
   * Resets all attributes of all cells of
   * this userlevelthread.
   */
  resetCells(): void {
    this.cells.forEach((c) => {
      c.color = cellColors.TRANSPARENT;
      c.corrected = cellStatus.CLEAR;
      c.status = cellStatus.CLEAR;
      c.correctionIndex = '';
    });
  }

  /**
   * Resets the color and correction attribute of all cells of
   * this userlevelthread.
   */
  resetCellsWithoutUserInput(): void {
    this.cells.forEach((c) => {
      c.color = cellColors.TRANSPARENT;
      c.corrected = cellStatus.CLEAR;
      c.correctionIndex = '';
    });
  }
}
