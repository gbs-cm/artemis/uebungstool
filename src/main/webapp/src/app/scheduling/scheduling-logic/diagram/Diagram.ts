import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { KernelLevelThread } from './KernelLevelThread';
import { UserLevelThread } from './UserLevelThread';
import { cellColors, DiagramCell } from './DiagramCell';

@Injectable({
  providedIn: 'root',
})
export class Diagram {
  /**
   * Diagram attributes. All attributes which are relevant for correcting the diagram must be added in
   * scheduling-service.ts -> prepareJSONString() to ensure that they are transmitted to the server or are used in an configuration string.
   * Additionally, new attributes should be added in this -> applyConfiguration() to move them between two instances.
   */
  strategy: Strategies = Strategies.FCFS;
  strategyHelper = Strategies.FCFS;
  timeSliceLength = 5;
  schedulerTime = true;
  dispatcherTime = false;

  visibleUserThreads = false;
  ownUserLevelScheduler = false;
  preemptive = false;
  prioChangeComputing = -1.0;
  prioChangeWaiting = 2.0;
  lowestPriorityFirst = false;
  allKernelLevelThreads: KernelLevelThread[] = [new KernelLevelThread(-1, 'Scheduler', -1, -1, 20)];
  allUserLevelThreads: UserLevelThread[] = [this.allKernelLevelThreads[0].userLevelThreadsOfKernelThread[0]];
  initialScheduler = false;
  userSchedulerTime = true;
  coreNumber = 1;

  /**
   * Local attributes which are only relevant to style and maintain the diagram in the browser.
   * These values do not need to be transmitted to a server
   */
  nextID = 1;
  highestStartingTime = 0;
  advancedConfigurationOptions = false;
  atLeastOnceCorrected = false;
  correctionText = '';
  showHelpGuides = true;
  rrOrder = true;

  getKernelLevelThreads(): Observable<KernelLevelThread[]> {
    return of(this.allKernelLevelThreads);
  }

  getAllUserLevelThreads(): Observable<UserLevelThread[]> {
    return of(this.allUserLevelThreads);
  }

  /**
   * Addes a new kernellevelthread to the configuration and initialices all
   * necessary values. This includes the highest starting time and the diagram length
   */
  addKernelLevelThread(): void {
    const computingTime = this.getRandom() + 1;
    const startingTime = this.getRandom();
    if (startingTime > this.highestStartingTime) {
      this.highestStartingTime = startingTime;
    }
    const newKernelLevelThread = new KernelLevelThread(
      this.nextID,
      `P${this.nextID}`,
      computingTime,
      startingTime,
      this.allUserLevelThreads[0].cells.length
    );
    if (this.strategy === Strategies.EDF) {
      newKernelLevelThread.userLevelThreadsOfKernelThread[0].setDeadline();
    }
    this.nextID++;
    this.allKernelLevelThreads.splice(this.allKernelLevelThreads.length - 1, 0, newKernelLevelThread); //add the new thead at the correct position in the list
    this.allUserLevelThreads.splice(
      this.allUserLevelThreads.length - 1,
      0,
      newKernelLevelThread.userLevelThreadsOfKernelThread[0]
    );
    this.updateDiagram();
  }

  /**
   * Addes a new userlevelthread to the configuration and initialices all
   * necessary values. This includes the highest starting time and the diagram length
   *
   * @param kernelLevelThread Thread in which the new thread is generated.
   */
  addUserLevelThread(kernelLevelThread: KernelLevelThread): void {
    if (kernelLevelThread.userLevelThreadsOfKernelThread.length === 1) {
      kernelLevelThread.userLevelThreadsOfKernelThread[0].name = `${kernelLevelThread.userLevelThreadsOfKernelThread[0].name}_U0`;
    }

    // get the index of the previous UserLevelThread in this KernellevelThread in the list of all UserlevelThreads.
    const position =
      1 +
      this.allUserLevelThreads.indexOf(
        kernelLevelThread.userLevelThreadsOfKernelThread[kernelLevelThread.userLevelThreadsOfKernelThread.length - 1]
      );
    const newUserLevelThread = kernelLevelThread.addUserlevelThread(kernelLevelThread.kernelThreadName);
    this.allUserLevelThreads.splice(position, 0, newUserLevelThread);
    if (this.strategy === Strategies.EDF) {
      newUserLevelThread.setDeadline();
    }

    this.updateDiagram();
  }

  getRandom(): number {
    return Math.floor(Math.random() * 10);
  }

  /**
   * Deletes the given kernellevelthread with all included userlevlthreads from the configuration
   * and adapts the iagram length.
   *
   * @param kernelLevelThread thread which is deleted.
   */
  deleteKernelLevelThread(kernelLevelThread: KernelLevelThread): void {
    if (kernelLevelThread.userLevelThreadsOfKernelThread.length > 1) {
      if (!window.confirm('Soll der KernellevelThread mit all seinen UserlevelThreads wirklich gelöscht werden?')) {
        return;
      }
    }
    // delete all userlevel Threads from allUserLevelThreads
    this.allUserLevelThreads.splice(
      this.allUserLevelThreads.indexOf(kernelLevelThread.userLevelThreadsOfKernelThread[0]),
      kernelLevelThread.userLevelThreadsOfKernelThread.length
    );

    const index = this.allKernelLevelThreads.indexOf(kernelLevelThread, 0);
    if (index > -1) {
      this.allKernelLevelThreads.splice(index, 1);
    }
    this.updateDiagram();
  }

  /**
   * Deletes a userlevelthread from a kernellevelthread.
   * The thread is deleted and the new diagram length is calculated.
   * @param userLevelThread thread which will be deleted
   * @param kernelLevelThread Parent thread of the deleted userlevelthread.
   */
  deleteUserLevelThread(userLevelThread: UserLevelThread, kernelLevelThread: KernelLevelThread): void {
    kernelLevelThread.userLevelThreadsOfKernelThread.splice(
      kernelLevelThread.userLevelThreadsOfKernelThread.indexOf(userLevelThread),
      1
    );
    if (
      kernelLevelThread.userLevelThreadsOfKernelThread.length === 1 &&
      kernelLevelThread.userLevelThreadsOfKernelThread[0].name.startsWith(kernelLevelThread.kernelThreadName)
    ) {
      kernelLevelThread.userLevelThreadsOfKernelThread[0].name =
        kernelLevelThread.userLevelThreadsOfKernelThread[0].name.substr(0, 2);
      kernelLevelThread.userID = 1;
    }
    const index = this.allUserLevelThreads.indexOf(userLevelThread, 0);
    if (index > -1) {
      this.allUserLevelThreads.splice(index, 1);
    }
    if (kernelLevelThread.lowestUserThreadID === userLevelThread.id) {
      this.recomputeLowestStartingTime(kernelLevelThread);
    }
    this.updateDiagram();
  }

  /**
   * Updates the size of the diagram dynamically based on the combined computing time
   * fall threads. The diagram size is decreased or increased depending on a factor.
   * This factor differs for different strategies, since some strategies may need a longer diagrams than others.
   *
   * The size of the diagam is limited to a length of 200 cells with this method. If users need
   * to use more cells, these cells are generated once one of the cells in the last five collums was
   * clicked. This reduces the amount of ressources which are needed to fill in the beginning of
   * the diagrm.
   */
  public updateDiagram(): void {
    const combinedComputingTime = this.computeCombinedComputingTime();

    const diagramSize = this.allKernelLevelThreads[0].userLevelThreadsOfKernelThread[0].cells.length;
    let factorUpperLimit = 1.5;
    let factorLowerLimit = 3.0;
    if (this.strategy === Strategies.Priority_dynamic) {
      factorUpperLimit = 3.0;
      factorLowerLimit = 1.5;
    }

    if (diagramSize < combinedComputingTime * factorUpperLimit && diagramSize < 200) {
      const sizeDifference = Math.floor(diagramSize * factorUpperLimit) - diagramSize;
      this.pushadMoreCollumns(sizeDifference);
    } else if (diagramSize * factorLowerLimit > combinedComputingTime && diagramSize * factorLowerLimit > 20) {
      const sizeDifference = diagramSize - Math.floor(diagramSize * factorLowerLimit);
      this.allUserLevelThreads.forEach((e) => e.cells.splice(e.cells.length - sizeDifference, sizeDifference));
    }
  }

  /**
   * Computes the combined computing time for all threads without any scheduling
   * nor dispatching. Highest starting time is added to take a possible offset in account
   */
  computeCombinedComputingTime(): number {
    let combinedComputingTime = this.highestStartingTime;
    this.allUserLevelThreads.forEach((e) => (combinedComputingTime += e.computingTime));
    return combinedComputingTime;
  }

  /**
   * Alters the starting time of a userlevelthread. This must update the orange
   * help line and possibly the starting time of the kernel-level thread
   * @param $event new starting tiem
   * @param userLevelThread userlevelthread whose starting time is changed
   * @param kernelLevelThread parent thread of the userlevelthread
   */
  alterStartingTime($event: number, userLevelThread: UserLevelThread, kernelLevelThread: KernelLevelThread): void {
    if ($event >= 0 && $event < userLevelThread.cells.length) {
      userLevelThread.cells[$event].start = true;

      userLevelThread.cells[userLevelThread.startTimePrevious].start = false;
      if ($event > this.highestStartingTime) {
        this.highestStartingTime = $event;
        this.updateDiagram();
      }

      if ($event < kernelLevelThread.startTime) {
        kernelLevelThread.startTime = $event;
        kernelLevelThread.lowestUserThreadID = userLevelThread.id;
      } else if (
        $event === kernelLevelThread.startTime + 1 &&
        kernelLevelThread.lowestUserThreadID === userLevelThread.id
      ) {
        this.recomputeLowestStartingTime(kernelLevelThread);
      }
    }
  }

  /**
   * Determines the lowest starting time of all userlevelthreads in a kernellevelthread
   * @param kernelLevelThread
   */
  recomputeLowestStartingTime(kernelLevelThread: KernelLevelThread): void {
    kernelLevelThread.lowestUserThreadID = kernelLevelThread.userLevelThreadsOfKernelThread[0].id;
    kernelLevelThread.startTime = kernelLevelThread.userLevelThreadsOfKernelThread[0].startTime;
    if (kernelLevelThread.userLevelThreadsOfKernelThread.length > 1) {
      for (const u of kernelLevelThread.userLevelThreadsOfKernelThread) {
        if (u.startTime < kernelLevelThread.startTime) {
          kernelLevelThread.lowestUserThreadID = u.id;
          kernelLevelThread.startTime = u.startTime;
        }
      }
    }
  }

  /**
   * Alters the deadline for a Userlevelthread and moves the helpline
   * in the diagramto the new position
   * @param $event
   * @param userLevelThread
   */
  alterDeadline($event: number, userLevelThread: UserLevelThread): void {
    if ($event >= userLevelThread.startTime && $event < userLevelThread.cells.length) {
      userLevelThread.cells[$event].end = true;
      userLevelThread.cells[userLevelThread.deadlinePrevious].end = false;
    }
  }

  /**
   * Activates Deadlines for all userlevelthreads.
   * The deadline must not be activated for the scheduler.
   */
  setDeadlines(): void {
    this.allUserLevelThreads
      .filter((u) => u.id !== -1)
      .forEach((u) => {
        u.setDeadline();
      });
  }

  /**
   * Resets the values from all userlevelthreads to their original value.
   */
  setDeadlinesToDefault(): void {
    if (this.allUserLevelThreads[0].deadline !== -1) {
      this.allUserLevelThreads
        .filter((u) => u.id !== -1)
        .forEach((u) => {
          u.resetDeadline();
        });
    }
  }

  /**
   * Computes the custom transport format for the diagram configuration.
   * This is not used at the moment but can possivbly be used in teh future to reduce the
   * the size of the REST API request massively. This is not used at
   * the moment since the parsing is not as performant as teh JSON parsing
   * and JSON is much more robust and readable.
   */
  customTransportFormat(): string {
    return (
      this.strategy +
      '' +
      this.dispatcherTime +
      this.schedulerTime +
      this.timeSliceLength +
      this.preemptive +
      this.convertAllUserlevelThreads()
    );
  }

  /**
   * Produces the configuration string for all userlevelthreads in the
   * shorter unused custom format.
   * @private
   */
  private convertAllUserlevelThreads(): string {
    let retValue = '';
    this.allUserLevelThreads.forEach((u) => {
      retValue = retValue += `${u.customTransportFormat()} `;
    });
    return retValue;
  }

  resetDiagram(): void {
    this.allUserLevelThreads.forEach((u) => {
      u.resetCells();
    });
  }

  /**
   * addes new cells to the each exisitng thread.
   * @param amount number of additional cells.
   */
  pushadMoreCollumns(amount: number): void {
    if (this.allUserLevelThreads[0].cells.length + amount > 200) {
      amount = 200 - this.allUserLevelThreads[0].cells.length;
    }

    this.allUserLevelThreads.forEach((e) =>
      e.cells.push(
        ...Array(amount)
          .fill(null)
          .map(() => new DiagramCell())
      )
    );
  }

  /**
   * Increases the IOAfterN variable. If the new
   * value is larger than the computing time, the computing time is increased
   * @param $event
   * @param userLevelThread
   */
  alterIOAfterN($event: number, userLevelThread: UserLevelThread): void {
    if ($event >= userLevelThread.computingTime) {
      userLevelThread.computingTime = $event + 1;
    }
  }

  /**
   * resets the correction color of all cells if
   * they were still set to a different value than the default configuration.
   */
  updateCorrectionStatus(): void {
    if (this.allUserLevelThreads.length > 0 && this.allUserLevelThreads[0].cells[0].color !== cellColors.TRANSPARENT) {
      this.allUserLevelThreads.forEach((u) => {
        u.resetCellsWithoutUserInput();
      });
    }
  }

  /**
   * Uses the configuration String to update theee diagram with the given configuration.
   * All null values in the diagram will be ignored and used from the previous configuration.
   *
   * @param configurationString JSON String of the diagram.
   */
  applyConfiguration(configurationString: string): boolean {
    configurationString = configurationString.replace(/{\\"status\\":\\"\\"},/g, '');
    configurationString = configurationString.replace(/{\\"status\\":\\"X\\"},/g, '');
    configurationString = configurationString.replace(/{\\"status\\":\\"-\\"},/g, '');
    configurationString = configurationString.replace(/,"cells":/g, '');
    configurationString = configurationString.replace(/\\"/g, '"');

    try {
      const newDiagram = JSON.parse(configurationString);
      this.nextID = newDiagram.nextID || this.nextID;
      this.timeSliceLength = newDiagram.timeSliceLength || this.timeSliceLength;
      this.highestStartingTime = newDiagram.highestStartingTime;
      this.schedulerTime = newDiagram.schedulerTime;
      this.dispatcherTime = newDiagram.dispatcherTime;
      this.advancedConfigurationOptions = newDiagram.advancedConfigurationOptions;
      this.preemptive = newDiagram.preemptive;
      this.prioChangeComputing = newDiagram.prioChangeComputing;
      this.prioChangeWaiting = newDiagram.prioChangeWaiting;
      this.lowestPriorityFirst = newDiagram.lowestPriorityFirst;
      this.initialScheduler = newDiagram.initialScheduler;
      this.userSchedulerTime = newDiagram.userSchedulerTime;
      this.coreNumber = newDiagram.coreNumber;
      this.rrOrder = newDiagram.rrOrder;

      this.strategy = newDiagram.strategy || this.strategy;
      if (typeof this.strategy === 'string') {
        this.strategy = this.strategyToEnum(this.strategy);
      }
      if (this.strategy === Strategies.Priority_dynamic) {
        this.strategyHelper = Strategies.Priority;
      } else {
        this.strategyHelper = this.strategy;
      }

      this.initNewThreads(newDiagram);
      return false;
    } catch (e) {
      console.log(e);
      return true;
    }
  }

  /**
   * Inits new the field allUserlevelThreads and allKernellevelthreads from one json array to this diagram
   * @param newDiagram whose values should be copied
   */
  initNewThreads(newDiagram: Diagram): void {
    const allKernelLevelThreadsHelper: KernelLevelThread[] = [];
    if (newDiagram.allKernelLevelThreads !== null) {
      // Create all kernellevelthreads and add them to the list of kernellevelthreads
      for (const k of newDiagram.allKernelLevelThreads) {
        allKernelLevelThreadsHelper.push(this.createKernelLevelThreadForConfig(k));
      }

      const numberofPreviousElements = this.allUserLevelThreads.length;
      newDiagram.allUserLevelThreads.forEach((u) => {
        this.allUserLevelThreads.push(this.createUserLevelThreadForConfig(u));
      });

      // The following code will add the newly created objects to the existing arrays.
      // It is important that the arrays are not completle empty, otherwise the configuration
      // will fail.
      this.allUserLevelThreads.splice(0, numberofPreviousElements);

      this.allKernelLevelThreads.push(allKernelLevelThreadsHelper.pop()); //copy first element in order to have at least one element remaining in the array
      this.allKernelLevelThreads.splice(0, this.allKernelLevelThreads.length - 1);
      allKernelLevelThreadsHelper.forEach((k) => this.allKernelLevelThreads.push(k));

      // Add all userlevelthreads to the corresponding kernellevelthreads
      this.allUserLevelThreads.forEach((u) =>
        this.allKernelLevelThreads.find((k) => k.kernelthreadID === u.parentid).userLevelThreadsOfKernelThread.push(u)
      );
      // Delete the dummy from the userlevelthread list in any kernellevelthread.
      this.allKernelLevelThreads.forEach((k) => k.userLevelThreadsOfKernelThread.splice(0, 1));

      if (this.allKernelLevelThreads[0].kernelthreadID === -1) {
        this.allKernelLevelThreads.push(this.allKernelLevelThreads.shift());
      }

      this.allKernelLevelThreads.forEach((k) =>
        typeof k.userStrategy === 'string' ? (k.userStrategy = this.strategyToEnum(k.userStrategy)) : {}
      ); //console.log("User strategy type: "+typeof k.userStrategy));

      this.expandDiagramConfig();
      this.allUserLevelThreads.forEach((u) => this.updateStartAndDeadlineUserlevelthread(u));
    }
  }

  /**
   * Creates a new Thread from the given kernelleveltrhread.
   * This allows to check the input more easily and use the same objects in the diagram.
   * @param k kernellevelthread from a diagram configuration.
   */
  createKernelLevelThreadForConfig(k: KernelLevelThread): KernelLevelThread {
    const helperthread: KernelLevelThread = new KernelLevelThread();
    helperthread.userID = k.userID;
    helperthread.userLevelThreadsOfKernelThread = [];
    helperthread.userLevelThreadsOfKernelThread.push(new UserLevelThread(-2, '', 1, 0, 1, -2));
    helperthread.kernelthreadID = k.kernelthreadID;
    helperthread.userlevelSchedulerArrivalTimeRR = k.userlevelSchedulerArrivalTimeRR;

    if (k.userStrategy === Strategies.Priority_dynamic) {
      helperthread.userStrategyHelper = Strategies.Priority;
      helperthread.userStrategyDynamic = true;
    } else {
      helperthread.userStrategyHelper = k.userStrategy;
    }
    helperthread.userStrategy = k.userStrategy;

    helperthread.lowestPriorityFirstKernel = k.lowestPriorityFirstKernel;
    helperthread.kernelThreadName = k.kernelThreadName;
    helperthread.lowestUserThreadID = k.lowestUserThreadID;
    helperthread.prioChangeComputingKernel = k.prioChangeComputingKernel;
    helperthread.prioChangeWaitingKernel = k.prioChangeWaitingKernel;
    helperthread.userlevelTimeSliceLength = k.userlevelTimeSliceLength;
    helperthread.userSchedulingPreemptive = k.userSchedulingPreemptive;
    helperthread.startTime = k.startTime;
    return helperthread;
  }

  /**
   * Creates a new Thread from the given userleveltrhread.
   * This allows to check the input more easily and use the same objects in the diagram.
   * @param u userlevelthread from a diagram configuration.
   */
  createUserLevelThreadForConfig(u: UserLevelThread): UserLevelThread {
    const helperThread: UserLevelThread = new UserLevelThread(
      u.id,
      u.name,
      u.computingTime,
      u.startTime,
      20,
      u.parentid
    );

    helperThread.deadline = u.deadline;
    helperThread.id = u.id;
    helperThread.parentid = u.parentid;
    if (helperThread.deadline > -1) {
      helperThread.deadlinePrevious = helperThread.deadline;
    }
    helperThread.ioAftern = u.ioAftern;
    helperThread.ioLength = u.ioLength;
    if (u.ioLength > -1) {
      helperThread.IOInterruptionActivated = true;
      helperThread.minComputingTime = 2;
    }
    helperThread.priority = u.priority;
    helperThread.startTimePrevious = u.startTime;

    return helperThread;
  }

  updateStartAndDeadlineUserlevelthread(u: UserLevelThread): void {
    if (u.startTime > -1) {
      //set Cell for start helpline
      u.cells[u.startTime].start = true;
    }
    if (u.deadline > 0) {
      //set Cell for deadline helpline
      console.log('Deadline: ' + u.deadline + '   Cellsize: ' + u.cells.length);
      u.cells[u.deadline].end = true;
    }
  }

  /**
   * Adds the correct amount of cells to the diagram which was configured with an external configuration.
   * @private
   */
  private expandDiagramConfig() {
    const numberofCells = this.computeCombinedComputingTime();

    let factorUpperLimit = 1.5;
    if (this.strategy === Strategies.Priority_dynamic) {
      factorUpperLimit = 3.0;
    }

    const sizeDifference = Math.min(Math.floor(numberofCells * factorUpperLimit), 180);
    console.log('Size Difference: ' + sizeDifference);
    this.pushadMoreCollumns(sizeDifference);
  }

  strategyToEnum(strategy: string): number {
    switch (strategy) {
      case 'SJF':
        return 1;
      case 'SRTN':
        return 2;
      case 'RR':
        return 3;
      case 'EDF':
        return 4;
      case 'Priority':
        return 5;
      case 'Priority_dynamic':
        return 6;
      default:
        return 0;
    }
  }

  /**
   * Updates the name for the scheduler /Dispatcher row depending on whether the dispatcher is activated or not.
   * @param b
   */
  updateDispatcherState(b: boolean): void {
    if (b) {
      this.allKernelLevelThreads[this.allKernelLevelThreads.length - 1].userLevelThreadsOfKernelThread[0].name =
        'Sched./Disp.';
    } else {
      this.allKernelLevelThreads[this.allKernelLevelThreads.length - 1].userLevelThreadsOfKernelThread[0].name =
        'Scheduler';
    }
    this.updateCorrectionStatus();
  }
}

export enum Strategies {
  FCFS = 0,
  SJF = 1,
  SRTN = 2,
  RR = 3,
  EDF = 4,
  Priority = 5, // 6 is reserved for dynamic scheduling
  Priority_dynamic = 6,
}
