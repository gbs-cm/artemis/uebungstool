export class DiagramCell {
  status: cellStatus = cellStatus.CLEAR;
  corrected: cellStatus = cellStatus.CLEAR;
  color: cellColors = cellColors.TRANSPARENT;
  start = false;
  end = false;
  correctionIndex = ''; // can be used for any kind of correction annotation

  customTransportFormat(): string {
    if (this.status === cellStatus.CLEAR) {
      return 'E';
    }
    return this.status;
  }
  ganttExportFormat(): string {
    if (this.status === cellStatus.CLEAR) {
      return '.';
    }
    if (this.status === cellStatus.WAITING) {
      return '-';
    }
    return 'x';
  }
}

export enum cellStatus {
  CLEAR = '',
  COMPUTING = 'X',
  WAITING = '-',
}

export enum cellColors {
  TRANSPARENT = 'transparent',
  CORRECT = 'green',
}
