import { UserLevelThread } from './UserLevelThread';
import { Strategies } from './Diagram';

export class KernelLevelThread {
  /**
   * KernelLevel attributes. All attributes which are relevant for correcting the diagram must be added in
   * scheduling-service.ts -> prepareJSONString() to ensure that they are transmitted to the server or are used in an configuration string.
   * Additionally, new attributes should be added in this -> applyConfiguration() to move them between two instances.
   */

  userID = 1;
  kernelthreadID: number;
  userLevelThreadsOfKernelThread: UserLevelThread[] = [];
  userStrategy: Strategies = Strategies.FCFS;
  userStrategyHelper: Strategies = Strategies.FCFS;
  startTime = 0;
  userlevelTimeSliceLength = 2;
  lowestPriorityFirstKernel = false;
  prioChangeComputingKernel = -1.0;
  prioChangeWaitingKernel = 2.0;
  userSchedulingPreemptive = false;

  /**
   * Local attributes which are only relevant to style and maintain the diagram in the browser.
   * These values do not need to be transmitted to a server
   */
  kernelThreadName: string;
  lowestUserThreadID = -1;
  userStrategyDynamic = false;
  userlevelSchedulerArrivalTimeRR = false;

  /**
   * Initialieses th important attributes of the kernellevelthread
   * The attributes are not set if a new Kernellevelthread is created from the configuration string.
   * The corresponding values will be set maual in this case.
   *
   * In all other cases, all attributes must be added
   *
   * @param id
   * @param name
   * @param computingTime
   * @param startTime
   * @param numberOfCells
   */
  constructor(id?: number, name?: string, computingTime?: number, startTime?: number, numberOfCells?: number) {
    if (computingTime !== undefined) {
      this.kernelThreadName = name;
      this.kernelthreadID = id;
      if (id !== -1) {
        id = id * 1000;
        this.userID = id + 1;
      } else {
        this.userID = -1;
      }
      this.userLevelThreadsOfKernelThread.push(
        new UserLevelThread(id, name, computingTime, startTime, numberOfCells, this.kernelthreadID)
      );

      this.startTime = startTime;
    }
  }

  /**
   * Creates a new Userlevelthread inside this Kernellevelthread. Attributes will be initialised randomly
   * @param parentName prefix of the corresponding parent thread
   * @return newly created Userlevelthread
   */
  addUserlevelThread(parentName: string): UserLevelThread {
    const userStartTime = Math.floor(Math.random() * 7);
    const newUserLevelThread = new UserLevelThread(
      this.userID,
      parentName + '_U' + (this.userID % 1000),
      Math.floor(Math.random() * 7) + 1,
      userStartTime,
      this.userLevelThreadsOfKernelThread[0].cells.length,
      this.kernelthreadID
    );
    this.userLevelThreadsOfKernelThread.push(newUserLevelThread);
    this.userID++;
    if (userStartTime < this.startTime) {
      this.startTime = userStartTime;
    }

    return newUserLevelThread;
  }
}
