import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulingConfigurationComponent } from './scheduling-configuration.component';

describe('SchedulerConfigurationComponent', () => {
  let component: SchedulingConfigurationComponent;
  let fixture: ComponentFixture<SchedulingConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SchedulingConfigurationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulingConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
