import { Component, OnInit } from '@angular/core';
import { Diagram, Strategies } from '../scheduling-logic/diagram/Diagram';
import { SchedulingService } from '../scheduling-logic/scheduling.service';
import { TranslateService } from '@ngx-translate/core';
import { faQuestion, faAlignJustify } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-scheduler-configuration',
  templateUrl: './scheduling-configuration.component.html',
  styleUrls: ['./scheduling-configuration.component.scss'],
})
export class SchedulingConfigurationComponent implements OnInit {
  faQuestion = faQuestion; // Icons which are used in this component
  faAlignJustify = faAlignJustify;

  Strategies = Strategies;

  showConfigParseError: boolean;
  preemptiveAllowed = false;
  strategyDescription = '';
  configurationString = '';
  strategyTitle = '';
  dynamicHelper = false;

  constructor(
    public diagram: Diagram,
    public schedulerService: SchedulingService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.diagram.getKernelLevelThreads().subscribe((next) => (this.diagram.allKernelLevelThreads = next));
    this.translate.onLangChange.subscribe(() => {
      if (this.strategyDescription !== '') {
        this.displayStrategyDescriptionBody();
      }
    });
  }

  /**
   * Alters parameters according to the selected strategy.
   * Deadlines are toggled on/off if EDF is selected.
   */
  onStrategyChange($event: number): void {
    this.diagram.strategy = +$event;
    if (this.diagram.strategy === Strategies.EDF) {
      this.diagram.setDeadlines();
    } else if (!this.diagram.advancedConfigurationOptions) {
      this.diagram.setDeadlinesToDefault();
    }

    this.diagram.updateCorrectionStatus();
    if (this.strategyDescription !== '') {
      this.displayStrategyDescriptionBody();
    }
  }

  /**
   * Use the given config String to use the configuration.
   */
  useConfig(): void {
    this.showConfigParseError = this.diagram.applyConfiguration(this.configurationString);
    this.diagram.updateCorrectionStatus();
  }

  toggleConfigurationActivated(): void {
    this.strategyDescription = '';
  }

  /**
   * Toggles the visibility of the Strategy description.
   */
  displayStrategyDescription(): void {
    if (this.strategyDescription !== '') {
      this.strategyDescription = '';
      this.strategyTitle = '';
      return;
    }
    this.displayStrategyDescriptionBody();
  }

  /**
   * displays the description for the current strategy in the dialog.
   */
  displayStrategyDescriptionBody(): void {
    switch (this.diagram.strategy) {
      case Strategies.FCFS: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.fcfs');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.fcfstitle');
        break;
      }
      case Strategies.SJF: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.sjf');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.sjftitle');
        break;
      }
      case Strategies.SRTN: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.srtn');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.srtntitle');
        break;
      }
      case Strategies.EDF: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.edf');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.edftitle');
        break;
      }
      case Strategies.Priority: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.prio');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.priotitle');
        break;
      }
      default: {
        this.strategyDescription = this.translate.instant('scheduling.config.strategyDescription.rr');
        this.strategyTitle = this.translate.instant('scheduling.config.strategyDescription.rrtitle');
      }
    }
  }

  /**
   * Helper method to use the html input with booleans.
   * @param $event true if the strategy should be dynamic.
   */
  updatedynamicStatic($event: boolean): void {
    if ($event) {
      this.diagram.strategy = Strategies.Priority_dynamic;
    } else {
      this.diagram.strategy = Strategies.Priority;
    }
  }
}
