import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadConfigurationComponent } from './thread-configuration.component';

describe('ThreadConfigurationComponent', () => {
  let component: ThreadConfigurationComponent;
  let fixture: ComponentFixture<ThreadConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThreadConfigurationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
