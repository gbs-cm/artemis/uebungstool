import { Component, OnInit } from '@angular/core';
import { Diagram, Strategies } from '../scheduling-logic/diagram/Diagram';
import { KernelLevelThread } from '../scheduling-logic/diagram/KernelLevelThread';
import { UserLevelThread } from '../scheduling-logic/diagram/UserLevelThread';
import { faMinus, faQuestion, faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-thread-configuration',
  templateUrl: './thread-configuration.component.html',
  styleUrls: ['./thread-configuration.component.scss'],
})
export class ThreadConfigurationComponent implements OnInit {
  faMinus = faMinus; // Icons which are used in this component
  faPlus = faPlus;
  faQuestion = faQuestion;

  Strategies = Strategies;

  kernelLevelThreads: KernelLevelThread[] = [];

  public showHelp = false;

  // Max Values If values are changed here, they must also be adapted in teh server.
  //Otherwise teh input sanitation in the server would not work properly anymore.
  readonly maxComputingTime = 50;
  readonly maxStartingTime = 100;
  readonly maxIOLength = 50;
  readonly maxNumberOfThreads = 50;

  constructor(public diagram: Diagram) {}

  ngOnInit(): void {
    this.diagram.getKernelLevelThreads().subscribe((next) => (this.kernelLevelThreads = next));
  }

  /**
   * Toggles the Io Interruption of this userlevelthread.
   * The minimal Computation time will be adapted -> must be at least 2
   * so taht an io interruption can take place.
   * @param userLevelThread whose IO-interruption will be toggled.
   */
  addIOInterruption(userLevelThread: UserLevelThread): void {
    userLevelThread.addIOInterruption();
    this.diagram.updateCorrectionStatus();
  }

  /**
   * togles the deadline for the given Userlevelthread
   * @param userLevelThread whose dealine will be updated.
   */
  toggleDeadline(userLevelThread: UserLevelThread): void {
    userLevelThread.toggleDeadline();
    this.diagram.updateCorrectionStatus();
  }

  /**
   * adds a new Kernellevelthread to the list of threads
   */
  addKernelLevelThread(): void {
    this.diagram.addKernelLevelThread();
    this.diagram.updateCorrectionStatus();
  }

  deleteKernelLevelThread(kernelLevelThread: KernelLevelThread): void {
    this.diagram.deleteKernelLevelThread(kernelLevelThread);
    this.diagram.updateCorrectionStatus();
  }

  addUserLevelThread(kernelLevelThread: KernelLevelThread): void {
    this.diagram.addUserLevelThread(kernelLevelThread);
    this.diagram.updateCorrectionStatus();
  }

  /**
   * changes the computing time of the userlevelthread. If the io interruption starts after the
   * the computing time, the io start time will be adapted.
   * @param $event new Value of the computing time.
   * @param userLevelThread changed thread
   */
  changeComputingTime($event: number, userLevelThread: UserLevelThread): void {
    if ($event <= userLevelThread.ioAftern) {
      userLevelThread.ioAftern--;
    }

    this.diagram.updateCorrectionStatus();
    this.diagram.updateDiagram();
  }

  /**
   * changes the starting time of the userlevelthread. If the io deadline is earlier than the
   * the starting time, the deadline will be increased to one position after the starttime.
   * @param $event new Value of the start time.
   * @param userLevelThread changed thread
   * @param kernelLevelThread kernellevelthread of the userlevelthread to adapt the combined
   * computing time.
   */
  changeStartingTime($event: number, userLevelThread: UserLevelThread, kernelLevelThread: KernelLevelThread): void {
    if ($event >= userLevelThread.deadline && userLevelThread.deadline > 0) {
      userLevelThread.deadline = $event + 1;
      this.diagram.alterDeadline($event + 1, userLevelThread);
      userLevelThread.deadlinePrevious = $event + 1;
    }
    this.diagram.alterStartingTime($event, userLevelThread, kernelLevelThread);
    userLevelThread.startTimePrevious = $event;
    this.diagram.updateCorrectionStatus();
  }

  /**
   * changes the deadline of the userlevelthread. If the io deadline is earlier than the
   * the starting time, the starting time will be decreased to one position before the deadline.
   * @param $event new Value of the deadline.
   * @param userLevelThread changed thread
   * @param kernelLevelThread kernellevelthread of the userlevelthread to adapt the combined computing time.
   */
  changeDeadline($event: number, userLevelThread: UserLevelThread, kernelLevelThread: KernelLevelThread): void {
    if ($event <= userLevelThread.startTime) {
      userLevelThread.startTime = $event - 1;
      this.diagram.alterStartingTime($event - 1, userLevelThread, kernelLevelThread);
      userLevelThread.startTimePrevious = $event - 1;
    }

    this.diagram.alterDeadline($event, userLevelThread);
    userLevelThread.deadlinePrevious = $event;
    this.diagram.updateCorrectionStatus();
  }

  /**
   * Deletes a userlevelthread. If this userlevelthread is the last thread in a
   * kernellevelthread, this kernellevelthread will also be deleted.
   * @param userLevelThread
   * @param kernelLevelThread
   */
  deleteThread(userLevelThread: UserLevelThread, kernelLevelThread: KernelLevelThread): void {
    if (kernelLevelThread.userLevelThreadsOfKernelThread.length === 1) {
      this.diagram.deleteKernelLevelThread(kernelLevelThread);
    } else {
      this.diagram.deleteUserLevelThread(userLevelThread, kernelLevelThread);
    }
    this.diagram.updateCorrectionStatus();
  }

  changeIOAfterN($event: number, userLevelThread: UserLevelThread): void {
    this.diagram.alterIOAfterN($event, userLevelThread);
    this.diagram.updateCorrectionStatus();
  }

  toggleHelp(): void {
    this.showHelp = !this.showHelp;
  }

  /**
   * Sets the userstrategy to strategies priority if the dynamic option is selected for thsi thread.
   * @param $event
   * @param kernelLevelThread
   */
  updateUserStrategyDynamic($event: boolean, kernelLevelThread: KernelLevelThread): void {
    if ($event) {
      kernelLevelThread.userStrategy = Strategies.Priority_dynamic;
    } else {
      kernelLevelThread.userStrategy = Strategies.Priority;
    }
  }

  /**
   * Manages a newly selected userlevel Strategy:
   * Disables/enables Deadlines
   * @param $event
   * @param kernelLevelThread
   */
  updateUserStrategy($event: Strategies, kernelLevelThread: KernelLevelThread): void {
    kernelLevelThread.userStrategy = +$event;
    if (kernelLevelThread.userStrategy === Strategies.EDF) {
      kernelLevelThread.userLevelThreadsOfKernelThread.forEach((u) => {
        u.setDeadline();
      });
    } else {
      kernelLevelThread.userLevelThreadsOfKernelThread.forEach((u) => {
        u.resetDeadline();
      });
    }
    this.diagram.updateCorrectionStatus();
  }
}
