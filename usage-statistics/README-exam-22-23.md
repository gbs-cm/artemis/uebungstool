# Evaluation
Evaluates the usage of the tool with the logfiles from the server.

## Usage
1. Download, unzip and merge the logfiles with the Downloader Script ```logDownloader.sh```
2. Execute the Jupiter Notebook file ```analysisMain.ipynb```
3. The generated diagrams have been stored in the `diagrams` directory.


## Results
These diagrams visualize all requests to the springboot server between the 17. December 2022 until the midterm exam at the 25. February 2023.

They mainly show that most people start practicing a few days before the exam :)

### Proportion of Individual Scheduling Strategies
<a>
<img src="diagrams/totalScheduling-exam23.png" alt="img: Proportion of Individual Scheduling Strategies">
</a>

### Proportion of individual Paging Strategies 
<a>
<img src="diagrams/totalPaging-exam23.png" alt="img: Proportion of Individual Paging Strategies">
</a>

### Scheduling Over Time
<a>
<img src="diagrams/schedulingOverTime-exam23.png" alt="img: Scheduling over time">
</a>


### Paging Over Time
<a>
<img src="diagrams/pagingOverTime-exam23.png" alt="img: Paging over time">
</a>
