# Evaluation
Evaluates the usage of the tool with the logfiles from the server.

## Usage
1. Download, unzip and merge the logfiles with the Downloader Script ```logDownloader.sh```
2. Execute the Jupiter Notebook file ```analysisMain.ipynb```
3. The generated Diagrams have been stored in teh diagrams directory.


## Results
These diagrams visualize all requests to the springboot server between the 30. October 2022 until the midterm exam at the 18. December 2022.

They mainly show that most people start practicing a few days before the exam :)

### Proportion of Individual Scheduling Strategies
<a>
<img src="diagrams/totalScheduling-midterm22.png" alt="img: Proportion of Individual Scheduling Strategies">
</a>

### Proportion of individual Paging Strategies 
<a>
<img src="diagrams/totalPaging-midterm22.png" alt="img: Proportion of Individual Paging Strategies">
</a>

### Scheduling Over Time
<a>
<img src="diagrams/schedulingOverTime-midterm22.png" alt="img: Scheduling over time">
</a>


### Paging Over Time
<a>
<img src="diagrams/pagingOverTime-midterm22.png" alt="img: Paging over time">
</a>
