#!/usr/bin/expect -f
# Downloads all existing logfiles and merges them into one merged file.
# At the given location, a new folder (./logs) is created which contains all logs in the mergedLogFile.txt


# First Argument:  user on vmott42.in.tum.de
# Second Argument: path at which the files should be stored
sudo scp -r $1@vmott42.in.tum.de:/home/jungmann/logs $2

cd $2

sudo chmod 777 logs
cd logs
gzip -d *.gz
cat *.0 > mergedLogFile.txt
find . -name 'spring.log*' -delete
