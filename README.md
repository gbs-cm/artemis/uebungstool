# Übungstool

## Requirements

1. install java-16
2. install maven
3. install Node.js: https://nodejs.org/en/download/
4. install Angular: `npm install -g @angular/cli`
5. Load all necessary Node dependencies: `npm install`

## Starting the Service

### Server:

execute: `startServer.sh`

(Alternative: execute /src/main/java/de.uebungstool.uebungstool/UebungstoolApplication.java in your IDE.)

Using intellij: edit configurations > active profiles -> enter: dev

-> Will be available on http://localhost:8080/.

### Client:

execute: `startClient.sh`

(Alternative: execute `npm start` in /src/main/webapp/)

-> Will be available on http://localhost:4200/.

## Paging Configuration Files

The memory paging site offers the possibility to deploy configurations from server. To deploy configurations they have
to be placed in the subfolder "PagingSampleFiles" of the servers working directory, and they must contain a JSON Diagram
in the first line. Other lines will be ignored.

### How to make a JSON Diagram

1. Configure your Diagram via the Clients Web Interface.
2. Open the developers console.
3. Click on "Submit" inside the clients interface.
4. Copy the JSON Object from developer console.

### How to add Configuration to server

1. Go into the servers working directory
2. Go to "PagingSampleFiles", create if it doesn't exist
3. Create a new files here with the file extension ".sample". (Other extensions will be ignored)
4. Place your JSON in the first line of the new file. Other lines will be ignored.

